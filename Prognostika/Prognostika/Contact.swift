//
//	Contact.swift
//
//	Create by grom on 13/12/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct Contact{

	var mail : String!
	var message : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		mail = dictionary["mail"] as? String
		message = dictionary["message"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if mail != nil{
			dictionary["mail"] = mail
		}
		if message != nil{
			dictionary["message"] = message
		}
		return dictionary
	}

}
