//
//  FAQVC.swift
//  Prognostika
//
//  Created by Cope on 3/15/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class FAQVC: UIViewController, ProtocolFaq, ProtocolRefreshToken {
    
    @IBOutlet weak var lbl_noFaq: UILabel!

    //MARK: Variables
    var closeOtherNodes: Bool = true
    var insertRowAnimation: UITableViewRowAnimation = .top
    var deleteRowAnimation: UITableViewRowAnimation = .top
    var allNodes : [YUTableViewNode]!
    var arrayFaq = [Faq]()
    var arrayquestions : [String]!
    var arrayAnswers : [String]!
    var open : Bool!
    var offset = 0
    var pagination = 0
    //MARK: Outlets
    @IBOutlet weak var tableView: YUTableView!
    //MARK: Actions
    @IBAction func actionSideMenu(_ sender: Any) {
        self.view.backgroundColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 41.0/255.0, alpha: 1.0)

        if !open {
            self.captureScreen()
            self.showMenu(sender as! UIButton)
            open = true
        }
        else{
            self.closeMenu(sender as! UIButton)
            open = false
        }
    }
    //MARK: LyfeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getFaq()
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 46.0/255.0, alpha: 1.0)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor(colorLiteralRed: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)

        open = appdelegat.is_side_menu_open
        appdelegat.titleForVC(vc: self, title: "FAQ",isTransparent: false)
        if tableView.visibleCells.count == 0 {
            tableView.isHidden = true
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        appdelegat.offset = 0
        appdelegat.payload = 0
        arrayFaq = []
    }
    //MARK: Functions
    //MARK: Get Faq
    func getFaq(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        var dataStore = DataStore()
        dataStore.delegat_faq = self
        dataStore.getFaq()
    }
    func didGetFaq(response: ResponseFaq?) {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        
        //FIXME:WHEN THERE IS NO FAQ THERE IS NO NEED TO SHOW TABLEVIEW...IT WAS UGLY,Question is to place text here or not.
        
        if response != nil {
            print(response)
            if response?.payload.count == 0 {
                tableView.isHidden = true
                lbl_noFaq.isHidden = false
            }
            if response?.status.key == "OK" {
                
                for faq in (response?.payload)! {
                    arrayFaq.append(faq)
                }
                
                var pagination : Int!
                if (response?.pagination.count)! <= 1 {
                    pagination = 0
                    offset = 0
                    appdelegat.payload = pagination
                    appdelegat.offset = offset
                    return
                }
                pagination = (response?.pagination.count)! - 1
                offset = pagination + offset
                appdelegat.payload += pagination
                appdelegat.offset += offset
                setTableProperties()
            }
            else if response?.status.key == "TOKEN_EXPIRED" {
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                saveSuspendedDeviceInfo(info: (response!.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
    }
    //MARK: Refresh token
    func refreshToken(){
        //let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_refreshToken = self
        dataStore.refreshToken()
    }
    func didRefreshToken(response: ResponseLogin?) {
        if let _response = response {
            if _response.status.key == "OK"{
                saveToken(info: _response.payload.first!)
                //call again
                getFaq()
            }
        }
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
    }
    //MARK: Save suspended device
    private func saveSuspendedDeviceInfo(info : Faq){
        DataUtility.sharedInstance.saveSuspendedDeviceInfo(username:DataUtility.sharedInstance.getUserCredentials().username, suspended_until: info.suspended_until)
    }
    //MARK: Table
    func setTableProperties () {
        allNodes = createNodes()
        tableView.setNodes(allNodes)
        tableView.setDelegate(self)
        tableView.allowOnlyOneActiveNodeInSameLevel = closeOtherNodes
        tableView.insertRowAnimation = insertRowAnimation
        tableView.deleteRowAnimation = deleteRowAnimation
        tableView.animationCompetitionHandler = {
            print("Animation ended")
        }
    }
    
    func setTableViewSettings (closeOtherNodes: Bool, insertAnimation: UITableViewRowAnimation, deleteAnimation: UITableViewRowAnimation) {
        self.closeOtherNodes = closeOtherNodes
        self.insertRowAnimation = insertAnimation
        self.deleteRowAnimation = deleteAnimation
    }
    
    func createNodes() -> [YUTableViewNode] {
        var nodes = [YUTableViewNode] ()
        for i in 0..<arrayFaq.count {
            var childNodes = [YUTableViewNode] ()
            for j in 1...1 {
                let grandChildNodes = [YUTableViewNode] ()
//                for k in 1...3 {
//                    let node = YUTableViewNode (data: arrayFaq[i].question, cellIdentifier: "BasicCell")
//                    grandChildNodes.append(node)
//                }
                
                let node = YUTableViewNode(childNodes: grandChildNodes, data: arrayFaq[i].answer, cellIdentifier: "ComplexCell")
                childNodes.append(node)
            }
            let node = YUTableViewNode(childNodes: childNodes, data: arrayFaq[i].question, cellIdentifier: "BasicCell")
            nodes.append (node)
        }
        return nodes
    }
    
    @IBAction func selectRandomNode() {
        var selectedNode = allNodes.first?.getParent()
        repeat {
            selectedNode = selectedNode!.childNodes[Int(arc4random_uniform(UInt32(selectedNode!.childNodes.count)))]
        } while arc4random_uniform(2) == 0 && selectedNode!.hasChildren()
        tableView.selectNode (selectedNode!)
    }
}

extension UIViewController: YUTableViewDelegate {
    public func setContentsOfCell(_ cell: UITableViewCell, node: YUTableViewNode) {
        if let customCell = cell as? FAQCell, let cellDic = node.data as? [String:String] {
            customCell.setLabel(cellDic["label"]!, andImage: cellDic["img"]!)
        } else {
            //question label
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            cell.textLabel!.text = node.data as? String
            cell.textLabel?.textColor = UIColor(colorLiteralRed: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0)
        }
    }
    public func heightForNode(_ node: YUTableViewNode) -> CGFloat? {
        if node.cellIdentifier == "ComplexCell" {
            return 55.0
        }
        else{
            return 55.0
        }
        return nil
    }
    
    public func didSelectNode(_ node: YUTableViewNode, indexPath: IndexPath) {
        if !node.hasChildren () {
            let alert = UIAlertView(title: "Row Selected", message: "Label: \(node.data as! String)", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        if appdelegat.faqActiveIndex == nil {
            appdelegat.faqActiveIndex = indexPath.row

        }
        if appdelegat.faqActiveIndex != indexPath.row{
            appdelegat.faqPreviousIndex = appdelegat.faqActiveIndex
            appdelegat.faqActiveIndex = indexPath.row

        }
        if appdelegat.faqActiveIndex == indexPath.row{
            appdelegat.faqActiveIndex = indexPath.row
        }
        //appdelegat.faqActiveIndex = indexPath.row
    }
    
}

