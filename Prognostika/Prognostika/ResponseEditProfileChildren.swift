//
//	ResponseEditProfileChildren.swift
//
//	Create by Apple on 19/12/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ResponseEditProfileChildren{

	var first : ResponseEditProfileFirst!
	var second : ResponseEditProfileNameAndSurname!
	var nameAndSurname : ResponseEditProfileNameAndSurname!
	//var plainPassword : ResponseEditProfilePlainPassword!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let firstData = dictionary["first"] as? NSDictionary{
				first = ResponseEditProfileFirst(fromDictionary: firstData)
			}
		if let secondData = dictionary["second"] as? NSDictionary{
				second = ResponseEditProfileNameAndSurname(fromDictionary: secondData)
			}
		if let nameAndSurnameData = dictionary["nameAndSurname"] as? NSDictionary{
				nameAndSurname = ResponseEditProfileNameAndSurname(fromDictionary: nameAndSurnameData)
			}
//		if let plainPasswordData = dictionary["plainPassword"] as? NSDictionary{
//				plainPassword = ResponseEditProfilePlainPassword(fromDictionary: plainPasswordData)
//			}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if first != nil{
			dictionary["first"] = first.toDictionary()
		}
		if second != nil{
			dictionary["second"] = second.toDictionary()
		}
		if nameAndSurname != nil{
			dictionary["nameAndSurname"] = nameAndSurname.toDictionary()
		}
//		if plainPassword != nil{
//			dictionary["plainPassword"] = plainPassword.toDictionary()
//		}
		return dictionary
	}

}
