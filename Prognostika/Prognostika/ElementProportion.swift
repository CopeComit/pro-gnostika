//
//  ImageProportion.swift
//  Prognostika
//
//  Created by Cope on 3/2/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation
import UIKit

typealias ElementProportion = CGSize

extension ElementProportion {
    func resizeElement(originalWidth : CGFloat, originalHeight : CGFloat)-> CGSize{
        
        var imageSize = CGSize()
        let screenSize: CGRect = UIScreen.main.bounds
        
        // size of applications screen
        let actualScreenWidth     = screenSize.width
        let actualScreenHeight    = screenSize.height
        
        //size of mockup design screen
        let originalScreenWidth    : CGFloat = 360
        let originalScreenHeight   : CGFloat = 640
        
        //apply mockup design proportions to application
        if originalWidth != nil{
            imageSize.width     = (originalWidth / originalScreenWidth) * actualScreenWidth
        }
        if originalHeight != nil{
            imageSize.height    = (originalHeight / originalScreenHeight) * actualScreenHeight
        }
        return imageSize
    }
    
    
}
