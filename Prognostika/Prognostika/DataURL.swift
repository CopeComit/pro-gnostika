//
//  DataURL.swift
//  Click2Win
//
//  Created by Branko Grbic on 7/30/15.
//  Copyright (c) 2015 ComITGroup. All rights reserved.
//

import UIKit
internal typealias URLPath = String

class DataURL {
   
    static let sharedInstance = DataURL()
    
    static let  baseUrlHelper           = "http://test.pro-gnostika.gr"
    lazy var    register                : URLPath   = {  return  baseUrlHelper   + "/api/user/register" }()
    lazy var    sendContact             : URLPath   = {  return  baseUrlHelper   + "/api/contact"}()
    lazy var    requestPassword         : URLPath   = {  return  baseUrlHelper   + "/api/user/request-password" }()
    lazy var    resetPassword           : URLPath   = {  return  baseUrlHelper   + "/api/user/reset-password" }()
    lazy var    requestToken            : URLPath   = {  return  baseUrlHelper   + "/oauth/v2/token"}()
    lazy var    requestTokenPassword    : URLPath   = {  return  baseUrlHelper   + "/oauth/v2/token"}()
    lazy var    requestTokenRefresh     : URLPath   = {  return  baseUrlHelper   + "/oauth/v2/token"}()
    lazy var    getActivePredictions    : URLPath   = {  return  baseUrlHelper   + "/api/prediction/?kind=active" }()
    lazy var    getPendingPredictions   : URLPath   = {  return  baseUrlHelper   + "/api/prediction/?kind=pending"}()
    lazy var    getOutcomesPredictions  : URLPath   = {  return  baseUrlHelper   + "/api/prediction?kind=outcomes"}()
    lazy var    getRecentInfo           : URLPath   = {  return  baseUrlHelper   + "/api/recent_info/" }()
    lazy var    registerDevice          : URLPath   = {  return  baseUrlHelper   + "/push/device/ios/register"}()
    lazy var    getFaq                  : URLPath   = {  return  baseUrlHelper   + "/api/general/Faq"}()
    lazy var    userPredictions         : URLPath   = {  return  baseUrlHelper   + "/api/general/UserPrediction"}()
    //MARK: TODO
    lazy var    getPersonalStatistics   : URLPath  = {  return  baseUrlHelper   + "/api/statistic/user/" }()
    lazy var    getGeneralStatistics    : URLPath  = {  return  baseUrlHelper   + "/api/statistic"}()
    lazy var    getUserProfile          : URLPath  = {  return  baseUrlHelper   + "/api/general/User"}()
    lazy var    editUser                 : URLPath  = {  return  baseUrlHelper    + "/api/general/User/" }()


}



