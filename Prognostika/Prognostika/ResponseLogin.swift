//
//	ResponseLogin.swift
//
//	Create by Branko Grbic on 28/11/2016

import Foundation

struct ResponseLogin{

	var pagination  : Pagination!
	var payload     : [LoginPayload]!
	var status      : Status!

	init(fromDictionary dictionary: NSDictionary){
		if let paginationData = dictionary["pagination"] as? NSDictionary{
				pagination = Pagination(fromDictionary: paginationData)
			}
		payload = [LoginPayload]()
		if let payloadArray = dictionary["payload"] as? [NSDictionary]{
			for dic in payloadArray{
				let value = LoginPayload(fromDictionary: dic)
				payload.append(value)
			}
		}
		if let statusData = dictionary["status"] as? NSDictionary{
				status = Status(fromDictionary: statusData)
			}
	}
}
