//
//  PrognostikaProducts.swift
//  Prognostika
//
//  Created by Cope on 8/7/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation

public struct PrognostikaProducts {
    public static let credit25 = "com.app.prognostika_25_units"
    public static let credit50 = "com.app.prognostika_50_units"
    public static let credit75 = "com.app.prognostika_75_units"
    public static let credit100 = "com.app.prognostika_100_units"
    
    fileprivate static let productIdentifiers: Set<ProductIdentifier> = [PrognostikaProducts.credit25, PrognostikaProducts.credit50, PrognostikaProducts.credit75, PrognostikaProducts.credit100]
    
    public static let store = IAPHelper(productIds: PrognostikaProducts.productIdentifiers)
}
func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
