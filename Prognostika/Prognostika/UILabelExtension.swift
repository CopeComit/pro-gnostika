//
//  UILabelExtension.swift
//  SharpGentleman
//
//  Created by Branko Grbic on 1/6/17.
//
import Foundation
import UIKit

extension UILabel {
    func from(html: String) {
        if let htmlData = html.data(using: String.Encoding.unicode) {
            do {
                self.attributedText = try NSAttributedString(data: htmlData,
                                                             options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                             documentAttributes: nil)
            } catch let e as NSError {
                print("Couldn't parse this html my buddy :( ---> \(html): \(e.localizedDescription)")
            }
        }
    }
}
