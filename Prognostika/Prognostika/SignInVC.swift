//
//  SignInVC.swift
//  Prognostika
//
//  Created by Cope on 3/17/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class SignInVC: UIViewController, ProtocolGetToken, ProtocolRegisterDevice, ProtocolSuccesLoginUser,GIDSignInDelegate, GIDSignInUIDelegate {

    //MARK: Variables
    let SUCCESS_RESPONSE_KEY         =  "OK"
    let BAD_DATA_RESPONSE_KEY        = "BAD_DATA"
    var isGoogleRegister = false
    //MARK: Outlets
    
    @IBOutlet weak var tf_email: CustomTextField!
    @IBOutlet weak var tf_password: CustomTextField!
    @IBOutlet weak var btn_signIn: UIButton!
    @IBOutlet weak var btn_googlePlus: UIButton!
    @IBOutlet weak var btn_Facebook: UIButton!
    //MARK: Actions
    
    @IBAction func actionSignIn(_ sender: Any) {
        getToken()
    }
    
    @IBAction func actionGooglePlus(_ sender: Any) {
        setupGoogleID()
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func actionFacebook(_ sender: Any) {
        let fblogin              =  FBSDKLoginManager()
        appdelegat.isFBLogin     = true
        fblogin.logIn(withReadPermissions: ["public_profile","email"], from: self, handler:{ [unowned self] (result,error) in
            appdelegat.isFBLogin = false
            let info     = result as? Dictionary<String,String>
            var fb_id : String!
            if error != nil {
                print("Facebook connection problems !")
                return
            }else if result!.isCancelled {
                print("Facebook login canceled")
                return
            }else {
                let fbToken = result!.token.tokenString
                DataUtility.sharedInstance.saveFBToken(token: fbToken!)
                fb_id = result?.token.appID
                DataUtility.sharedInstance.saveFBID(fb_id: fb_id!)
                self.getFacebookToken()
            }
        })
    }
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesign()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        appdelegat.titleForVC(vc: self, title: "Sign in",isTransparent: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Functions
    //Google
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
            let userId     = user.userID!                  // For client-side use only!
            let idToken    = user.authentication.accessToken! // Safe to send to the server
            let fullName   = user.profile.name!
            _  = user.profile.givenName!
            let familyName = user.profile.familyName!
            let email      = user.profile.email!
            print(fullName + " " + familyName + " " + email + " " + userId)
            loginFbOrGoogle(valid_token: idToken, isFbToken: false)
        } else {
            print("\(error.localizedDescription)")
        }
    }
    private func tryAutomaticGoogleLogin(){
        GIDSignIn.sharedInstance().signInSilently()
    }
    private func setupGoogleID(){
        GIDSignIn.sharedInstance().clientID   =  "799568545144-6g2h97ucl479f96a6ps3eaelbfjnbqf0.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate   = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    @IBAction func didTapSignOut(sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let idToken      = user.authentication.accessToken
            //let email        = user.profile.email
            DataUtility.sharedInstance.saveGoogleToken(token: idToken)
            
            
                self.isGoogleRegister = true
                self.getGoogleToken()
        } else {
            print("\(error.localizedDescription)")
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        print(error.localizedDescription)
    }

    //Facebook
    //login
    private func loginFbOrGoogle(valid_token : String, isFbToken : Bool){
        var login : LoginUtility!
        if isFbToken == true {
            login = LoginUtility(fb_token: valid_token)
        }else {
            login = LoginUtility(google_token: valid_token)
        }
        login.delegat_success_login = self
        login.delegat_view = self
        login.loginUser()
    }
    func didSuccessLoginUser() {
        self.performSegue(withIdentifier: "signIn", sender: self)
    }
    func setupFacebook(){
        //(self, selector: #selector(self.textFieldViewDidBeginEditing(_:))
        //btn_Facebook.addTarget(self, action: #selector(self.loginButtonClicked) forControlEvents: .touchUpInside)
        btn_Facebook.addTarget(self, action: #selector(self.actionFacebook(_:)), for: .touchUpInside)
    }
    func setupDesign(){
        btn_googlePlus.layer.cornerRadius = 5
        btn_Facebook.layer.cornerRadius = 5
        btn_signIn.layer.cornerRadius = 5
        btn_googlePlus.addShadowImage(shadowStyle: .ShadowRightBottomAndLeft, shadowColor: nil, shadowRadius: 5)
        btn_Facebook.addShadowImage(shadowStyle: .ShadowRightBottomAndLeft, shadowColor: nil, shadowRadius: 5)
    }
    
    //MARK: Protocol login
    //MARK: Get token via Google
    func getGoogleToken(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        let json      = prepareFacebookLogin()
        var dataStore = DataStore()
        dataStore.delegat_getToken = self
        dataStore.getToken(info: json)
    }
    //google login
    private func prepareGoogleLogin()->Dictionary<String,String>{
        var json         = UserToken()
        json.google_token = DataUtility.sharedInstance.getGoogleToken()
        json.client_secret = "4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
        json.client_id = "2_5zt2fjzasbs4sc0w8kgg80wssc8k008ogcskocoo4sog00wo88"
        json.grant_type  = "password"
        return json.toDictionary()
    }
    //MARK: Get token via Facebook
    func getFacebookToken(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        let json      = prepareFacebookLogin()
        var dataStore = DataStore()
        dataStore.delegat_getToken = self
        dataStore.getToken(info: json)
    }
    func getToken(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_getToken = self
        dataStore.getToken(info: json)
    }
    private func prepareJsonLogin()->Dictionary<String,String>{
        var json         = UserToken()
        json.password    = tf_password.text!
        let email_encode = tf_email.text!
        json.username    = email_encode
        json.client_secret = "4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
        json.grant_type  = "password"
        
        return json.toDictionary()
    }
    //facebook login
    private func prepareFacebookLogin()->Dictionary<String,String>{
        var json         = UserToken()
        json.fb_token = DataUtility.sharedInstance.getFBToken()
        json.client_secret = " 4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
        json.client_id = "1_5zt2fjzasbs4sc0w8kgg80wssc8k008ogcskocoo4sog00wo88"
        json.grant_type  = "password"
        return json.toDictionary()
    }
    func didGetToken(response: ResponseLogin?) {
        if let _response = response {
            if _response.status.key == SUCCESS_RESPONSE_KEY{
                DataUtility.sharedInstance.ordinaryLoginPass(success: "OK")
                DataUtility.sharedInstance.saveUserCredentials(username: tf_email.text!, password: tf_password.text!)
                saveToken(info: _response.payload.first!)
                registerDevice()
                
            }
            else if _response.status.key == BAD_DATA_RESPONSE_KEY{
                DataUtility.sharedInstance.hideActivityIndicator(self.view)
                DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.messages.last!)
            }
        }
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
        DataUtility.sharedInstance.saveUser(email: tf_password.text!, password: tf_email.text!)
    }
    //MARK: Register device
    func registerDevice(){
        var dataStore = DataStore()
        dataStore.delegat_registerDevice = self
        dataStore.registerDevice(info: DataUtility.sharedInstance.getDeviceInfo() as! Dictionary<String, String>)
    }
    func didRegisterDevice() {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        self.performSegue(withIdentifier: "signIn", sender: self)
    }

}
