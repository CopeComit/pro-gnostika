//
//  DataFetcher.swift
//  VQuarter
//
//  Created by Branko Grbic on 11/18/2016.
//  Copyright © 2015 ComIT. All rights reserved.
//
import UIKit

typealias ServerStringURL                   = String
typealias UserAuthorizationString           = String
typealias UserJSONDictionary                = NSDictionary
typealias forceReturnDataOnly               = Bool
typealias statusCode                        = Int
typealias statusMessage                     = String
typealias HTTPHeader                        = String
typealias UserStringJson                    = String
typealias AnyUserJsonObject                 = Any?

enum NetworkHTTPMethod : String {
    case GET    = "GET"
    case POST   = "POST"
    case PUT    = "PUT"
    case PATCH  = "PATCH"
    case DELETE = "DELETE"
}
enum ServerNetworkErrors: String ,  Error {
    
    case ErrorResponse              = "ERROR: Server Response!"
    case ErrorJSON                  = "ERROR: JsonSerialization!"
    case ErrorData                  = "ERROR: No data!"
    case UnknownError               = "ERROR: Unknown error!"
    case ErrorLoadImages            = "Can't load images!"
    case noNetworkMsg               = "No Network!"
    case Info                       = "Info"
    case Error                      = "Error"
    case Loading                    = "Loading"
    case InternetIsBroken           = "Internet connection is broken!"
    case InternetIsConnected        = "Internet connection is established"
}

final  class ResultJSON<T>:NSObject{
    let value : T!
    init(_ value : T)
    {
        self.value = value
    }
}
protocol ProtocolMedia : class {
    func didReceiveMediaFile(_ media : Data?)
}
let _USER_AUTHORIZATION = "application/x-www-form-urlencoded"

final class DataFetcher : NSObject , URLSessionDownloadDelegate,URLSessionDelegate,URLSessionTaskDelegate ,URLSessionDataDelegate{
    
    static let  sharedInstance          = DataFetcher()
    
    fileprivate var authorization       : String!
    fileprivate var current_session     : Foundation.URLSession? = nil
    weak    var delegat_media           : ProtocolMedia?
    fileprivate let authorizationfield  = "Authorization"
    fileprivate let applicationjson     = "application/json; charset=utf-8"
    fileprivate let contenttypefield    = "Content-Type"
    fileprivate let sessionIdentifier   = "com.vquarter.background.getdata"
    fileprivate let contentlengthfield  = "Content-Length"
    fileprivate let maximumConnection   = 4
    let client_secret                    = "4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
    let apiKeyHeader                    = "client_secret"
    var         stopFetchingData    = false
    
    func getData(_ url : ServerStringURL, authorization : UserAuthorizationString? , httpHeader : HTTPHeader?,  json : AnyUserJsonObject? , method : NetworkHTTPMethod? , callback: @escaping (_ object : AnyObject?)->Void)
    {
        /*
         if  NetworkConnection.sharedNetwork.isConnectedToNetwork() == false {
         
         DataUtility.sharedInstance.showMessage(ResponseMessageInfo.Error.rawValue, message: NetworkErrors.noNetworkMsg.rawValue)
         return
         }*/
        let url     = URL(string: url)
        var request = URLRequest(url: url!)
        if authorization != nil
        {
            request.setValue(authorization!, forHTTPHeaderField: authorizationfield)
        }
        if httpHeader != nil {
            request.setValue(httpHeader, forHTTPHeaderField: contenttypefield)
        }else {
            request.setValue(applicationjson, forHTTPHeaderField: contenttypefield)
        }
        request.setValue(client_secret, forHTTPHeaderField: apiKeyHeader)
        switch json {
        case let json as UserJSONDictionary:
            let data_body    = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            var buffer       = [UInt8](repeating: 0, count: data_body.count)
            (data_body as NSData).getBytes(&buffer, length: data_body.count)
            let string_data  = String(bytes: buffer, encoding: String.Encoding.utf8)
            request.httpBody = string_data!.data(using: String.Encoding.utf8)
        case let json as UserStringJson:
            request.httpBody = json.data(using: String.Encoding.utf8)
        default:
            print("")
        }
        request.httpMethod   = method!.rawValue
        
        let configuration    = URLSessionConfiguration.ephemeral
        let session          = Foundation.URLSession(configuration: configuration)
        
        session.dataTask(with: request, completionHandler: {
            
            (data,response,error)->Void in
            
            do{
                guard error == nil else { throw NetworkErrors.ErrorResponse }
                guard data  != nil else { throw NetworkErrors.ErrorData}
                
                guard let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject!
                    else {
                        var buffer = [UInt8](repeating: 0, count: data!.count)
                        (data as NSData?)?.getBytes(&buffer, length: (data?.count)!)
                        
                        if let string_json = String(bytes: buffer, encoding: String.Encoding.utf8) as String!{
                            callback(string_json as AnyObject?)
                            return
                        }else {
                            throw NetworkErrors.ErrorJSON
                        }
                }
                switch json
                {
                case let dictionary as NSDictionary:
                    callback(dictionary)
                    return
                case let array as NSArray:
                    callback(array)
                    return
                case let datas as Data:
                    callback(datas as AnyObject?)
                    return
                default:
                    //nothing from data my buddy😇
                    DataUtility.sharedInstance.showMessage(ResponseMessageInfo.Error.rawValue, message: NetworkErrors.UnknownError.rawValue)
                    callback(nil)
                    return
                }
            }catch let error as ServerNetworkErrors
            {
                DataUtility.sharedInstance.showMessage(ResponseMessageInfo.Error.rawValue, message: error.rawValue)
                callback(nil)
                return
            }catch let error {
                print(error)
                callback(nil)
                return
            }
        }).resume()
    }
    lazy var getSessions : Foundation.URLSession =  {
        
        let sessionIdentifier                = "com.vquarter.background.getdata"
        let config : URLSessionConfiguration!
        config = URLSessionConfiguration.background(withIdentifier: sessionIdentifier)
        let queue                            = OperationQueue()
        queue.maxConcurrentOperationCount    = OperationQueue.defaultMaxConcurrentOperationCount
        config.httpMaximumConnectionsPerHost = 4
        let session = Foundation.URLSession(configuration: config, delegate: self, delegateQueue: queue)
        return session
    }()
    func getData(_ paths : [URL])
    {
        let session = getSessions
        current_session = session
        
        DispatchQueue.concurrentPerform(iterations: paths.count, execute: { index -> Void in
            
            session.downloadTask(with: paths[index]).resume()
        })
    }
    func getData(_ paths : URL , result : @escaping (_ media : Data?)->Void)
    {
        let request = URLRequest(url: paths)
        let configuration    = URLSessionConfiguration.ephemeral
        let _session =   Foundation.URLSession(configuration: configuration)
        
        current_session  = _session
        current_session?.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
            if let media = data{
                DispatchQueue.main.async(execute: { () -> Void in
                    if self.stopFetchingData == false {
                        result(media)
                    }
                })
            }
        }).resume()
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        print("sentData: " + "\(bytesSent)" + " totalBytesSent : " + "\(totalBytesSent)")
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        let data = try? Data(contentsOf: location)
        
        if let _delegat_media = self.delegat_media
        {
            DispatchQueue.main.async(execute: { () -> Void in
                _delegat_media.didReceiveMediaFile(data)
            })
        }
    }
    /*
     func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
     
     if error != nil && error?.code != 200 {
     DataUtility.sharedInstance.showMessage(MessageInfo.Error.rawValue, message: (error?.description)!)
     }
     if let _delegat_media = delegat_media {
     dispatch_async(dispatch_get_main_queue(), { () -> Void in
     _delegat_media.didReceiveMediaFile(nil)
     })
     }
     }*/
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        debugprint("DID FINISH")
        print(session.sessionDescription!)
    }
    internal func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        
        if error != nil {
            DataUtility.sharedInstance.showMessage(NetworkErrors.Error.rawValue, message: NetworkErrors.ErrorData.rawValue)
        }
        return
    }
}
