//
//	Payload.swift
//
//	Create by grom on 15/12/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct PersonalStatistics{

    var investment : String!
    var lost : String!
    var profit : String!
    var voidField : String!
    var won : String!
    var yield : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    init(fromDictionary dictionary: NSDictionary){
        investment = dictionary["investment"] as? String
        lost = dictionary["lost"] as? String
        profit = dictionary["profit"] as? String
        voidField = dictionary["void"] as? String
        won = dictionary["won"] as? String
        yield = dictionary["yield"] as? String
    }

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if investment != nil{
			dictionary["investment"] = investment
		}
		if lost != nil{
			dictionary["lost"] = lost
		}
		if profit != nil{
			dictionary["profit"] = profit
		}
		if voidField != nil{
			dictionary["void"] = voidField
		}
		if won != nil{
			dictionary["won"] = won
		}
		if yield != nil{
			dictionary["yield"] = yield
		}
		return dictionary
	}

}
