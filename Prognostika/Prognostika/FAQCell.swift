//
//  FAQCell.swift
//  Prognostika
//
//  Created by Cope on 3/15/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class FAQCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setLabel (_ text: String, andImage imageName: String) {
        label.text = text
        img.image = UIImage(named: imageName)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
