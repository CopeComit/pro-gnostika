//
//	ResponseEditProfile.swift
//
//	Create by Apple on 19/12/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ResponseEditProfile{

	var pagination : ResponseEditProfilePagination!
	var payload : [ResponseEditProfilePayload]!
	var status : ResponseEditProfileStatu!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let paginationData = dictionary["pagination"] as? NSDictionary{
				pagination = ResponseEditProfilePagination(fromDictionary: paginationData)
			}
		payload = [ResponseEditProfilePayload]()
		if let payloadArray = dictionary["payload"] as? [NSDictionary]{
			for dic in payloadArray{
				let value = ResponseEditProfilePayload(fromDictionary: dic)
				payload.append(value)
			}
		}
		if let statusData = dictionary["status"] as? NSDictionary{
				status = ResponseEditProfileStatu(fromDictionary: statusData)
			}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		var dictionary = NSMutableDictionary()
		if pagination != nil{
			dictionary["pagination"] = pagination.toDictionary()
		}
		if payload != nil{
			var dictionaryElements = [NSDictionary]()
			for payloadElement in payload {
				dictionaryElements.append(payloadElement.toDictionary())
			}
			dictionary["payload"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status.toDictionary()
		}
		return dictionary
	}

}