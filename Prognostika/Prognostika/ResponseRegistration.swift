//
//	RootClass.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ResponseRegistration{

	var pagination : Pagination!
	var payload : [RegisterData]!
	var status : Status!

	init(fromDictionary dictionary: NSDictionary){
		if let paginationData = dictionary["pagination"] as? NSDictionary{
				pagination = Pagination(fromDictionary: paginationData)
			}
		payload = [RegisterData]()
		if let payloadArray = dictionary["payload"] as? [NSDictionary]{
			for dic in payloadArray{
				let value = RegisterData(fromDictionary: dic)
				payload.append(value)
			}
		}
		if let statusData = dictionary["status"] as? NSDictionary{
				status = Status(fromDictionary: statusData)
			}
	}

	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if pagination != nil{
			dictionary["pagination"] = pagination.toDictionary()
		}
		if payload != nil{
			var dictionaryElements = [NSDictionary]()
			for payloadElement in payload {
				dictionaryElements.append(payloadElement.toDictionary())
			}
			dictionary["payload"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status.toDictionary()
		}
		return dictionary
	}

}
