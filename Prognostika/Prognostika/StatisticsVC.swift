//
//  StatisicsVC.swift
//  Prognostika
//
//  Created by Cope on 3/16/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit
import Charts

class StatisticsVC: UIViewController, ProtocolGeneralStatistics, ProtocolRefreshToken, ChartViewDelegate {
    //MARK: Vars and lets
    var open : Bool!
    var generalStatistics = true
    var activeFilterTag = 0
    var previousFilterTag = 0
    var temporaryFilterTag = 0
    var roomsStatistics =  [GeneralStatistics]()
    var selectedRoomStatistic : GeneralStatistics!
    var winData  =      [Double]()
    var lostData =      [Double]()
    var voidData =      [Double]()
    let arrayWin    = ["Win","Total"]
    let arrayVoid   = ["Void","Total"]
    let arrayLost   = ["Lost","Total"]
    let winColor = UIColor(red: 0/255, green: 121/255, blue: 255/255, alpha: 1)
    let voidColor = UIColor(red: 183/255, green: 183/255, blue: 183/255, alpha: 1)
    let lostColor = UIColor(red: 19/255, green: 19/255, blue: 19/255, alpha: 1)
    let platinum = UIColor(colorLiteralRed: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    var arrayButton = [UIButton]()
    var swipeFilterTag = 0
    var filter = appdelegat.statisticsFilter
    //MARK: Outlets
    @IBOutlet weak var lblFilterExplainer: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblYield: UILabel!
    @IBOutlet weak var lblProfit: UILabel!
    @IBOutlet weak var chartWin: PieChartView!
    @IBOutlet weak var chartVoid: PieChartView!
    @IBOutlet weak var chartLost: PieChartView!
    @IBOutlet weak var lblWon: UILabel!
    @IBOutlet weak var lblWonData: UILabel!
    @IBOutlet weak var lblVoid: UILabel!
    @IBOutlet weak var lblVoidData: UILabel!
    @IBOutlet weak var lblLost: UILabel!
    @IBOutlet weak var lblLostData: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    //Button Outlets
    @IBOutlet weak var btnFilterOne: UIButton!
    @IBOutlet weak var btnFilterTwo: UIButton!
    @IBOutlet weak var btnFilterThree: UIButton!
    @IBOutlet weak var btnFIlterFour: UIButton!
    @IBOutlet weak var viewFilterContainer: UIView!
    //Constraints
    @IBOutlet weak var constraintChartsConteinerTop: NSLayoutConstraint!
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayButton = [btnFilterOne, btnFilterTwo, btnFilterThree, btnFIlterFour]
        chartWin.delegate = self
        chartLost.delegate = self
        chartVoid.delegate = self
        constraintChartsConteinerTop.constant = self.view.frame.height * 0.22
        lblFilterExplainer.text = "Ολα"
        swipeSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    self.view.backgroundColor = UIColor.white
    UINavigationBar.appearance().isTranslucent = false
    self.navigationController?.navigationBar.isTranslucent = false
    self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 41.0/255.0, alpha: 1.0)
        open = appdelegat.is_side_menu_open
        getGeneralStatistics()
        btnFilterOne.titleLabel?.font  = UIFont.boldSystemFont(ofSize: 15)
        constraintChartsConteinerTop.constant = self.view.frame.height * 0.25
        chartWin.frame.size.width = 20
        chartWin.frame.size.height = chartWin.frame.size.width
    }
    //MARK: Actions
    @IBAction func openSideMenu(_ sender: Any) {
        self.view.backgroundColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 41.0/255.0, alpha: 1.0)
        appdelegat.payload = 0
        appdelegat.offset = 0
        if !open {
            self.captureScreen()
            self.showMenu(sender as! UIButton)
            open = true
        }
        else{
            self.closeMenu(sender as! UIButton)
            open = false
        }
    }
    
    @IBAction func actionFilterOne(_ sender: UIButton) {
        if activeFilterTag != 0{
            activeFilterTag = 0
            pageControl.currentPage = activeFilterTag
            changeFilterLabelText(activeFilterTag: activeFilterTag)
            let filteredBold = arrayButton.filter{($0.tag == activeFilterTag)}
            filteredBold.first?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            
            let filteredRegular = arrayButton.filter{($0.tag != activeFilterTag)}
            for button in filteredRegular {
                button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            }
            
            filterByTime(tag: sender.tag)
        }
    }
    @IBAction func actionFilterTwo(_ sender: UIButton) {
        if activeFilterTag != 1{
            activeFilterTag = 1
            pageControl.currentPage = activeFilterTag
            changeFilterLabelText(activeFilterTag: activeFilterTag)
            let filteredBold = arrayButton.filter{($0.tag == activeFilterTag)}
            filteredBold.first?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            
            let filteredRegular = arrayButton.filter{($0.tag != activeFilterTag)}
            for button in filteredRegular {
                button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            }


            filterByTime(tag: sender.tag)
        }
    }
    @IBAction func actionFilterThree(_ sender: UIButton) {
        if activeFilterTag != 2{
            activeFilterTag = 2
            pageControl.currentPage = activeFilterTag
            changeFilterLabelText(activeFilterTag: activeFilterTag)
            let filteredBold = arrayButton.filter{($0.tag == activeFilterTag)}
            filteredBold.first?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            
            let filteredRegular = arrayButton.filter{($0.tag != activeFilterTag)}
            for button in filteredRegular {
                button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            }
            filterByTime(tag: sender.tag)
        }
    }
    @IBAction func actionFilterFour(_ sender: UIButton) {
        if activeFilterTag != 3{
            activeFilterTag = 3
            pageControl.currentPage = activeFilterTag
            changeFilterLabelText(activeFilterTag: activeFilterTag)
            let filteredBold = arrayButton.filter{($0.tag == activeFilterTag)}
            filteredBold.first?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            
            let filteredRegular = arrayButton.filter{($0.tag != activeFilterTag)}
            for button in filteredRegular {
                button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            }

            filterByTime(tag: sender.tag)
        }
    }
    
    @IBAction func filterStatistics(_ sender: Any) {
        let currentDateTime = NSDate()
        print(currentDateTime)
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            generalStatistics = true
            generalOrPersonalStatistics()
        case 1:
            generalStatistics = false
            generalOrPersonalStatistics()
        default:
            break;
        }
    }
    
    //MARK: Functions
    func changeFilterLabelText(activeFilterTag : Int){
        switch activeFilterTag {
        case 0:
            lblFilterExplainer.text = "Ολα"
        case 1:
            lblFilterExplainer.text = "Μια Εβδομάδ"
        case 2:
            lblFilterExplainer.text = "Ένας Μήνας"
        case 3:
            lblFilterExplainer.text = "Τρείς Μήνες"
        default:
            break
        }
    }
    //MARK: Swipe
    func swipeSetup(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            print("Swiped right")
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if activeFilterTag > 0{
                    activeFilterTag -= 1
                    changeFilterLabelText(activeFilterTag: activeFilterTag)
                    let filteredBold = arrayButton.filter{($0.tag == activeFilterTag)}
                    filteredBold.first?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
                    
                    let filteredRegular = arrayButton.filter{($0.tag != activeFilterTag)}
                    for button in filteredRegular {
                        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    }
                    filterByTime(tag: activeFilterTag)
                    pageControl.currentPage = activeFilterTag
                    
                }
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                if activeFilterTag < 3{
                    activeFilterTag += 1
                    changeFilterLabelText(activeFilterTag: activeFilterTag)
                    let filteredBold = arrayButton.filter{($0.tag == activeFilterTag)}
                    filteredBold.first?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
                    
                    let filteredRegular = arrayButton.filter{($0.tag != activeFilterTag)}
                    for button in filteredRegular {
                        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    }
                    filterByTime(tag: activeFilterTag)
                    pageControl.currentPage = activeFilterTag
                    
                }
                
            default:
                break
            }
        }
    }
    func generalOrPersonalStatistics(){
        if generalStatistics == true{
            getGeneralStatistics()
        }
        else {
            getPersonalStatistics()
        }
    }
    //MARK: Setup filters deisgn and behavior
    func filtersDesignAndBehavior(filterTagOne: Int,filterTagTwo: Int, filterTagTemp : Int){
        let filterButtonOne : UIButton!
        let filterButtonTwo : UIButton!
        
    }
    //MARK: Filter by time interval
    func filterByTime(tag : Int){
        let currentDateTime = NSDate()
        switch tag {
        case 0:
            appdelegat.statisticsFilter = ""
            getGeneralStatistics()
        case 1:
            let oneW = currentDateTime.addingTimeInterval(-(7*60*60*24))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let trimedDate = dateFormatter.string(from: oneW as Date)
            print(trimedDate)
            appdelegat.statisticsFilter = "?filters[]=validTo>" + "'" + "\(trimedDate)" + "'"
            NSLog("oneWeek selected " + appdelegat.statisticsFilter)
            getGeneralStatistics()
        case 2:
            let oneM = currentDateTime.addingTimeInterval(-(30*60*60*24))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let trimedDate = dateFormatter.string(from: oneM as Date)
            print(trimedDate)
            appdelegat.statisticsFilter = "?filters[]=validTo>" + "%27" + "\(trimedDate)" + "%27"
            NSLog("twoWeeks selected " + appdelegat.statisticsFilter)
            getGeneralStatistics()
        case 3:
            let threeM = currentDateTime.addingTimeInterval(-(90*60*60*24))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let trimedDate = dateFormatter.string(from: threeM as Date)
            print(trimedDate)
            appdelegat.statisticsFilter = "?filters[]=validTo>" + "%27" + "\(trimedDate)" + "%27"
            NSLog("three months selected " + appdelegat.statisticsFilter)
            getGeneralStatistics()
        default:
            break;
        }
    }
    func getGeneralStatistics(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        self.view.isUserInteractionEnabled = false
        var dataStore = DataStore()
        dataStore.delegat_generalStatistics = self
        dataStore.getGeneralStatistics()
    }
    func getPersonalStatistics(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        var dataStore = DataStore()
        dataStore.delegat_generalStatistics = self
        dataStore.getPersonalStatistics()
    }
    func didGetGeneralStatistics(response: ResponseGeneralStatistics?) {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        self.view.isUserInteractionEnabled = true
        if response != nil {
            if response?.status.key == "OK" {
                roomsStatistics = response!.payload
                selectedRoomStatistic = roomsStatistics[0]// MARK: TODO add filters
                lblWonData.text = selectedRoomStatistic.won
                lblVoidData.text = selectedRoomStatistic.void
                lblLostData.text = selectedRoomStatistic.lost
                
                handlePiecharts(win: Double(selectedRoomStatistic.won)! , void:Double(selectedRoomStatistic.void)!,lost : Double(selectedRoomStatistic.lost)!)
                
                lblYield.text = "\(selectedRoomStatistic.yield!)%"
                lblProfit.text = "\(selectedRoomStatistic.profit!) Μονάδες"
            }
            else if response?.status.key == "TOKEN_EXPIRED"{
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                saveSuspendedDeviceInfo(info: (response?.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
    }
    //MARK: Refresh token
    func refreshToken(){
        //let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_refreshToken = self
        dataStore.refreshToken()
    }
    func didRefreshToken(response: ResponseLogin?) {
        //DataUtility.sharedInstance.hideActivityIndicator(self.view)
        if let _response = response {
            if _response.status.key == "OK"{
                //DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.key)
                saveToken(info: _response.payload.first!)
                //call again
                getGeneralStatistics()
            }
        }
    }
    private func saveSuspendedDeviceInfo(info : GeneralStatistics){
        DataUtility.sharedInstance.saveSuspendedDeviceInfo(username:DataUtility.sharedInstance.getUserCredentials().username, suspended_until: info.suspended_until)
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
    }
    //MARK: PieCharts Data
    func handlePiecharts(win: Double , void:Double,lost : Double){
        
        winData.removeAll()
        lostData.removeAll()
        voidData.removeAll()
        
        
        let total =  win + void + lost
        
        let diffWin = total - win
        let diffVoid = total - void
        let diffLost = total - lost
        
        
        
        winData.append(win)
        winData.append(diffWin)
        
        voidData.append(void)
        voidData.append(diffVoid)
        
        
        lostData.append(lost)
        lostData.append(diffLost)
        
        
        setWin(dataPoints: arrayWin, values: winData)
        chartWin.holeRadiusPercent = 0.93
        
        chartWin.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        
        setVoid(dataPoints: arrayVoid, values: voidData)
        chartVoid.animate(xAxisDuration: 1.25, yAxisDuration: 1.25)
        chartVoid.holeRadiusPercent = 0.93
        
        setLost(dataPoints: arrayLost, values: lostData)
        chartLost.holeRadiusPercent = 0.93
        chartLost.animate(xAxisDuration: 1.5, yAxisDuration: 1.5)
        
        
        chartWin.data?.setValueTextColor(UIColor.clear)
        chartWin.legend.enabled = false
        chartWin.highlightPerTapEnabled = false
        
        
        chartVoid.data?.setValueTextColor(UIColor.clear)
        chartVoid.legend.enabled = false
        chartVoid.highlightPerTapEnabled = false
        
        chartLost.data?.setValueTextColor(UIColor.clear)
        chartLost.legend.enabled = false
        chartLost.highlightPerTapEnabled = false
        print("========")
        print("========")
        print("========")
        print(winData)
        print("========")
        print(voidData)
        print("========")
        print(lostData)
        
    }
    
    func setWin(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: values[i], y: values[i])
            dataEntries.append(dataEntry)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: " ")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        chartWin.data = pieChartData
        var colors: [UIColor] = [winColor, platinum]
        pieChartDataSet.colors = colors
        let color = UIColor.black
        colors.append(color)
        
    }
    
    func setVoid(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: values[i], y: values[i])
            dataEntries.append(dataEntry)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: " ")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        chartVoid.data = pieChartData
        var colors: [UIColor] = [voidColor, platinum]
        pieChartDataSet.colors = colors
        let color = UIColor.black
        colors.append(color)
        
    }
    
    func setLost(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: values[i], y: values[i])
            dataEntries.append(dataEntry)
        }
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: " ")
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        chartLost.data = pieChartData
        var colors: [UIColor] = [lostColor, platinum]
        pieChartDataSet.colors = colors
        let color = UIColor.black
        colors.append(color)
        
    }

}
