//
//  Faq.swift
//  Prognostika
//
//  Created by Cope on 3/31/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation

struct Faq {
    var question : String!
    var answer : String!
    var priority : Int!
    var id : Int!
    //SUSPENDED
    var suspended_until : String!
    
    init(fromDictionary dictionary : NSDictionary){
        question = dictionary["question"] as! String
        answer = dictionary["answer"] as! String
        priority = dictionary["priority"] as! Int
        id = dictionary["id"] as! Int
        if suspended_until != nil {
        suspended_until = dictionary["suspended_until"] as! String
        }
    }
    
    func toDictionary()->NSDictionary{
        let dictionary = NSMutableDictionary()
        if question != nil{
            dictionary["question"] = question
        }
        if answer != nil{
            dictionary["answer"] = answer
        }
        if priority != nil{
            dictionary["priority"] = priority
        }
        if id != nil{
            dictionary["id"] = id
        }
        if suspended_until != nil{
            dictionary["suspended_util"] = suspended_until
        }
        return dictionary
    }
}
