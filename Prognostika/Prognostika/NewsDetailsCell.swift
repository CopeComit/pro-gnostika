//
//  NewsDetailsCell.swift
//  Prognostika
//
//  Created by Cope on 3/8/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class NewsDetailsCell: UITableViewCell {
    @IBOutlet weak var lbl_validUntil: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_won: UILabel!
    @IBOutlet weak var lbl_void: UILabel!
    @IBOutlet weak var lbl_lost: UILabel!
    @IBOutlet weak var lbl_news_text: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
