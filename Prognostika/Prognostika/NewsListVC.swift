//
//  NewsListVC.swift
//  Prognostika
//
//  Created by Cope on 3/6/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit
import StoreKit

class NewsListVC: SideMenu, UITabBarDelegate, UITableViewDataSource, UITableViewDelegate, ProtocolPredictions, ProtocolRefreshToken, ProtocolUserPredictions, ProtocolPatchUserProfile, ProtocolUserProfile{
    //MARK:Variables
    var array_myRoomPredictions =  [Predictions]()
    var selectedPrediction : Predictions!
    var open : Bool!
    var isActivePrediction = Int()
    var products = [SKProduct]()
    var refreshData = false
    private let refreshControl = UIRefreshControl()
    var userProfile =  [UserProfile]()
    @IBOutlet weak var tableViewIAP: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: Outlets
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblNoActivePredictions: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var lblRemainingUnits: UILabel!
    @IBOutlet weak var viewBottomRemainingUnits: UIView!
    //MARK: Custom popup
    @IBOutlet weak var btnBuyMoreUnits: UIButton!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet var viewIAPDialog: UIView!
    var effect : UIVisualEffect!
    @IBOutlet var viewFirstCustomPopUp: UIView!
    //open new inapppurchase dialog
    @IBAction func ActionCloseSecondIAPDialog(_ sender: Any) {
        animateSecondIAPDialogOut()
        
    }
    @IBAction func ActionOpenIAPDialog(_ sender: Any) {
        animateOutForSecondView()
        animateSecondIAPDialogIn()
    }
    //First IAP Dialog
    @IBAction func ActionCloseFirstDialog(_ sender: Any) {
        //appdelegat.dontShowIAPDialog = true
        DataUtility.sharedInstance.saveShowIAPDialogValue(boolValue: false)
        animateOut()
    }
    
    func animateIn(){
        self.view.addSubview(viewFirstCustomPopUp)
        visualEffectView.isHidden = false
        viewFirstCustomPopUp.center = self.view.center
        viewFirstCustomPopUp.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewFirstCustomPopUp.alpha = 0
        UIView.animate(withDuration: 0.4) { 
            self.visualEffectView.effect = self.effect
            self.viewFirstCustomPopUp.alpha = 1
            self.viewFirstCustomPopUp.transform = CGAffineTransform.identity
        }
    }
    func animateOut(){
        UIView.animate(withDuration: 0.3, animations: {
            self.viewFirstCustomPopUp.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.viewFirstCustomPopUp.alpha = 0
            self.visualEffectView.effect = nil
            
        }) { (Bool) in
            self.viewFirstCustomPopUp.removeFromSuperview()
            self.visualEffectView.isHidden = true
        }
    }
    func animateOutForSecondView(){
        UIView.animate(withDuration: 0.3, animations: {
            self.viewFirstCustomPopUp.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.viewFirstCustomPopUp.alpha = 0
            //self.visualEffectView.effect = nil
            
        }) { (Bool) in
            self.viewFirstCustomPopUp.removeFromSuperview()
            //self.visualEffectView.isHidden = true
        }
    }
    //Second IAP Dialog
    func animateSecondIAPDialogIn(){
        self.view.addSubview(viewIAPDialog)
        visualEffectView.isHidden = false
        viewIAPDialog.center = self.view.center
        viewIAPDialog.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewIAPDialog.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.visualEffectView.effect = self.effect
            self.viewIAPDialog.alpha = 1
            self.viewIAPDialog.transform = CGAffineTransform.identity
        }
    }
    func animateSecondIAPDialogOut(){
        UIView.animate(withDuration: 0.3, animations: {
            self.viewIAPDialog.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.viewIAPDialog.alpha = 0
            self.visualEffectView.effect = nil
            
        }) { (Bool) in
            self.viewIAPDialog.removeFromSuperview()
            self.visualEffectView.isHidden = true
        }
    }
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(NewsListVC.methodOfReceivedNotification(notification:)), name: Notification.Name("PatchUserUnitsAfterIAP"), object: nil)
        appdelegat.titleForVC(vc: self, title: "News list",isTransparent: false)
        table.rowHeight = UIScreen.main.bounds.height * 0.404
        table.allowsSelection = false
        tableViewIAP.allowsSelection = false
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        // Configure Refresh Control
        setupRefreshControl()
        refreshControl.addTarget(self, action: #selector(NewsListVC.refreshData(sender:)), for: .valueChanged)
        //Custom PopUp Setup
        tableViewIAP.tableFooterView = UIView()
        //tableViewIAP.tableFooterView = UIView(frame: .zero)
        viewIAPDialog.layer.cornerRadius = 5
        effect = visualEffectView.effect!
        visualEffectView.effect = nil
        visualEffectView.layer.cornerRadius = 5
        btnBuyMoreUnits.layer.cornerRadius = 5
    }
    override func viewWillAppear(_ animated: Bool) {
        isActivePrediction = appdelegat.isActivePrediction
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 41.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        open = appdelegat.is_side_menu_open
        array_myRoomPredictions = []
        appdelegat.offset = 0
        appdelegat.payload = 0
        getPredictions()
    }
    override func viewWillDisappear(_ animated: Bool) {
        appdelegat.offset = 0
        appdelegat.payload = 0
        open = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        reload()
    }
    
    //MARK: Actions
    @IBAction func ActionSideMenu(_ sender: UIButton) {
        appdelegat.payload = 0
        appdelegat.offset = 0
        if !open {
            self.captureScreen()
            self.showMenu(sender)
            open = true
        }
        else{
            self.closeMenu(sender)
            open = false
        }
    }
    @IBAction func actionDetails(_ sender: Any) {
        //array_myRoomPredictions[0].remaining units > 0
        
        selectedPrediction = array_myRoomPredictions[(sender as AnyObject).tag]
        if isActivePrediction == 1{
            appdelegat.isPending = false
            if (selectedPrediction.content == nil || selectedPrediction.content == ""){
                if  DataUtility.sharedInstance.getShowIAPDialogValue() == nil || DataUtility.sharedInstance.getShowIAPDialogValue() == true {
                    animateIn()
                    return
                }
                else if DataUtility.sharedInstance.getShowIAPDialogValue() != nil && DataUtility.sharedInstance.getShowIAPDialogValue() == false{
                    animateSecondIAPDialogIn()
                    return
                }

            }
        }
        else if isActivePrediction == 2{
            appdelegat.isPending = true
        }
        if appdelegat.lblValidUntil != nil {
        appdelegat.lblValidUntil = selectedPrediction.validTo
        }
        else {
            appdelegat.lblValidUntil = ""
        }
        self.performSegue(withIdentifier: "newsDetails", sender: self)
    }
    
    @IBAction func segmentedControl(_ sender: Any) {
        appdelegat.offset = 0
        appdelegat.payload = 0
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            print("Active")
            array_myRoomPredictions = []
            appdelegat.isActivePrediction = 1
            isActivePrediction = appdelegat.isActivePrediction
            //table.setContentOffset(CGPoint.zero, animated: true)
            getPredictions()
        case 1:
            array_myRoomPredictions = []
            print("Pending")
            appdelegat.isActivePrediction = 2
            isActivePrediction = appdelegat.isActivePrediction
            //table.setContentOffset(CGPoint.zero, animated: true)
            getPredictions()
        case 2:
            array_myRoomPredictions = []
            print("Pending")
            appdelegat.isActivePrediction = 3
            isActivePrediction = appdelegat.isActivePrediction
            //table.setContentOffset(CGPoint.zero, animated: true)
            getPredictions()
        default:
            break;
        }

    }
    //MARK: Functions
    //MARK: Get user profile
    func getUserProfile(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        var dataStore = DataStore()
        dataStore.delegat_userProfile = self
        dataStore.getUserProfile()
    }
    func didGetUserProfile(response: ResponseUserProfile?) {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        if response != nil {
            if response?.status.key == "OK" {
                for profile in (response?.payload)! {
                    userProfile.append(profile)
                    appdelegat.userProfile.append(profile)
                    if profile.remaining_units != nil {
                        lblRemainingUnits.text = String(profile.remaining_units!)
                    }
                    else {
                        lblRemainingUnits.text = "0"
                    }
                    
                }
                //userProfile = response?.payload.first
                //fillData()
            }
            else if response?.status.key == "TOKEN_EXPIRED" {
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                //MARK: TODO
                //saveSuspendedDeviceInfo(info: (response?.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
    }

    //MARK: Patch user after purchase
    func patchUserProfile(){
        let userId : String = String(describing: userProfile.first!.id!)
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        var dataStore = DataStore()
        dataStore.delegat_patchUserProfile = self
        dataStore.patchUserProfile(info: preparePatchIAPJSON(), id : userId)
    }
    func didPatchUserProfile(response: ResponseUserProfile?) {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        //MOVE THIS TO if response?.status.key == "OK" when Puza fix BO
        getPredictions()
        animateSecondIAPDialogOut()
        if response != nil {
            if response?.status.key == "OK" {
//                getPredictions()
//                animateOutForSecondView()
                
            }
            else if response?.status.key == "TOKEN_EXPIRED" {
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                saveSuspendedDeviceInfoForUser(info: (response?.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
    }
    func preparePatchIAPJSON()->Dictionary<String,Any>{
        var json = Dictionary<String,Any>()
        var user_form = Dictionary<String,Any>()
        var general_form = Dictionary<String,Any>()
        var remainingUnits : String!
        if array_myRoomPredictions.first?.remUnits == nil{
            remainingUnits = "0"
        }
        else {
            remainingUnits = (array_myRoomPredictions.first?.remUnits)!
        }
        var addedUnits = appdelegat.pricePublicValueForUserPatch
        addedUnits = addedUnits?.substring(to: (addedUnits?.index(before: (addedUnits?.endIndex)!))!)
        let newValueForUnits = Float(remainingUnits)! + Float(addedUnits!)!
        let newValueForUnitsString = String(newValueForUnits)
        general_form["_format"] = "json"
        json["remainingUnits"] = newValueForUnitsString
//        let userId : String = String(describing: userProfile.first!.id!)
//        json["id"] = userId
        user_form["user"] = json
        user_form["_format"] = "json"
//        let userId : String = String(describing: userProfile.first!.id!)
//        json["user"] = userId 
        return user_form as JSON
    }
    func methodOfReceivedNotification(notification: Notification){
        //Take Action on Notification
        patchUserProfile()
    }
    
    //remove notification after editing
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    
    func reload() {
        products = []
        
        tableViewIAP.reloadData()
        
        PrognostikaProducts.store.requestProducts{success, products in
            if success {
                self.products = products!
                self.products = (products?.sorted(by: { (product1, product2) -> Bool in
                    return product1.price.compare(product2.price) == .orderedAscending
                }))!
                /*
                 filteredArrayTasks = _arrayOfTempTasks.sorted(by: { (task1, task2) -> Bool in
                 //return task1.executedDate.compare(task2.executedDate) == .orderedAscending
                 return task1.completedAtDate.compare(task2.completedAtDate) == .orderedDescending
                 })
                 */
                self.tableViewIAP.reloadData()
            }
            
            self.refreshControl.endRefreshing()
        }
    }
    @objc private func refreshData(sender: Any!){
        refreshData = true
        appdelegat.offset = 0
        appdelegat.payload = 0
        getPredictions()
    }
    //Prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newsDetails"{
            let destinationVC = segue.destination as! NewsDetailsVC
            destinationVC.activePrediction = selectedPrediction
            appdelegat.activeSportImageLarge = selectedPrediction.sportTitle?.lowercased()
        }
    }
    func getPredictions(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        var dataStore = DataStore()
        dataStore.delegat_predictions = self
        if refreshData == true {
            array_myRoomPredictions.removeAll()
            refreshData = false
        }
        if isActivePrediction == 1 {
            dataStore.getPredictions(isPredictionRoom: 1)
        }else if isActivePrediction == 2 {
            dataStore.getPredictions(isPredictionRoom: 2)
        }
        else if isActivePrediction == 3{
            dataStore.getPredictions(isPredictionRoom: 3)
        }
    }
    func didGetPredictions(response: ResponsePredictions?) {
        //FIXME: Check are all fields are filled when we get data from server, there is posibility that final score is different.
       print(response)
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        self.refreshControl.endRefreshing()
        var offset : Int = 0
        if response != nil {
            if response?.status.key == "OK" {
                getUserProfile()
                for prediction in (response?.payload)! {
                    array_myRoomPredictions.append(prediction)
                }
                if array_myRoomPredictions.count > 0 {
                    lblNoActivePredictions.isHidden = true
                }else {
                    lblNoActivePredictions.isHidden = false
                    switch isActivePrediction {
                    case 1:
                        lblNoActivePredictions.text = "Επί του παρόντος δεν υπάρχουν πλέον ενεργά στοιχήματα."
                    case 2:
                        lblNoActivePredictions.text = "Επί του παρόντος, δεν υπάρχουν πλέον εκκρεμείς στοιχήματα."
                    case 3:
                        lblNoActivePredictions.text = "Επί του παρόντος, δεν υπάρχουν περισσότερα βαθμολογούμενα στοιχήματα."
                    default:
                         lblNoActivePredictions.text = ""
                    }
                    //lblNoActivePredictions.text = "There are no active predictions"
                    
                }
                var pagination : Int!
                if (response?.pagination.count)! <= 1 {
                    pagination = 0
                    offset = 0
                    appdelegat.payload = pagination
                    appdelegat.offset = offset
                    self.table.reloadData()
                    return
                }
                pagination = (response?.pagination.count)! - 1
                offset = pagination + offset
                appdelegat.payload += pagination
                appdelegat.offset += offset
                
                
                
                self.table.reloadData()
                
            }
            else if response?.status.key == "TOKEN_EXPIRED" {
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                saveSuspendedDeviceInfo(info: (response?.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
    }
    //MARK: Send user predictions
    func sendPrediction(){
        if selectedPrediction.userPredictionID == nil {
            postUserPredictions()
        }
        else{
            patchUserPredictions()
        }
    }
    func postUserPredictions(){
        var dataStore = DataStore()
        dataStore.delegat_userPredictions = self
        dataStore.postUserPredictions(info: preparePostJSON())
    }
    func patchUserPredictions(){
        var dataStore = DataStore()
        dataStore.delegat_userPredictions = self
        dataStore.patchUserPredictions(info: preparePatchJSON(), id: selectedPrediction.userPredictionID)
    }
    func preparePostJSON()->Dictionary<String,Any>{
        var json = Dictionary<String,Any>()
        var userPrediction = Dictionary<String,Any>()
        userPrediction["user"] = selectedPrediction.userId
        userPrediction["prediction"] = String(selectedPrediction.id)
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        userPrediction["deletedAt"] = result
        json["user_prediction"] = userPrediction
        
        return json
    }
    func preparePatchJSON()->Dictionary<String,Any>{
        var json = Dictionary<String,Any>()
        var userPrediction = Dictionary<String,Any>()
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        userPrediction["deletedAt"] = result
        json["user_prediction"] = userPrediction
        
        return json
    }
    func didSendUserPredictions(response: ResponsePredictions?) {
        if response != nil {
            if response?.status.key == "OK" {
                print("OK")
            }
            else if response?.status.key == "TOKEN_EXPIRED" {
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                saveSuspendedDeviceInfo(info: (response?.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
        table.reloadData()
    }

    
    //MARK: Refresh token
    func refreshToken(){
        //let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_refreshToken = self
        dataStore.refreshToken()
    }
    func didRefreshToken(response: ResponseLogin?) {
        if let _response = response {
            if _response.status.key == "OK"{
                //DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.key)
                saveToken(info: _response.payload.first!)
                //call again
                getPredictions()
            }
        }
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
    }
    //MARK: Save suspended device
    private func saveSuspendedDeviceInfo(info : Predictions){
        DataUtility.sharedInstance.saveSuspendedDeviceInfo(username:DataUtility.sharedInstance.getUserCredentials().username, suspended_until: info.suspended_until)
    }
    private func saveSuspendedDeviceInfoForUser(info : UserProfile){
        DataUtility.sharedInstance.saveSuspendedDeviceInfo(username:DataUtility.sharedInstance.getUserCredentials().username, suspended_until: info.suspended_until)
    }
    //MARK: Table
    func setupRefreshControl(){
        if #available(iOS 10.0, *) {
            table.refreshControl = refreshControl
        } else {
            table.addSubview(refreshControl)
        }
        refreshControl.tintColor = UIColor(red:20.0/255.0, green:26.0/255.0, blue:42.0/255.0, alpha:1.0)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewIAP{
            return products.count
        }
        else {
            return array_myRoomPredictions.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewIAP{
            let cell = tableView.dequeueReusableCell(withIdentifier: "IAPCell", for: indexPath) as! IAPCell
            let product = products[(indexPath as NSIndexPath).row]
            
            cell.product = product
            
            switch indexPath.row {
            case 0:
                cell.lblUnits.textColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 0.4)
                cell.lblPacketName.textColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 0.4)
            case 1:
                cell.lblUnits.textColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 0.7)
                cell.lblPacketName.textColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 0.7)
            case 2:
                cell.lblUnits.textColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
                cell.lblPacketName.textColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
                cell.imgUnderline.isHidden = true
            case 3:
                cell.lblUnits.textColor = UIColor(colorLiteralRed: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                cell.lblPacketName.textColor = UIColor(colorLiteralRed: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                cell.backgroundColor = UIColor(colorLiteralRed: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                cell.imgUnderline.isHidden = true
            default:
                print("there are no")
            }
            var packageName = product.localizedTitle
            if let upperBound = packageName.range(of: " ")?.upperBound {
                print(packageName.substring(to: upperBound))
                packageName = packageName.substring(to: upperBound)
            }
            cell.lblPacketName.text = packageName//product.localizedTitle
            cell.buyButtonHandler = { product in
                PrognostikaProducts.store.buyProduct(product)

             
            }
            return cell
        }
        else {
            //let shadowColor = UIColor(colorLiteralRed: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 1)
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsListCell")! as! NewsListCell
            if array_myRoomPredictions.count == 0 {
                return cell
            }
            if appdelegat.isActivePrediction == 3{
                self.table.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)

                viewBottomRemainingUnits.isHidden = true
                //cell.img_cellShadow.addShadowImage(shadowStyle: .ShadowFourSides, shadowColor: UIColor.clear, shadowRadius: 5)
                let blackColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
                let blackColorTransparent = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 0.5)
                let whiteColor = UIColor(colorLiteralRed: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                let whiteColorTransparent = UIColor(colorLiteralRed: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.5)
                
                let outcomesCell = tableView.dequeueReusableCell(withIdentifier: "OutcomesCell")! as! OutcomesCell
                let singleOutcome = array_myRoomPredictions[indexPath.row]
                if singleOutcome.outcome_title == "W"{
                    outcomesCell.img_cellShadow.backgroundColor = UIColor(colorLiteralRed: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1.0)
                    outcomesCell.imgOutcomeIndicator.image = UIImage(named: "won_obj")
                    outcomesCell.lblLeague.textColor = whiteColorTransparent
                    outcomesCell.lblBet.textColor = whiteColor
                    outcomesCell.lblOdd.textColor = whiteColorTransparent
                    outcomesCell.lblOddData.textColor = whiteColor
                    outcomesCell.lblStake.textColor = whiteColorTransparent
                    outcomesCell.lblStakeData.textColor = whiteColor
                    outcomesCell.lblProfit.textColor = whiteColorTransparent
                    outcomesCell.lblProfitData.textColor = whiteColor
                    outcomesCell.lblOutcomeTitle.textColor = whiteColor
                    outcomesCell.lblResult.textColor = whiteColor
                    outcomesCell.imgLineOne.backgroundColor = whiteColorTransparent
                    outcomesCell.imgLineTwo.backgroundColor = whiteColorTransparent
                    outcomesCell.imgLineThree.backgroundColor = whiteColorTransparent
                    outcomesCell.imgLineFour.backgroundColor = whiteColorTransparent
                    outcomesCell.lblOutcomeTitle.text =  "Κερδισμένο"
                }
                else if singleOutcome.outcome_title == "V"{
                    outcomesCell.img_cellShadow.backgroundColor = UIColor(colorLiteralRed: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
                    outcomesCell.imgOutcomeIndicator.image = UIImage(named: "void_obj")
                    outcomesCell.lblLeague.textColor = blackColorTransparent
                    outcomesCell.lblBet.textColor = blackColor
                    outcomesCell.lblOdd.textColor = blackColorTransparent
                    outcomesCell.lblOddData.textColor = blackColor
                    outcomesCell.lblStake.textColor = blackColorTransparent
                    outcomesCell.lblStakeData.textColor = blackColor
                    outcomesCell.lblProfit.textColor = blackColorTransparent
                    outcomesCell.lblProfitData.textColor = blackColor
                    outcomesCell.lblOutcomeTitle.textColor = blackColor
                    outcomesCell.lblResult.textColor = blackColor
                    outcomesCell.imgLineOne.backgroundColor = blackColorTransparent
                    outcomesCell.imgLineTwo.backgroundColor = blackColorTransparent
                    outcomesCell.imgLineThree.backgroundColor = blackColorTransparent
                    outcomesCell.imgLineFour.backgroundColor = blackColorTransparent
                    outcomesCell.lblOutcomeTitle.text =  "Άκυρο"

                }
                if singleOutcome.outcome_title == "L"{
                    outcomesCell.img_cellShadow.backgroundColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 0.87)
                    outcomesCell.imgOutcomeIndicator.image = UIImage(named: "lost_obj")
                    outcomesCell.lblLeague.textColor = whiteColorTransparent
                    outcomesCell.lblBet.textColor = whiteColor
                    outcomesCell.lblOdd.textColor = whiteColorTransparent
                    outcomesCell.lblOddData.textColor = whiteColor
                    outcomesCell.lblStake.textColor = whiteColorTransparent
                    outcomesCell.lblStakeData.textColor = whiteColor
                    outcomesCell.lblProfit.textColor = whiteColorTransparent
                    outcomesCell.lblProfitData.textColor = whiteColor
                    outcomesCell.lblOutcomeTitle.textColor = whiteColor
                    outcomesCell.lblResult.textColor = whiteColor
                    outcomesCell.imgLineOne.backgroundColor = whiteColorTransparent
                    outcomesCell.imgLineTwo.backgroundColor = whiteColorTransparent
                    outcomesCell.imgLineThree.backgroundColor = whiteColorTransparent
                    outcomesCell.imgLineFour.backgroundColor = whiteColorTransparent
                    outcomesCell.lblOutcomeTitle.text =  "Χαμένο"
                }
    //             outcomesCell.img_cellShadow.addShadowImage(shadowStyle: .ShadowFourSides, shadowColor: shadowColor, shadowRadius: 5)
                
                outcomesCell.lblLeague.text = singleOutcome.leagueTitle + " (" + singleOutcome.countryTitle  + ", " + singleOutcome.sportTitle + ")"
                outcomesCell.lblBet.text = "Στοίχημα: \(singleOutcome.bet!)"//singleOutcome.title
                outcomesCell.lblOddData.text = String(singleOutcome.odd)
                outcomesCell.lblStakeData.text = String(singleOutcome.stake)
                outcomesCell.lblProfitData.text = String(singleOutcome.profit)
                //outcomesCell.lblOutcomeTitle.text =  singleOutcome.outcome_title
                if singleOutcome.result != nil{
                    outcomesCell.lblResult.text = "Τελικό Αποτέλεσμα: " + singleOutcome.result
                }
                return outcomesCell
            }
            else {
                viewBottomRemainingUnits.isHidden = false
                self.table.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: self.viewBottomRemainingUnits.frame.size.height + 2,right: 0)
                let entry = array_myRoomPredictions[indexPath.row]
                //lblRemainingUnits.text = entry.remUnits
                if entry.seenAt != nil && entry.seenAt != ""{
                    // add the shadow to the base view
                    //img_cellShadow.backgroundColor = UIColor.white
                    cell.lbl_price.text = "Bought"
                    cell.lbl_price.textColor = UIColor(colorLiteralRed: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
                    cell.lbl_price.font = UIFont.systemFont(ofSize: 18)
                    cell.img_cellShadow.alpha = 0.5
                    cell.img_newsSport.alpha = 0.5
                    cell.img_cellShadow.layer.shadowColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 0.85).cgColor
                    cell.img_cellShadow.layer.shadowOffset = CGSize(width: 0, height: 1)
                    cell.img_cellShadow.layer.shadowOpacity = 0.7
                    cell.img_cellShadow.layer.shadowRadius = 1.0
                    
                    // improve performance
                    cell.img_cellShadow.layer.shadowPath = UIBezierPath(roundedRect: cell.img_cellShadow.bounds, cornerRadius: 10).cgPath
                    cell.img_cellShadow.layer.shouldRasterize = true
                    cell.img_cellShadow.layer.rasterizationScale = UIScreen.main.scale
                    //end of shadow implementation
                    cell.btn_buy.isHidden = true
                    cell.btn_details.isHidden = false
                    cell.btn_buy.tag = indexPath.row
                    cell.lbl_validUntil.text = "Εγκυρο μέχρι: " + entry.validTo
                    cell.lbl_newsTitle.text = entry.title
                    cell.lbl_newsText.text = entry.teaser
                }
                else  {
                    cell.btn_buy.isHidden = false
                    cell.btn_details.isHidden = true
                    cell.img_cellShadow.alpha = 1
                    cell.img_newsSport.alpha = 1
                    cell.btn_buy.tag = indexPath.row
                    cell.lbl_validUntil.text = "Εγκυρο μέχρι: " + entry.validTo
                    cell.lbl_newsTitle.text = entry.title
                    cell.lbl_newsText.text = entry.teaser
                    if  entry.priceValue != nil {
                        cell.lbl_price.text = entry.priceValue + " units"
                    }
                }
                let activeSport = entry.sportTitle?.lowercased()
                if array_myRoomPredictions.count > 0 {
                    cell.img_newsSport.image = UIImage(named: "\(activeSport!)_small")
                }
                
            }
            return cell
        }
    }
    //Delete
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if appdelegat.isActivePrediction == 1{
            return true
        }
        else{
            return false
        }
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if appdelegat.isActivePrediction == 1{
            if (editingStyle == UITableViewCellEditingStyle.delete) {
                // handle delete (by removing the data from your array and updating the tableview)
                
                selectedPrediction = array_myRoomPredictions[indexPath.row]
                array_myRoomPredictions.remove(at: indexPath.row)
                sendPrediction()
            }
        }
    }
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Διαγραφή \n πρόβλεψης" //or customize for each indexPath
    }
}
