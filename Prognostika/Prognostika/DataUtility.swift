//
//  Utility.swift
//  VQuarter
//
//  Created by Branko Grbic on 11/18/2016.
//  Copyright (c) 2016 ComITGroup. All rights reserved.
//

import UIKit

typealias PrintDebugMessage             = String
typealias DebugMessage                  = String

enum NetworkErrors: String ,  Error {
    
    case ErrorResponse              = "ERROR: Server Response!"
    case ErrorJSON                  = "ERROR: JsonSerialization!"
    case ErrorData                  = "ERROR: No data!"
    case UnknownError               = "ERROR: Unknown error!"
    case ErrorLoadImages            = "Can't load images!"
    case noNetworkMsg               = "No Network!"
    case Info                       = "Info"
    case Error                      = "Error"
    case Loading                    = "Loading"
    case InternetIsBroken           = "Internet connection is broken!"
    case InternetIsConnected        = "Internet connection is established"
}

enum MessageInfo : String {
    
    case noNetworkMsg                = "No Network"
    case Info                        = "Info"
    case Error                       = "Error"
    case Loading                     = "Loading"
    case ErrorGetToken               = "Error get token!"
    case InternetIsBroken            = "Internet connection is broken !"
    case InternetIsConnected         = "Internet connection is established"
    case EmailIsEmpty                = "Email is empty !"
    case EmailIsNotValid             = "Email is not valid !"
    case PasswordIsEmpty             = "Password is empty !"
    case EmailAndPasswordEmpty       = "You must enter email and password !"
    case NotValidEmail               = "Not valid email !"
    case PasswordIsNotSame           = "Password is not same !"
    case CodeIsEmpty                 = "You must enter code from email"
    case ConfirmYourPassword         = "Confirm your password !"
    case EnterNewPassword            = "Enter new password !"
    case ProblemWithAuthorization    = "Problem with Authorization !"
    case Ok                          = "Ok"
    case Cancel                      = "Cancel"
    case YouMustEnableCoreLocation   = "You must enable CoreLocation Service in Settings"
    case Verify                      = "Verify"
    case Uploading                   = "Uploading"
    case Activate                    = "Activate"
    case YouAreStilNotRegisterUser   = "You are stil not register user!"
    case Download                    = "Download"
    case Sending                     = "Sending"
    case Searching                   = "Searching"
}
enum ResponseMessageInfo : String {
    
    case noNetworkMsg                = "No Network"
    case Info                        = "Info"
    case Error                       = "Error"
    case Loading                     = "Loading"
    case ErrorGetToken               = "Error get token!"
    case InternetIsBroken            = "Internet connection is broken !"
    case InternetIsConnected         = "Internet connection is established"
    case EmailIsEmpty                = "Email is empty !"
    case EmailIsNotValid             = "Email is not valid !"
    case PasswordIsEmpty             = "Password is empty !"
    case EmailAndPasswordEmpty       = "You must enter email and password !"
    case NotValidEmail               = "Not valid email !"
    case PasswordIsNotSame           = "Password is not same !"
    case CodeIsEmpty                 = "You must enter code from email"
    case ConfirmYourPassword         = "Confirm your password !"
    case EnterNewPassword            = "Enter new password !"
    case ProblemWithAuthorization    = "Problem with Authorization !"
    case Ok                          = "Ok"
    case Cancel                      = "Cancel"
    case YouMustEnableCoreLocation   = "You must enable CoreLocation Service in Settings"
    case WrongVerificationNumber     = "Wrong verification number"
    case Verify                      = "Verify"
    case Uploading                   = "Uploading"
    case Activate                    = "Activate"
    case YouAreStilNotRegisterUser   = "You are stil not register user!"
    case Download                    = "Download"
}

func debugprint(_ message : PrintDebugMessage , function : String = #function , file  : String = #file ,  line : Int = #line , column : Int = #column)
{
    print(message + " function : " +  function + " file : " + file + " line: " + "\(line)" + " column: " + "\(column)")
}

let AUTH_KEY                    = "authorization_key"
let CURRENT_USER                = "current_user_key"
let ACTIV_USER                  = "activ_user"
let COUNTRY_ID                  = "country_id"
let TOKEN                       = "token"
let AUTOMATIC_LOGIN             = "automatic_login"
let STATUS_ERROR                = "ERROR"
let STATUS_OK                   = "OK"
let LOCALE_LANGUAGE             = "locale"
let ACCESS_TOKEN                = "ACCESS_TOKEN"
let REFRESH_TOKEN               = "REFRESH_TOKEN"
let ORDINARY_LOGIN              = "ORDINARY_LOGIN"
let USERNAME                    = "USERNAME"
let NAMEANDSURNAME              = "NAMEANDSURNAME"
let PASSWORD                    = "PASSWORD"
let TOKEN_EXPIRED               = "TOKEN_EXPIRED"
let SUSPENDED_UNTIL             = "SUSPENDED_UNIL"
let ROOM_TITLE                  = "ROOM_TITLE"
let ROOM_ID                     = "ROOM_ID"
let DEVICE_INFO                 = "deviceInfo"
let FBTOKEN                     = "FBTOKEN"
let FBID                        = "FBID"
let GOOGLETOKEN                 = "GOOGLETOKEN"
let GOOGLEID                    = "GOOGLEID"
let IAPDIALOGSETTINGSBOOL       = "IAPDIALOGSETTINGSBOOL"
let PROMOCODE                   = "PROMOCODE"

final class DataUtility {
    
    static let sharedInstance = DataUtility()
    
    var container       : UIView!
    var Indicator       : UIActivityIndicatorView!
    var progressView    : UIProgressView!
    
    func ordinaryLoginPass(success : String?){
        UserDefaults.standard.setValue(success, forKey: ORDINARY_LOGIN)
    }
    func getOrdinaryLogin()->String? {
        return UserDefaults.standard.value(forKey: ORDINARY_LOGIN) as! String?
    }
    func savePromoCode(promoCode : String?){
        UserDefaults.standard.setValue(promoCode, forKey: PROMOCODE)
    }
    func getPromoCode()->String? {
        return UserDefaults.standard.value(forKey: PROMOCODE) as! String?
    }
    
    func saveAcccesToken(token : String){
        UserDefaults.standard.setValue(token, forKey: ACCESS_TOKEN)
    }
    func SaveAuthKey(_ auth_key : String)
    {
        UserDefaults.standard.setValue(auth_key, forKey: AUTH_KEY)
        UserDefaults.standard.synchronize()
    }
    func saveRoomInfo(room_title : String? , room_id : String?){
        let ud = UserDefaults.standard
        ud.set(room_title, forKey: ROOM_TITLE)
        ud.set(room_id, forKey: ROOM_ID)
    }
    func getRoomInfo()->(room_title : String? , room_id : String?){
        let ud = UserDefaults.standard
        let _room_title = ud.value(forKey: ROOM_TITLE)as? String
        let _room_id = ud.value(forKey: ROOM_ID)as? String
        return (_room_title,_room_id)
    }
    func saveUserCredentials(username : String , password : String){
        let ud = UserDefaults.standard
        ud.set(username, forKey: USERNAME)
        ud.set(password, forKey: PASSWORD)
    }
    func saveShowIAPDialogValue(boolValue: Bool){
        let ud = UserDefaults.standard
        ud.set(boolValue, forKey: IAPDIALOGSETTINGSBOOL)
    }
    func getShowIAPDialogValue()->Bool?{
        let ud = UserDefaults.standard
        let boolValue = ud.value(forKey: IAPDIALOGSETTINGSBOOL) as? Bool
        return boolValue
    }
    func getUserCredentials()->(username : String? , password : String?){
        let ud = UserDefaults.standard
        let _username = ud.value(forKey: USERNAME)as? String
        let _password = ud.value(forKey: PASSWORD)as? String
        return (_username,_password)
    }
    func saveUserAdditionalCredentials(nameAndSurname : String ){
        let ud = UserDefaults.standard
        ud.set(nameAndSurname, forKey: NAMEANDSURNAME)
    }
    func getUserAdditionalCredentials()->(String?){
        let ud = UserDefaults.standard
        let _nameAndSurname = ud.value(forKey: NAMEANDSURNAME) as? String
        return _nameAndSurname
    }
    func getDeviceInfo()->Dictionary<String,Any>{
        return UserDefaults.standard.value(forKey: DEVICE_INFO) as! Dictionary<String, Any>
    }
    func saveSuspendedDeviceInfo(username : String? ,suspended_until : String?){
        let ud = UserDefaults.standard
        ud.set(username, forKey: USERNAME)
        ud.set(suspended_until, forKey: SUSPENDED_UNTIL)
    }
    func getSuspendedDeviceInfo()->(String?){
        return UserDefaults.standard.value(forKey: SUSPENDED_UNTIL) as! String?
    }
    
    func checkIfSuspensionDateExpired()->Bool{
        var expired : Bool = true
        //convert string date to nsdate
        let expirationDate = (UserDefaults.standard.value(forKey: SUSPENDED_UNTIL)as? String)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
       // dateFormatter.timeZone = TimeZone(secondsFromGMT: 28800)
        let date = dateFormatter.date(from: expirationDate)
        print(dateFormatter.string(from: date!))
        //Check if date has expired
        let currentDateTime = NSDate()
        // Compare them
        switch currentDateTime.compare(date!) {
            case .orderedAscending     :   print("Current date is earlier than expiration date"); expired = false
            case .orderedDescending    :   print("Current date is later than expiration date"); expired = true
            case .orderedSame          :   print("The two dates are the same"); expired = false
        }
        if expired == true {
            clearSuspendedDeviceInfo()
        }
        return expired
    }
    func clearSuspendedDeviceInfo(){
        let defaultStd = UserDefaults.standard
        defaultStd.setValue(nil, forKey: SUSPENDED_UNTIL)
        defaultStd.synchronize()
    }
    func clearAllLoginTokens(){
        ordinaryLoginPass(success: nil)
        saveFBToken(token: nil)
    }
    //MARK: Facebook token and ID
    func saveFBToken(token : String?){
        UserDefaults.standard.setValue(token, forKey: FBTOKEN)
    }
    func getFBToken()->String? {
        return UserDefaults.standard.value(forKey: FBTOKEN) as! String?
    }
    func saveFBID(fb_id : String?){
        UserDefaults.standard.setValue(fb_id, forKey: FBID)
    }
    func getFBID()->String?{
        return UserDefaults.standard.value(forKey: FBID) as! String?
    }
    //MARK: Google token and ID
    func saveGoogleToken(token : String?){
        UserDefaults.standard.setValue(token, forKey: GOOGLETOKEN)
    }
    func getGoogleToken()->String? {
        return UserDefaults.standard.value(forKey: GOOGLETOKEN) as! String?
    }
    func saveGoogleID(fb_id : String?){
        UserDefaults.standard.setValue(fb_id, forKey: GOOGLEID)
    }
    func getGoogleID()->String?{
        return UserDefaults.standard.value(forKey: GOOGLEID) as! String?
    }
    func getAccessToken()->String?{
        return UserDefaults.standard.value(forKey: ACCESS_TOKEN) as! String?
    }
    func saveRefreshToken(token : String){
        UserDefaults.standard.setValue(token, forKey: REFRESH_TOKEN)
    }
    func getRefreshToken()->String?{
        return UserDefaults.standard.value(forKey: REFRESH_TOKEN) as! String?
    }
    func saveLocalLanguage(locale : String){
        UserDefaults.standard.setValue(locale, forKey: LOCALE_LANGUAGE)
        UserDefaults.standard.synchronize()
    }
    func getLocalLanguage()->String?{
        return UserDefaults.standard.value(forKey: LOCALE_LANGUAGE)as? String
    }
    func setAutomaticLogin(_ automatic_login : Bool){
        let number = NSNumber(value: automatic_login)
        UserDefaults.standard.setValue(number, forKey: AUTOMATIC_LOGIN)
        UserDefaults.standard.synchronize()
    }
    func saveUser(email : String,password : String){
        let defaultStd = UserDefaults.standard
        defaultStd.setValue(email, forKey: "email")
        defaultStd.setValue(password, forKey: "password")
        defaultStd.synchronize()
    }
    func clearUser(){
        let defaultStd = UserDefaults.standard
        defaultStd.setValue(nil, forKey: "email")
        defaultStd.setValue(nil, forKey: "password")
        defaultStd.synchronize()
    }
    func getRememberUser()->(email : String? , password : String?){
        let defaultStd  = UserDefaults.standard
        let email       = defaultStd.value(forKey: "email")as? String
        let password    = defaultStd.value(forKey: "password") as? String
        return(email ,password)
    }
    func appendTokenToUrlPath(_ url : String)->String{
        
        let token = "?token=" + GetAuthKey()!
        return url + token
    }
    func appendToken()->String{
        let    token  = "?token=" + GetAuthKey()!
        let    _tokenAddPercent = token.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed)
        return _tokenAddPercent!
    }
    func getAutomaticLogin()->Bool{
        if let value   =  UserDefaults.standard.value(forKey: AUTOMATIC_LOGIN) as? NSNumber {
            return value.boolValue
        }
        return false
    }
    func GetAuthKey()->String?
    {
        if  let user_auth_key = UserDefaults.standard.value(forKey: AUTH_KEY) as? String {
            return user_auth_key
        }
        return nil
    }
    func clearRememberUser(){
        UserDefaults.standard.set(nil, forKey: "users")
        UserDefaults.standard.synchronize()
    }
    func clearAuthKey()
    {
        UserDefaults.standard.setValue(nil, forKey: AUTH_KEY)
    }
    func ReturnComponents(_ end : String)->DateComponents
    {
        let startDate       = Date()
        let dateFormatter   = DateFormatter()
        let timeZone        = TimeZone(identifier: TimeZone.autoupdatingCurrent.identifier)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = timeZone
        let cal             = Calendar.current
        let unit:NSCalendar.Unit = NSCalendar.Unit.day
        let endDate:Date  = dateFormatter.date(from: end)!
        let components      = (cal as NSCalendar).components(unit, from: startDate, to: endDate, options: [])
        return components
    }
    func showProgressView(_ view : UIView, message : String?)
    {
        progressView            = UIProgressView(progressViewStyle: .default)
        progressView.center     = view.center
        view.addSubview(progressView)
        
        let progressLabel       = UILabel()
        let label_frame         = CGRect(x: view.center.x - 25, y: view.center.y - 50, width: 100, height: 50)
        progressLabel.frame     = label_frame
        view.addSubview(progressLabel)
    }
    func hideProgressView(){
        if progressView != nil {
            progressView.removeFromSuperview()
        }
    }
    func showActivityIndicatory(_ view: UIView , message : String?) {
        container                       = UIView()
        container.frame                 = view.frame
        container.center                = view.center
        container.backgroundColor       = UIColorFromHex(0xffffff, _alpha: 0.3)
        
        let loadingView: UIView         = UIView()
        loadingView.frame               = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center              = view.center
        loadingView.backgroundColor     = UIColorFromHex(0x444444, _alpha: 0.7)
        loadingView.clipsToBounds       = true
        loadingView.layer.cornerRadius  = 10
        
        Indicator       = UIActivityIndicatorView()
        Indicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        Indicator.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        Indicator.center = CGPoint(x: loadingView.frame.size.width / 2,
            y: loadingView.frame.size.height / 2);
        loadingView.addSubview(Indicator)
        
        if message != nil {
            let label       = UILabel(frame: CGRect(x: 10, y: 50, width: 80, height: 30))
            label.textColor = UIColor.white
            label.text      = message
            label.font      = UIFont(name: "HelveticaNeue-Light", size: 15.0)
            loadingView.addSubview(label)
        }
        container.addSubview(loadingView)
        view.addSubview(container)
        Indicator.startAnimating()
    }
    func hideActivityIndicator(_ uiView: UIView) {
        guard Indicator != nil else {return}
        Indicator.stopAnimating()
        container.removeFromSuperview()
    }
    func showMessage(_ title : String , message : String)
    {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: MessageInfo.Ok.rawValue, style: .default, handler: { _ in
            }))
            if let vc =  UIApplication.topViewController()
            {
                if Thread.isMainThread == true {
                        vc.present(alert, animated: true, completion: nil)
                }else {
                    mainTask({ () -> () in
                        vc.present(alert, animated: true, completion: nil)
                    })
                }
            }
    }
    func UIColorFromHex(_ rgbValue:UInt32, _alpha : CGFloat)->UIColor{
        let red   = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue  = CGFloat(rgbValue  & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:_alpha)
    }
    func getBase64(_ base : String)->String
    {
        let data64  : Data =  base.data(using: String.Encoding.utf8)!
        let str64   : String =  data64.base64EncodedString(options: NSData.Base64EncodingOptions())
        return str64
    }
    func getTemporaryDirectoryPath()->URL{
        
        let tmpDir = URL(fileURLWithPath: NSTemporaryDirectory())
        return tmpDir
    }
}

