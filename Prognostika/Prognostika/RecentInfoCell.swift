//
//  RecentInfoCell.swift
//  Prognostika
//
//  Created by Cope on 4/3/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class RecentInfoCell: UITableViewCell {

    //notif data
    @IBOutlet weak var lbl_recentInfo: UILabel!
    //notif title
    @IBOutlet weak var lbl_recentInfoTitle: UILabel!
    //notif time
    @IBOutlet weak var lbl_dateOfRecentInfo: UILabel!
    @IBOutlet weak var imgShadowContainer: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        imgShadowContainer.layer.cornerRadius = 2
        imgShadowContainer.addShadowImage(shadowStyle: .ShadowFourSides, shadowColor: nil, shadowRadius: 1)
        // add the shadow to the base view
        imgShadowContainer.backgroundColor = UIColor.white
        imgShadowContainer.layer.shadowColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 0.85).cgColor
        imgShadowContainer.layer.shadowOffset = CGSize(width: 0, height: 1)
        imgShadowContainer.layer.shadowOpacity = 0.7
        imgShadowContainer.layer.shadowRadius = 2.0
        
        // improve performance
        imgShadowContainer.layer.shadowPath = UIBezierPath(roundedRect: imgShadowContainer.bounds, cornerRadius: 2).cgPath
        imgShadowContainer.layer.shouldRasterize = true
        imgShadowContainer.layer.rasterizationScale = UIScreen.main.scale
    }
}
