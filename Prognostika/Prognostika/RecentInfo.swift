//
//  ResponseRecentInfo.swift
//  SharpGentleman
//
//  Created by Cope on 12/26/16.
//  Copyright © 2016 MilosStevanovic. All rights reserved.
//


import Foundation

struct RecentInfo{
    
    var createdAt : String!
    var message : String!
    var title : String!
    //SUSPENDED
    var suspended_until : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        createdAt = dictionary["createdAt"] as? String
        message = dictionary["message"] as? String
        title = dictionary["title"] as? String
        suspended_until = dictionary["suspended_until"] as? String

    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if createdAt != nil{
            dictionary["createdAt"] = createdAt
        }
        if message != nil{
            dictionary["message"] = message
        }
        if title != nil{
            dictionary["title"] = title
        }
        if suspended_until != nil{
            dictionary["suspended_until"] = suspended_until
        }
        return dictionary
    }
    
}
