//
//  DataExtension.swift
//  VQuarter
//
//  Created by Branko Grbic on 11/20/16.
//  Copyright © 2016 Comit. All rights reserved.
//

import Foundation

extension Data {
    var hexString: String {
        return map { String(format: "%02.2hhx", arguments: [$0]) }.joined()
    }
}
