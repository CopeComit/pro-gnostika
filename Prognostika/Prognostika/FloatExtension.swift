//
//  FloatExtension.swift
//  SharpGentleman
//
//  Created by Cope on 1/25/17.
//  Copyright © 2017 MilosStevanovic. All rights reserved.
//

import Foundation

extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
