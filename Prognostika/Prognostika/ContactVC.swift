//
//  ContactVC.swift
//  Prognostika
//
//  Created by Cope on 3/13/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit
import MessageUI
import IQKeyboardManagerSwift
class ContactVC: SideMenu, UITextFieldDelegate, UITextViewDelegate, MFMailComposeViewControllerDelegate, ProtocolSendEmail {

    //MARK: Variables
    var open : Bool!
    var activeTextField : UITextField!
    var activeTextView : UITextView!
    var activeField : Bool!
    internal typealias ContactJSON = Dictionary<String,Any>

    //MARK: Outlets
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tf_question: UITextView!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var lbl_header: UILabel!
    //MARK: Actions
    @IBAction func ActionSend(_ sender: Any) {
        if (tf_email.text?.isEmpty)! || tf_question.text == "Please enter your mail..." || tf_email.text?.isValidEmail() == false {
            
            if tf_email.text?.isValidEmail() == false{
                let alertController = UIAlertController(title: "Invalid email", message: "Please enter valid email.", preferredStyle:UIAlertControllerStyle.alert)
                
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                { action -> Void in
                    print("invalid mail")
                })
                self.present(alertController, animated: true, completion: nil)
            }
            
            if (tf_email.text?.isEmpty)! || tf_question.text == "Please enter your mail..." {
                let alertController = UIAlertController(title: "Empty field", message: "Please fill in all required fields", preferredStyle:UIAlertControllerStyle.alert)
                
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                { action -> Void in
                    print("empty fields")
                })
                self.present(alertController, animated: true, completion: nil)
            }
            return
        }
        else{
            sendContactWithJSON()
        }
    }
    
    @IBAction func actionSideMenu(_ sender: Any) {
        view.endEditing(true)
        tf_email.text = ""
        tf_question.text = ""
        self.view.backgroundColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 41.0/255.0, alpha: 1.0)
        if !open {
            self.captureScreen()
            self.showMenu(sender as! UIButton)
            open = true
        }
        else{
            self.closeMenu(sender as! UIButton)
            open = false
        }
    }
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ContactVC.sendSystemEmail))
        lbl_header.addGestureRecognizer(recognizer)
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        
        setupDesign()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.white
        open = appdelegat.is_side_menu_open
        lbl_header.attributedText = attributedText()
        btnSend.isEnabled = false
        btnSend.setTitleColor(UIColor(colorLiteralRed: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 0.61), for: .normal)
        appdelegat.titleForVC(vc: self, title: "Contact",isTransparent: false)
    }
    //MARK: Functions
    func setupDesign(){
        tf_email.textColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
        
    }
    //MARK: TextFields
    func textFieldDidEndEditing(_ textField: UITextField) {
        if activeTextField?.text?.isEmpty == true{
            btnSend.isEnabled = false
            btnSend.setTitleColor(UIColor(colorLiteralRed: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 0.61), for: .normal)
        }
        else{
            btnSend.isEnabled = true
            btnSend.setTitleColor(UIColor.white, for: .normal)
        }
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = true
        self.activeTextField = textField
        if activeTextField.text != ""{
            return
        }
        else {
            activeTextField.text = ""
        }
    }
    func textViewDidChange(_ textView: UITextView) {

        let
        line: CGRect? = textView.caretRect(for: (textView.selectedTextRange?.start)!)
        let lineInfo = (line?.origin.y)! + (line?.size.height)!
        let textViewInfo = (textView.contentOffset.y + textView.bounds.size.height - textView.contentInset.bottom - textView.contentInset.top)
        let overflow: CGFloat? = lineInfo - textViewInfo
        if Int(overflow!) > 0 {
            // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
            // Scroll caret to visible area
            
            var offset: CGPoint = textView.contentOffset
            offset.y += overflow! + 7
            // leave 7 pixels margin
            // Cannot animate with setContentOffset:animated: or caret will not appear
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                textView.contentOffset = offset
            })
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        activeField = true
        self.activeTextView = textView
        activeTextView.text = ""
        tf_question.textColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if activeTextView.text?.isEmpty == true{
            self.view.endEditing(true)
            tf_question.text = "Πληκτρολογήστε το μασάζ σας εδώ ..."
            tf_question.textColor = UIColor(colorLiteralRed: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
        }
    }
    //Tap gesture function for sending mail via system mail
    func sendSystemEmail(){
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }

    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["milos@comit.co.rs"])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    //Atributed text for top label
    func attributedText()->NSAttributedString{
        
        let string = "Παρακαλούμε χρησιμοποιήστε παρακάτω φόρμα επικοινωνίας ή να μας γράψετε με e-mail στο info@prognostika.com" as NSString
        
        let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15)])
        
        let boldFontAttribute = [NSFontAttributeName: UIFont.systemFont(ofSize: 15), NSForegroundColorAttributeName: UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)]
        
        attributedString.addAttributes(boldFontAttribute, range: string.range(of: "peca@comit.co.rs"))
        
        let paragraphStyle = NSMutableParagraphStyle()
        attributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        return attributedString
    }
    //MARK: Send JSON with email data
     func sendContactWithJSON(){
         var dataStore = DataStore()
         dataStore.delegat_contact = self
         dataStore.sendContactForm(info: prepareJson())
     }
     
     private func prepareJson()->ContactJSON {
         var json = Dictionary<String,Any>()
         var contact_form = Dictionary<String,Any>()
         json["mail"] = tf_email.text!
         json["message"] = tf_question.text!
         contact_form["contact"] = json
         return contact_form
     }
     
    func didSendEmail(response: ResponseContact?) {
        //MARK: TODO Response ok
        if let _response = response {
            if _response.status.key == "OK" {
                
            }
            else {
                DataUtility.sharedInstance.showMessage("Mail Info", message: _response.status.key)
            }
        }
    }
    
}
