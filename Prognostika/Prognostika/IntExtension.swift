//
//  IntExtension.swift
//  Prognostika
//
//  Created by Cope on 3/1/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation
import UIKit

extension Int {
    func times(f: () -> ()) {
        if self > 0 {
            for _ in 0..<self {
                f()
            }
        }
    }
    
    func times( f: @autoclosure () -> ()) {
        if self > 0 {
            for _ in 0..<self {
                f()
            }
        }
    }
}
