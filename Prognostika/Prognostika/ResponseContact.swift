//
//  ResponseContact.swift
//  Prognostika
//
//  Created by Cope on 5/11/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation

struct ResponseContact{
    var pagination : Pagination!
    var payload : [AnyObject]!
    var status : Status!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        if let paginationData = dictionary["pagination"] as? NSDictionary{
            pagination = Pagination(fromDictionary: paginationData)
        }
        payload = dictionary["payload"] as? [AnyObject]
        if let statusData = dictionary["status"] as? NSDictionary{
            status = Status(fromDictionary: statusData)
        }
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if pagination != nil{
            dictionary["pagination"] = pagination.toDictionary()
        }
        if payload != nil{
            dictionary["payload"] = payload
        }
        if status != nil{
            dictionary["status"] = status.toDictionary()
        }
        return dictionary
    }

}
