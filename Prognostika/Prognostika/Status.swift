//
//	Status.swift
//
//	Create by grom on 8/9/2016
import Foundation

struct Status{
    
    var code : Int!
    var key : String!
    var messages : [String]!
    
    init(){}
    init(fromDictionary dictionary: NSDictionary){
        code = dictionary["code"] as? Int
        key = dictionary["key"] as? String
        messages = dictionary["messages"] as? [String]
    }
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if code != nil{
            dictionary["code"] = code
        }
        if key != nil{
            dictionary["key"] = key
        }
        return dictionary
    }
}
