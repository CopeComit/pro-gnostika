//
//  ResponseFaq.swift
//  Prognostika
//
//  Created by Cope on 3/31/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation

import UIKit

import Foundation

struct ResponseFaq{
    
    var pagination : Pagination!
    var payload : [Faq]!
    var status : Status!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        if let paginationData = dictionary["pagination"] as? NSDictionary{
            pagination = Pagination(fromDictionary: paginationData)
        }
        payload = [Faq]()
        if let payloadArray = dictionary["payload"] as? [NSDictionary]{
            for dic in payloadArray{
                let value = Faq(fromDictionary: dic)
                payload.append(value)
            }
        }
        if let statusData = dictionary["status"] as? NSDictionary{
            status = Status(fromDictionary: statusData)
        }
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if pagination != nil{
            dictionary["pagination"] = pagination.toDictionary()
        }
        if payload != nil{
            var dictionaryElements = [NSDictionary]()
            for payloadElement in payload {
                dictionaryElements.append(payloadElement.toDictionary())
            }
            dictionary["payload"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status.toDictionary()
        }
        return dictionary
    }
    
}
