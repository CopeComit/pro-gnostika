//
//  NewsListCell.swift
//  Prognostika
//
//  Created by Cope on 3/7/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class NewsListCell: UITableViewCell {

    @IBOutlet weak var img_cellShadow: UIImageView!
    @IBOutlet weak var img_newsSport: UIImageView!
    @IBOutlet weak var lbl_validUntil: UILabel!
    @IBOutlet weak var lbl_newsTitle: UILabel!
    @IBOutlet weak var lbl_newsText: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var btn_buy: UIButton!
    @IBOutlet weak var btn_details: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        btn_buy.layer.cornerRadius = 5
        btn_details.layer.cornerRadius = 5
        img_cellShadow.layer.cornerRadius = 5
        img_cellShadow.addShadowImage(shadowStyle: .ShadowFourSides, shadowColor: nil, shadowRadius: 5)
        // add the shadow to the base view
        img_cellShadow.backgroundColor = UIColor.white
        img_cellShadow.layer.shadowColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 0.85).cgColor
        img_cellShadow.layer.shadowOffset = CGSize(width: 0, height: 3)
        img_cellShadow.layer.shadowOpacity = 0.7
        img_cellShadow.layer.shadowRadius = 4.0
        
        // improve performance
        img_cellShadow.layer.shadowPath = UIBezierPath(roundedRect: img_cellShadow.bounds, cornerRadius: 10).cgPath
        img_cellShadow.layer.shouldRasterize = true
        img_cellShadow.layer.rasterizationScale = UIScreen.main.scale
        img_newsSport.roundedElement()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
