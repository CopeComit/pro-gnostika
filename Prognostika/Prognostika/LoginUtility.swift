//
//  LoginUtility.swift
//  VQuarter
//
//  Created by Branko Grbic on 12/8/16.
//  Copyright © 2016 Branko Grbic. All rights reserved.
//

import Foundation
import UIKit
protocol ProtocolSuccesLoginUser : class {
    func didSuccessLoginUser()
}
class  LoginUtility : NSObject , ProtocolGetToken {
    
    private let SUCCESS_RESPONSE_KEY        =  "OK"
    var delegat_success_login           : ProtocolSuccesLoginUser?
    var delegat_view                    : UIViewController!
    var username                             : String!
    var password                             : String!
    var fb_token                             : String!
    var google_token                         : String!
    
    init(username : String , password : String ){
        self.username = username
        self.password = password
    }
    init(fb_token : String) {
        self.fb_token = fb_token
    }
    init(google_token : String) {
        self.google_token = google_token
    }
    func didGetToken(response: ResponseLogin?) {
        DataUtility.sharedInstance.hideActivityIndicator(delegat_view.view)
        if let _response = response {
            if _response.status.key != SUCCESS_RESPONSE_KEY{
                var msg = ""
                if let _msg2 = _response.status.messages.last {
                    msg  +=  _msg2
                }
                DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: msg)
                return
            }
            DataUtility.sharedInstance.ordinaryLoginPass(success: "OK")
            saveToken(info: _response.payload.first!)
            if let _delegat_success_login = delegat_success_login {
                _delegat_success_login.didSuccessLoginUser()
            }
        }
    }
    func loginUser(){
        DataUtility.sharedInstance.showActivityIndicatory(delegat_view.view, message: MessageInfo.Loading.rawValue)
        let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_getToken = self
        dataStore.getToken(info: json)
    }
    private func prepareJsonLogin()->Dictionary<String,String>{
        var json = Dictionary<String,String>()
        if let _ = username , let _ =  password  {
            json["username"] =  self.username.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            json["password"] =  self.password
        }

        return json
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
    }
}
