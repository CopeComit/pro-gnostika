//
//	Children.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation



struct RegistrationChildren{

    struct Error  {
        var title : String!
        var reason : String!
        init() {}
    }
	var country : String!
	var email : String!
	var level : String!
	var nameAndSurname : String!
	var stakePerUnit : Int!
	var username : String!
    var plainPassword : ResponseEditProfilePlainPassword!
    var error : Error!
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
        
        for (key,value) in dictionary {
            if value is NSDictionary {
                let _errorObject = value as! NSDictionary
                if let _arrayErrors  = _errorObject["errors"] as? [String] , _arrayErrors.count > 0 {
                self.error  = Error()
                error.title = key as! String
                let _errorsString = _errorObject["errors"]as! [String]
                error.reason = _errorsString.first!
                print(error)
                break
                }
            }
        }
        
         country = dictionary["country"] as? String
		 email = dictionary["email"] as? String
		 level = dictionary["level"] as? String
         nameAndSurname = dictionary["name_and_surname"] as? String
		 stakePerUnit = dictionary["stakePerUnit"] as? Int
		 username = dictionary["username"] as? String
        if let _plainPassword = dictionary["plainPassword"] as? NSDictionary {
            plainPassword = ResponseEditProfilePlainPassword(fromDictionary: _plainPassword)
        }
        
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if country != nil{
			dictionary["country"] = country
		}
		if email != nil{
			dictionary["email"] = email
		}
		if level != nil{
			dictionary["level"] = level
		}
		if nameAndSurname != nil{
			dictionary["name_and_surname"] = nameAndSurname
		}
		if stakePerUnit != nil{
			dictionary["stakePerUnit"] = stakePerUnit
		}
		if username != nil{
			dictionary["username"] = username
		}
        if plainPassword != nil {
            dictionary["plainPassword"] = plainPassword
        }
		return dictionary
	}

}
