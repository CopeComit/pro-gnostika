//
//  SideMenu.swift
//  Prognostika
//
//  Created by Cope on 2/28/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation
import UIKit

typealias SideMenu = UIViewController
var sideMenu: UIViewController!
var screenImageView: UIImageView!
var mainView: UIView!
var viewCurrentColor : UIColor!

extension SideMenu{
    
    func OpenViewController(storyboard: UIStoryboard, viewController: String){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewController)
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromRight
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(controller, animated: false, completion: nil)
    }
    func AnimateBouncing(){
        //Animate bouncing
        let xValue = CGFloat((screenImageView?.center.x)! + (8))
        let origin: CGPoint? = screenImageView?.center
        let target = CGPoint(x: CGFloat(xValue), y: CGFloat((screenImageView?.center.y)! ))
        let bounce = CABasicAnimation(keyPath: "position.x")
        bounce.duration = 0.2
        bounce.fromValue = Int((origin?.x)!)
        bounce.toValue = Int(target.x)
        bounce.repeatCount = 1
        bounce.autoreverses = true
        screenImageView?.layer.add(bounce, forKey: "position")
    }
   
    func captureScreen() -> UIImage {
        let activeScreen = self.view.window?.rootViewController?.view
//        var screenSize = activeScreen
//        screenSize?.frame.size.height -= 20
        UIGraphicsBeginImageContextWithOptions((activeScreen?.frame.size)!, (activeScreen?.isOpaque)!, 0.0)
        self.view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        screenImageView = UIImageView(image: image)
        return image!;
    }
    
    func closeMenu(_ sender: UIButton){
        sender.isEnabled = false
        appdelegat.is_side_menu_open = false
        UIView.animateKeyframes(withDuration: 0.4
            , delay: 0, options: [], animations: {
                
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: {
                    sideMenu.view.alpha = 0
                    sideMenu.view.frame.origin.y = -10
                })
                
                UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 2/3, animations: {
                    screenImageView.layer.transform = CATransform3DIdentity
                    screenImageView.frame.size = self.view.frame.size
                    screenImageView.frame.origin = CGPoint(x:0, y: 0)
                })
                
        },
              completion: {_ in
                //self.view.backgroundColor = viewCurrentColor
                screenImageView.removeFromSuperview()
                sideMenu.removeFromParentViewController()
               // sideMenu.removeFromSuperview()
                for subView in self.view.subviews as [UIView]{
                    if subView.tag < 2000{
                        subView.isHidden = false
                    }
                }
                self.viewWillAppear(true)

        }
        )
        sender.isEnabled = true
    }
    
    @IBAction func showMenu(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = true
        // Add the background image to the main view
        UIGraphicsBeginImageContext(self.view.frame.size);
        UIImage(named: "bckg_img copy")!.draw(in: self.view.bounds)
        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        view.backgroundColor = UIColor(patternImage: backgroundImage!)
        sender.isEnabled = false
        appdelegat.is_side_menu_open = true
        screenImageView.tag = 2001
        screenImageView.isUserInteractionEnabled = true
        view.addSubview(screenImageView)
        
        // Initiate Gesture recognizer
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(SideMenu.closeMenu(_:)))
        screenImageView.addGestureRecognizer(recognizer)
        
        // Build the menu from a Nib and add it as a sub view
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SideMenuVC")
        sideMenu = controller//Bundle.main.loadNibNamed("SideMenuVC", owner: self, options: nil)?[0] as! UIView
        sideMenu.view.frame = CGRect(x: 20, y: 600, width: 175, height: 400)//self.view.frame.size

        sideMenu.view.tag = 2002
        sideMenu.view.alpha = 0
        sideMenu.view.frame.origin.y = -10
        view.addSubview(sideMenu.view)
        
        // Hide all other subviews
        for subView in view.subviews as [UIView]{
            if subView.tag < 2000{
                subView.isHidden = true
            }
        }
        
        // Animate the captured image
        var id = CATransform3DIdentity
        id.m34 =  -1.0 / 1000
        
        // Rotate the view on the Y axis in a anti-clockwise direction
        let rotationTransform = CATransform3DRotate(id, 0.5 * CGFloat(-M_PI_2), 0, 1.0, 0)
        
        // Transform the view to the right of the screen
        let translationTransform = CATransform3DMakeTranslation(screenImageView.frame.width * 0.2, 0, 0)
        
        // Combine these two transformations
        let transform = CATransform3DConcat(rotationTransform, translationTransform)
        
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: [], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/3, animations: {
                screenImageView.layer.transform = transform
                screenImageView.frame.size.height -= 200
                screenImageView.center.y += 100
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 2/3, animations: {
                sideMenu.view.alpha = 1
                sideMenu.view.frame.origin.y = 0
            })
            
        },
                                completion: {_ in
                                //self.AnimateBouncing()
}
        )
       sender.isEnabled = true
    }

}
