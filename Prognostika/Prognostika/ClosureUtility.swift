//
//  ClosureUtility.swift
//  VQuarter
//
//  Created by Branko Grbic on 11/19/2016.
//  Copyright © 2016 ComIT. All rights reserved.
//

import Foundation

precedencegroup MultiplicationPrecedence {
    associativity: left
    higherThan: AdditionPrecedence
}

infix operator *>
infix operator <-> : MultiplicationPrecedence

@available(iOS 8.0, *)
private let user_initiated   = DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated)

func <-><B,R>(f : (B) ->R, a: B?)->R?{
    return a.map(f)
}
func *>(backgroundTask : @escaping ()->Void , mainTask:@escaping ()->Void){
    autoreleasepool { () -> () in
            user_initiated.async { () -> Void in
                backgroundTask()
                DispatchQueue.main.async(execute: { () -> Void in
                    mainTask()
                })
            }
    }
}
func *> <B>(backgroundTask:@escaping () -> B , mainTask:@escaping (_ result : B) -> ()){
    autoreleasepool { () -> () in

            user_initiated.async(execute: {
                let result = backgroundTask()
                DispatchQueue.main.async(execute: {
                    mainTask(result)
                })
            })
        }
}
func synchronize(_ backgroundTask : @escaping ()->Void , mainTask : @escaping ()->Void){
    autoreleasepool { () -> () in

            user_initiated.async(execute: { () -> Void in
                backgroundTask()
                DispatchQueue.main.async(execute: { () -> Void in
                    mainTask()
                })
            })
    }
}
func backgroundTask(_ task : @escaping () ->Void){
    
    autoreleasepool { () -> () in
            user_initiated.async { () -> Void in
                task()
            }
        }
}
func mainTask(_ task : @escaping () -> Void){
    autoreleasepool { () -> () in
            DispatchQueue.main.async { () -> Void in
                task()
           }
        }
}
