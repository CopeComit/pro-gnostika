//
//	RoomsStatistics.swift
//
//	Create by grom on 16/12/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct GeneralStatistics{

    var profit : String!
    var won : String!
    var void : String!
	var lost : String!
    var investment : String!
    var yield : String!
    var suspended_until : String!
    
//	var packageId : Int!
//	var packageTitle : String!

   


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		investment = dictionary["investment"] as? String
		lost = dictionary["lost"] as? String
//		packageId = dictionary["packageId"] as? Int
//		packageTitle = dictionary["packageTitle"] as? String
		profit = dictionary["profit"] as? String
		void = dictionary["void"] as? String
		won = dictionary["won"] as? String
		yield = dictionary["yield"] as? String
        suspended_until = dictionary["suspended_until"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if investment != nil{
			dictionary["investment"] = investment
		}
		if lost != nil{
			dictionary["lost"] = lost
		}
//		if packageId != nil{
//			dictionary["packageId"] = packageId
//		}
//		if packageTitle != nil{
//			dictionary["packageTitle"] = packageTitle
//		}
		if profit != nil{
			dictionary["profit"] = profit
		}
		if void != nil{
			dictionary["void"] = void
		}
		if won != nil{
			dictionary["won"] = won
		}
		if yield != nil{
			dictionary["yield"] = yield
		}
        if suspended_until != nil{
            dictionary["suspended_until"] = suspended_until
        }
        
		return dictionary
	}

}
