//
//  ExtensionRoundedElement.swift
//  Prognostika
//
//  Created by Cope on 3/7/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation
import UIKit

typealias RoundedElement = UIView
extension RoundedElement{
    func roundedElement(){
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: [.topLeft , .topRight],
                                     cornerRadii:CGSize(width: 5.0, height: 5.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
        
    }
}
