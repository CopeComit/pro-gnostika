//
//  SideMenuViewController.swift
//  Prognostika
//
//  Created by Cope on 3/1/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

var paintAtIndex = 0
class SideMenuViewController: SideMenu, UITableViewDelegate, UITableViewDataSource {

    //MARK: Outlets
    @IBOutlet weak var table: UITableView!
    //MARK: Variables
    let arrayOfSideMenuOptions = ["Ενεργά στοιχήματα", "Ειδοποιήσεις", "Στατιστικά", "Προφίλ", "Επικοινωνία", "Συχνές ερωτήσεις"]
    //MARK: LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        appdelegat.offset = 0
        appdelegat.payload = 0
    }
    
    //MARK: Functions
    //Screen opening
    func sideMenuCellAction(indexPath: Int){
        paintAtIndex = indexPath
        appdelegat.is_side_menu_open = false
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromRight
        view.window!.layer.add(transition, forKey: kCATransition)
        switch indexPath {
        case 0:
            appdelegat.isActivePrediction = 1
            OpenViewController(storyboard: UIStoryboard(name: "Main", bundle: nil), viewController: "NewsListVC")
            
        case 1:
            appdelegat.isActivePrediction = 1
            OpenViewController(storyboard: UIStoryboard(name: "Main", bundle: nil), viewController: "RecentInfoVC")
        case 2:
            OpenViewController(storyboard: UIStoryboard(name: "Main", bundle: nil), viewController: "StatisticsVC")

        case 3:
            OpenViewController(storyboard: UIStoryboard(name: "Main", bundle: nil), viewController: "ProfileVC")

        case 4:
            OpenViewController(storyboard: UIStoryboard(name: "Main", bundle: nil), viewController: "ContactVC")

        case 5:
            OpenViewController(storyboard: UIStoryboard(name: "Main", bundle: nil), viewController: "FAQVC")
        default:
            print("Default")
        }
    }
    
    //TABLE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfSideMenuOptions.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        
        let cell = tableView.cellForRow(at: indexPath) as? SideMenuCell
        cell?.lbl_title.textColor = UIColor(colorLiteralRed: 0/255.0, green:122/255.0, blue:255/255.0,  alpha:1.0)
        sideMenuCellAction(indexPath: indexPath.row)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SideMenuCell
        if indexPath.row == paintAtIndex {
            cell?.lbl_title.textColor = UIColor(colorLiteralRed: 0/255.0, green:122/255.0, blue:255/255.0,  alpha:1.0)
        }
        cell?.lbl_title.text = arrayOfSideMenuOptions[indexPath.row]
        cell?.selectionStyle = .none
        return cell!
    }
    var delay = 0.2
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        let transformation = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0)
        if indexPath.row == 0 {
            cell.layer.transform = transformation

        }
        UIView.animate(withDuration: 0.2) {
            if indexPath.row == 0 {
                cell.layer.transform = CATransform3DIdentity
                UIView.animate(withDuration: 0.2){
                    cell.alpha = 1.0
                }
            }
            else {
                if indexPath.row > 0 {
                     self.delay += 0.009
                }
                    UIView.animate(withDuration: 1.0, delay: TimeInterval(self.delay), options: [], animations: {
                        cell.alpha = 1.0
                    }, completion: nil)
            }
        }
    }
}

