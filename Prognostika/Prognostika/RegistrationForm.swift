//
//	Form.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct RegistrationForm{

	var children : RegistrationChildren!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let childrenData = dictionary["children"] as? NSDictionary{
				children = RegistrationChildren(fromDictionary: childrenData)
			}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if children != nil{
			dictionary["children"] = children.toDictionary()
		}
		return dictionary
	}

}
