//
//	LoginPayload.swift
//
//	Create by Branko Grbic on 28/11/2016
//	Copyright © 2016. All rights reserved.

import Foundation

struct LoginPayload{

	var accessToken     : String!
	var expiresIn       : Int!
	var refreshToken    : String!
	var scope           : String!
	var tokenType       : String!

	init(fromDictionary dictionary: NSDictionary){
		accessToken = dictionary["access_token"] as? String
		expiresIn = dictionary["expires_in"] as? Int
		refreshToken = dictionary["refresh_token"] as? String
		scope = dictionary["scope"] as? String
		tokenType = dictionary["token_type"] as? String
	}
}
