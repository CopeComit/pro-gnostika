//
//  DataParser.swift
//  Click2Win
//
//  Created by Branko Grbic on 7/31/15.
//  Copyright (c) 2015 ComITGroup. All rights reserved.
//

import UIKit

internal typealias JSON = Dictionary<String,AnyObject>
protocol  ProtocolParser : class  {
    func parseJson(_ json : JSON, _ typeOfParse : ParserTypes)->Any?
}
enum ParserTypes : Int{
    
    case Register
    case GetToken
    case Countries
    case Predictions
    case Contact
    case Singles
    case RequestPassword
    case Outcomes
    case PersonalStatistics
    case GeneralStatistics
    case UserProfile
    case EditProfile
    case PredictionCount
    case RegisterDevice
    case RecentInfo
    case Faq
}
final class DataParser : ProtocolParser {
    
    static let sharedInstance = DataParser()
    
    func parseJson(_ json : JSON, _ typeOfParse : ParserTypes)->Any?{
        
        var result : Any!
        
        switch typeOfParse {
        case .Register:
            result = ResponseRegistration(fromDictionary: json as NSDictionary)
        case .GetToken:
            result = ResponseLogin(fromDictionary: json as NSDictionary)
        case .RegisterDevice:
            result = ResponseRegisterDevice(fromDictionary: json as NSDictionary)
        case .RequestPassword:
            result = RequestPasswordResponse(fromDictionary: json as NSDictionary)
        case .Predictions:
            result = ResponsePredictions(fromDictionary: json as NSDictionary)
        case .Faq:
            result = ResponseFaq(fromDictionary: json as NSDictionary)
        case .RecentInfo:
            result = ResponseRecentInfo(fromDictionary: json as NSDictionary)
        case .GeneralStatistics:
            result = ResponseGeneralStatistics(fromDictionary: json as NSDictionary)
        case .PersonalStatistics:
            result = ResponsePersonalStatistics(fromDictionary: json as NSDictionary)
        case .UserProfile:
            result = ResponseUserProfile(fromDictionary: json as NSDictionary)
        default:
            print("default")
        }
        
        return result
    }
}


