//
//  UIViewShadowExtension.swift
//  Prognostika
//
//  Created by Cope on 3/1/17.
//  Copyright © 2017 Comit. All rights reserved.

import UIKit
import QuartzCore

extension UIView {
    
    var shadowColor: UIColor? {
        set {
            layer.shadowColor = newValue!.cgColor
        }
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    var shadowOpacity: Float {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    var shadowOffset: CGPoint {
        set {
            layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
        }
        get {
            return CGPoint(x: layer.shadowOffset.width, y:layer.shadowOffset.height)
        }
    }
    var shadowRadius: CGFloat {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }
}
extension UIView {
    
    //////////
    // Top
    //////////
    func createTopBorderWithHeight(height: CGFloat, color: UIColor) -> CALayer {
        return getOneSidedBorderWithFrame(frame: CGRect(x:0, y:0,width: self.frame.size.width, height:height), color:color)
    }
    
    func createViewBackedTopBorderWithHeight(height: CGFloat, color:UIColor) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0, y:0,width: self.frame.size.width,height: height), color:color)
    }
    
    func addTopBorderWithHeight(height: CGFloat, color:UIColor) {
        addOneSidedBorderWithFrame(frame: CGRect(x:0, y:0, width:self.frame.size.width, height:height), color:color)
    }
    
    func addViewBackedTopBorderWithHeight(height: CGFloat, color:UIColor) {
        addViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0, y:0, width:self.frame.size.width, height:height), color:color)
    }
    
    
    //////////
    // Top + Offset
    //////////
    
    func createTopBorderWithHeight(height:CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, topOffset:CGFloat) -> CALayer {
        return getOneSidedBorderWithFrame(frame: CGRect(x:0 + leftOffset, y:0 + topOffset, width:self.frame.size.width - leftOffset - rightOffset, height:height), color:color)
    }
    
    func createViewBackedTopBorderWithHeight(height:CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, topOffset:CGFloat) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0 + leftOffset, y:0 + topOffset, width:self.frame.size.width - leftOffset - rightOffset, height:height), color:color)
    }
    
    func addTopBorderWithHeight(height:CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, topOffset:CGFloat) {
        addOneSidedBorderWithFrame(frame: CGRect(x:0 + leftOffset,y: 0 + topOffset,width: self.frame.size.width - leftOffset - rightOffset,height: height), color:color)
    }
    
    func addViewBackedTopBorderWithHeight(height:CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, topOffset:CGFloat) {
        addViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0 + leftOffset, y:0 + topOffset, width:self.frame.size.width - leftOffset - rightOffset, height:height), color:color)
    }
    
    
    //////////
    // Right
    //////////
    
    func createRightBorderWithWidth(width:CGFloat, color:UIColor) -> CALayer {
        return getOneSidedBorderWithFrame(frame: CGRect(x:self.frame.size.width-width, y:0,width: width, height:self.frame.size.height), color:color)
    }
    
    func createViewBackedRightBorderWithWidth(width:CGFloat, color:UIColor) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(frame: CGRect(x:self.frame.size.width-width,y: 0,width: width, height:self.frame.size.height), color:color)
    }
    
    func addRightBorderWithWidth(width:CGFloat, color:UIColor){
        addOneSidedBorderWithFrame(frame: CGRect(x:self.frame.size.width-width, y:0, width:width, height:self.frame.size.height), color:color)
    }
    
    func addViewBackedRightBorderWithWidth(width:CGFloat, color:UIColor) {
        addViewBackedOneSidedBorderWithFrame(frame: CGRect(x:self.frame.size.width-width, y:0, width:width, height:self.frame.size.height), color:color)
    }
    
    
    //////////
    // Right + Offset
    //////////
    
    func createRightBorderWithWidth(width: CGFloat, color:UIColor, rightOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) -> CALayer {
        return getOneSidedBorderWithFrame(frame: CGRect(x:self.frame.size.width-width-rightOffset,y: 0 + topOffset,width: width, height:self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func createViewBackedRightBorderWithWidth(width: CGFloat, color:UIColor, rightOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(frame: CGRect(x:self.frame.size.width-width-rightOffset, y:0 + topOffset, width:width, height:self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func addRightBorderWithWidth(width: CGFloat, color:UIColor, rightOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) {
        addOneSidedBorderWithFrame(frame: CGRect(x:self.frame.size.width-width-rightOffset,y: 0 + topOffset, width:width, height:self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func addViewBackedRightBorderWithWidth(width: CGFloat, color:UIColor, rightOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) {
        addViewBackedOneSidedBorderWithFrame(frame: CGRect(x:self.frame.size.width-width-rightOffset, y:0 + topOffset, width:width, height:self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    
    //////////
    // Bottom
    //////////
    
    func createBottomBorderWithHeight(height: CGFloat, color:UIColor) -> CALayer {
        return getOneSidedBorderWithFrame(frame: CGRect(x:0, y:self.frame.size.height-height, width:self.frame.size.width, height:height), color:color)
    }
    
    func createViewBackedBottomBorderWithHeight(height: CGFloat, color:UIColor) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0, y:self.frame.size.height-height, width:self.frame.size.width, height:height), color:color)
    }
    
    func addBottomBorderWithHeight(height: CGFloat, color:UIColor) {
        return addOneSidedBorderWithFrame(frame: CGRect(x:0, y:self.frame.size.height-height, width:self.frame.size.width, height:height), color:color)
    }
    
    func addViewBackedBottomBorderWithHeight(height: CGFloat, color:UIColor) {
        addViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0, y:self.frame.size.height-height, width:self.frame.size.width, height:height), color:color)
    }
    
    
    //////////
    // Bottom + Offset
    //////////
    
    func createBottomBorderWithHeight(height: CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, bottomOffset:CGFloat) -> CALayer {
        return getOneSidedBorderWithFrame(frame: CGRect(x:0 + leftOffset, y:self.frame.size.height-height-bottomOffset,width: self.frame.size.width - leftOffset - rightOffset, height:height), color:color)
    }
    
    func createViewBackedBottomBorderWithHeight(height: CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, bottomOffset:CGFloat) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0 + leftOffset, y:self.frame.size.height-height-bottomOffset, width:self.frame.size.width - leftOffset - rightOffset,height: height), color:color)
    }
    
    func addBottomBorderWithHeight(height: CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, bottomOffset:CGFloat) {
        addOneSidedBorderWithFrame(frame: CGRect(x:0 + leftOffset, y:self.frame.size.height-height-bottomOffset, width:self.frame.size.width - leftOffset - rightOffset, height:height), color:color)
    }
    
    func addViewBackedBottomBorderWithHeight(height: CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, bottomOffset:CGFloat) {
        addViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0 + leftOffset, y:self.frame.size.height-height-bottomOffset, width:self.frame.size.width - leftOffset - rightOffset,height: height), color:color)
    }
    
    
    
    //////////
    // Left
    //////////
    
    func createLeftBorderWithWidth(width: CGFloat, color:UIColor) -> CALayer {
        return getOneSidedBorderWithFrame(frame: CGRect(x:0,y: 0,width: width, height:self.frame.size.height), color:color)
    }
    
    func createViewBackedLeftBorderWithWidth(width: CGFloat, color:UIColor) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0, y:0, width:width, height:self.frame.size.height), color:color)
    }
    
    func addLeftBorderWithWidth(width: CGFloat, color:UIColor) {
        addOneSidedBorderWithFrame(frame: CGRect(x:0, y:0, width:width, height:self.frame.size.height), color:color)
    }
    
    func addViewBackedLeftBorderWithWidth(width: CGFloat, color:UIColor) {
        addViewBackedOneSidedBorderWithFrame(frame: CGRect(x:0, y:0, width:width, height:self.frame.size.height), color:color)
    }
    
    
    
    //////////
    // Left + Offset
    //////////
    
    func createLeftBorderWithWidth(width:CGFloat, color:UIColor, leftOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) -> CALayer {
        return getOneSidedBorderWithFrame(frame: CGRect(x : 0 + leftOffset, y : 0 + topOffset, width:width, height:self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func createViewBackedLeftBorderWithWidth(width:CGFloat, color:UIColor, leftOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(frame: CGRect( x : 0 + leftOffset,y : 0 + topOffset, width : width, height :self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    
    func addLeftBorderWithWidth(width:CGFloat, color:UIColor, leftOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) {
        addOneSidedBorderWithFrame(frame: CGRect(x : 0 + leftOffset, y: 0 + topOffset, width:  width, height:  self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func addViewBackedLeftBorderWithWidth(width:CGFloat, color:UIColor, leftOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) {
        addViewBackedOneSidedBorderWithFrame(frame: CGRect(x  : 0 + leftOffset,y :  0 + topOffset, width: width , height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    
    
    //////////
    // Private methods helper:
    //////////
    
    private func addOneSidedBorderWithFrame(frame: CGRect, color:UIColor) {
        let border = CALayer()
        border.frame = frame
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
    
    private func getOneSidedBorderWithFrame(frame: CGRect, color:UIColor) -> CALayer {
        let border = CALayer()
        border.frame = frame
        border.backgroundColor = color.cgColor
        return border
    }
    
    private func addViewBackedOneSidedBorderWithFrame(frame: CGRect, color: UIColor) {
        let border = UIView(frame: frame)
        border.backgroundColor = color
        self.addSubview(border)
    }
    
    private func getViewBackedOneSidedBorderWithFrame(frame: CGRect, color: UIColor) -> UIView {
        let border = UIView(frame: frame)
        border.backgroundColor = color
        return border
    }
    
}
extension UIView {
    
    func addShadowImage(shadowStyle : ShadowStyle, shadowColor:UIColor?,shadowRadius: CGFloat?){
        
        var shadowColor = shadowColor
        if shadowColor == nil {
            shadowColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 0.85)
        }
        
        
        self.layer.shadowColor = shadowColor?.cgColor
        self.layer.masksToBounds = false
        
        if shadowStyle.description == 1 {
            //for bottom shadow on view
            self.layer.shadowOffset = CGSize(width:0,height:1.0)
            self.layer.shadowOpacity = 0.7
            self.layer.shadowRadius = 1.0
        }
        
        if shadowStyle.description == 2 {
            //for bottom and right sides shadow on view
            self.layer.shadowOffset = CGSize(width:1.0,height:1.0)
            self.layer.shadowOpacity = 1
            self.layer.shadowRadius = 1.0
            
        }
        
        if shadowStyle.description == 3 {
            //for empty shadow on view
            self.layer.shadowOffset = CGSize(width:0,height:0)
            self.layer.shadowOpacity = 1
            self.layer.shadowRadius = 0
            
        }
        
        if shadowStyle.description == 4 {
            //for bottom and right and left sides shadow on view
            self.layer.shadowOffset = CGSize(width:0,height:2.0)
            self.layer.shadowOpacity = 1
            self.layer.shadowRadius = 2.0
            
        }
        if shadowStyle.description == 5 {
            //for four sides shadow on view
            self.layer.shadowOffset = CGSize(width:0,height:0)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 5.0
        }
        if shadowRadius != nil {
            self.layer.shadowRadius = shadowRadius!
        }
    }
    
    enum ShadowStyle: String {
        case ShadowOnBottom
        case ShadowBottomAndRight
        case ShadowEmptyShadow
        case ShadowRightBottomAndLeft
        case ShadowFourSides
        
        var description: Int {
            switch self {
            case .ShadowOnBottom:
                return 1
            case .ShadowBottomAndRight:
                return 2
            case .ShadowEmptyShadow:
                return 3
            case .ShadowRightBottomAndLeft:
                return 4
            case .ShadowFourSides:
                return 5
            }
        }
    }
}
