//
//  UserToken.swift
//  VQuarter
//
//  Created by Branko Grbic on 12/1/16.
//  Copyright © 2016 Branko Grbic. All rights reserved.
//

import Foundation


struct UserToken {
    var _format         = "json"
    var client_id       = " 1_5zt2fjzasbs4sc0w8kgg80wssc8k008ogcskocoo4sog00wo88"
    var client_secret   = "4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
    var grant_type      : String! //default
    var username        : String!
    var password        : String!
    var refresh_token   : String!
    var fb_token        : String!
    var google_token    : String!
    init(){}
    func toDictionary() -> Dictionary<String,String>
    {
        var dictionary = Dictionary<String,String>()
            dictionary["_format"]       = _format
            dictionary["client_id"]     = client_id
            dictionary["client_secret"] = client_secret
        if grant_type != nil{
            dictionary["grant_type"]    = grant_type
        }
        if username != nil{
            dictionary["username"]      = username
        }
        if password != nil{
            dictionary["password"]      = password
        }
        if fb_token != nil{
            dictionary["fb_token"]      = fb_token
        }
        if google_token != nil{
            dictionary["google_token"]  = google_token
        }
        return dictionary
    }
}
