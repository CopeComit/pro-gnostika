//
//  RecentInfoVC.swift
//  Prognostika
//
//  Created by Cope on 4/3/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class RecentInfoVC: UIViewController,UITableViewDelegate,UITableViewDataSource, ProtocolRecentInfo, ProtocolRefreshToken {
    
    //MARK: Variables
    var arrayNotifications = [RecentInfo]()
    var open : Bool!
    //MARK: Outlets
    @IBOutlet weak var tableRecentInfo: UITableView!
    @IBOutlet weak var lblNoNotifications: UILabel!
    //MARK: Actions
    @IBAction func OpenMenu(_ sender: Any) {
        self.view.backgroundColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 41.0/255.0, alpha: 1.0)
        appdelegat.payload = 0
        appdelegat.offset = 0
        if !open {
            self.captureScreen()
            self.showMenu(sender as! UIButton)
            open = true
        }
        else{
            self.closeMenu(sender as! UIButton)
            open = false
        }
    }
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoNotifications.isHidden = true
        tableRecentInfo.delegate = self
        tableRecentInfo.dataSource = self
//        tableRecentInfo.rowHeight = UITableViewAutomaticDimension
//        tableRecentInfo.estimatedRowHeight = 170
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        open = appdelegat.is_side_menu_open
        getRecentInfo()
    }
    //MARK: Functions
    //MARK: Table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentInfoCell", for: indexPath) as! RecentInfoCell
        let entry = arrayNotifications[indexPath.row]
        
        cell.lbl_recentInfo.sizeToFit()
        cell.lbl_recentInfoTitle.sizeToFit()
        cell.lbl_recentInfo.text = entry.message
        cell.lbl_recentInfoTitle.text = entry.title
        cell.lbl_dateOfRecentInfo.text = entry.createdAt
        cell.imgShadowContainer.layer.shadowColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 0.85).cgColor
        cell.imgShadowContainer.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.imgShadowContainer.layer.shadowOpacity = 0.7
        //cell.imgShadowContainer.layer.shadowRadius = 1.0
        
        return cell
        
    }
    //MARK: Get recent info
    func getRecentInfo() {
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        
        var dataStore = DataStore()
        dataStore.delegat_recentInfo = self
        dataStore.getRecentInfo()
    }
    func didGetRecentInfo(response: ResponseRecentInfo?) {
        
        
        
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        if let _response = response{
            if _response.status.key == "OK"{
                arrayNotifications = _response.payload
                if arrayNotifications.count > 0 {
                    lblNoNotifications.isHidden = true
                }
                if arrayNotifications.count == 0 {
                    lblNoNotifications.isHidden = false
                    lblNoNotifications.text = "Δεν υπάρχουν πρόσφατες ειδοποιήσεις προς το παρόν."
                }
                tableRecentInfo.reloadData()
            }
            else if response?.status.key == "TOKEN_EXPIRED" {
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                saveSuspendedDeviceInfo(info: (response?.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
    }
    //MARK: Refresh token
    func refreshToken(){
        //let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_refreshToken = self
        dataStore.refreshToken()
    }
    func didRefreshToken(response: ResponseLogin?) {
        if let _response = response {
            if _response.status.key == "OK"{
                //DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.key)
                saveToken(info: _response.payload.first!)
                //call again
                getRecentInfo()
            }
        }
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
    }
    //MARK: Save suspended device
    private func saveSuspendedDeviceInfo(info : RecentInfo){
        DataUtility.sharedInstance.saveSuspendedDeviceInfo(username:DataUtility.sharedInstance.getUserCredentials().username, suspended_until: info.suspended_until)
    }
    
    func graphic(){}

}
