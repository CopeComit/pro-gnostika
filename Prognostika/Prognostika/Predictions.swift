//
//  ResponsePredictions.swift
//  SharpGentleman
//
//  Created by Cope on 12/12/16.
//  Copyright © 2016 MilosStevanovic. All rights reserved.
//

import UIKit
import Foundation

struct Predictions{
    
    var bet : String!
    var content : String!
    var countryTitle : String!
    var id : Int!
    var userId : String!
    var leagueTitle : String!
    var odd : Float!
    var packageTitle : String!
    var priceValue : String!
    var profit : String!
    var sportTitle : String!
    var stake : Int!
    var teaser : String!
    var title : String!
    var validTo : String!
    var remUnits : String!
    //Arguments for outcome
    var outcome_title : String!
    var result : String!
    //Extra arguments
    var userPredictionID : Int!
    var boughtAt  : String!
    var seenAt    : String!
    var deletedAt : String!
    //SUSPENDED
    var suspended_until : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        bet = dictionary["bet"] as? String
        content = dictionary["content"] as? String
        countryTitle = dictionary["country_title"] as? String
        id = dictionary["id"] as? Int
        userId = dictionary["userId"] as? String
        leagueTitle = dictionary["league_title"] as? String
        odd = dictionary["odd"] as? Float
        packageTitle = dictionary["package_title"] as? String
        priceValue = dictionary["priceValue"] as? String
        profit = dictionary["profit"] as? String
        sportTitle = dictionary["sport_title"] as? String
        stake = dictionary["stake"] as? Int
        teaser = dictionary["teaser"] as? String
        title = dictionary["title"] as? String
        validTo = dictionary["validTo"] as? String
        userPredictionID = dictionary["userPredictionID"] as? Int
        boughtAt = dictionary["boughtAt"] as? String
        seenAt = dictionary["seenAt"] as? String
        deletedAt = dictionary["deletedAt"] as? String
        boughtAt = dictionary["boughtAt"] as? String
        seenAt = dictionary["seenAt"] as? String
        deletedAt = dictionary["deletedAt"] as? String
        suspended_until = dictionary["suspended_until"] as? String
        outcome_title = dictionary["outcome_title"] as? String
        result = dictionary["result"] as? String
        remUnits = dictionary["remUnits"] as? String
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if bet != nil{
            dictionary["bet"] = bet
        }
        if content != nil{
            dictionary["content"] = content
        }
        if countryTitle != nil{
            dictionary["country_title"] = countryTitle
        }
        if id != nil{
            dictionary["id"] = id
        }
        if userId != nil{
            dictionary["userId"] = userId
        }
        if leagueTitle != nil{
            dictionary["league_title"] = leagueTitle
        }
        if odd != nil{
            dictionary["odd"] = odd
        }
        if packageTitle != nil{
            dictionary["package_title"] = packageTitle
        }
        if priceValue != nil{
            dictionary["priceValue"] = priceValue
        }
        if profit != nil{
            dictionary["profit"] = profit
        }
        if sportTitle != nil{
            dictionary["sport_title"] = sportTitle
        }
        if stake != nil{
            dictionary["stake"] = stake
        }
        if teaser != nil{
            dictionary["teaser"] = teaser
        }
        if title != nil{
            dictionary["title"] = title
        }
        if validTo != nil{
            dictionary["validTo"] = validTo
        }
        if boughtAt != nil{
            dictionary["boughtAt"] = boughtAt
        }
        if seenAt != nil{
            dictionary["seenAt"] = seenAt
        }
        if deletedAt != nil{
            dictionary["deletedAt"] = deletedAt
        }
        if userPredictionID != nil{
            dictionary["userPredictionID"] = userPredictionID
        }
        if boughtAt != nil{
            dictionary["boughtAt"] = boughtAt
        }
        if seenAt != nil{
            dictionary["seenAt"] = seenAt
        }
        if deletedAt != nil{
            dictionary["deletedAt"] = deletedAt
        }
        if suspended_until != nil{
            dictionary["suspended_until"] = suspended_until
        }
        if outcome_title != nil{
            dictionary["outcome_title"] = outcome_title
        }
        if result != nil{
            dictionary["result"] = result
        }
        if remUnits != nil{
            dictionary["remUnits"] = remUnits
        }
        return dictionary
    }
    
}
