//
//  ForgotPasswordFirstVC.swift
//  Prognostika
//
//  Created by Cope on 3/22/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class ForgotPasswordFirstVC: UIViewController, ProtocolRequestPassword {
    //MARK: Variables
    internal typealias RegistrationJSON = Dictionary<String,String>
    var activeTextField                                 = UITextField()
    //MARK: Outlets
    @IBOutlet weak var tf_email                              : UITextField!
    @IBOutlet weak var btn_resetPassword                     : UIButton!
    //MARK: Actions
    @IBAction func ResetPassword(_ sender: Any) {
        if tf_email.text?.isValidEmail() == false {
            let alertController = UIAlertController(title: "Invalid email", message: "Please enter valid email.", preferredStyle:UIAlertControllerStyle.alert)
            
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            { action -> Void in
                print("invalid mail")
            })
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            resetPassword()
        }
    }
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_resetPassword.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Functions
    private func prepareJson()->RegistrationJSON {
        var json = Dictionary<String,String>()
        json["username"] = tf_email.text!
        
        return json
    }
    func resetPassword(){
        var dataStore = DataStore()
        dataStore.delegat_requestPassword = self
        dataStore.requestPassword(info: prepareJson())
    }
    func didRequestPassword(response: RequestPasswordResponse?) {
        if let _response = response {
            if _response.status.key == "OK"{
                self.performSegue(withIdentifier: "resetPassword", sender: self)
            }
            else if _response.status.key == "DEVICE_SUSENDED"{
                DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: "Device suspended")
            }
            else{
                DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.messages.last!)
            }
        }
    }

}
