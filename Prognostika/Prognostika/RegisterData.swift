//
//	Payload.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct RegisterData{

	var errors : [String]!
	var form : RegistrationForm!

	init(fromDictionary dictionary: NSDictionary){
		errors = dictionary["errors"] as? [String]
		if let formData = dictionary["form"] as? NSDictionary{
				form = RegistrationForm(fromDictionary: formData)
			}
	}

	func toDictionary() -> NSDictionary
	{
		let  dictionary = NSMutableDictionary()
		if errors != nil{
			dictionary["errors"] = errors
		}
		if form != nil{
			dictionary["form"] = form.toDictionary()
		}
		return dictionary
	}

}
