//
//  SignUpVC.swift
//  Prognostika
//
//  Created by Cope on 3/6/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class SignUpVC: UIViewController, ProtocolGetToken/*Login*/, ProtocolRegister/*DataStore*/, ProtocolRegisterDevice {

    //MARK: Variables
    var formStatus = false
    var activeTextField                               = UITextField()
    let SUCCESS_RESPONSE_KEY         =  "OK"
    let BAD_DATA_RESPONSE_KEY        = "BAD_DATA"

    internal typealias RegistrationJSON = Dictionary<String,Any>
    //MARK: Outlets
    @IBOutlet weak var btn_signUp: UIButton!
    @IBOutlet weak var btn_googlePlus: UIButton!
    @IBOutlet weak var btn_facebook: UIButton!
        //Textfields
    @IBOutlet weak var view_email_shadow: UIView!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_password: UITextField!
    @IBOutlet weak var tf_confirmPassword: UITextField!
    @IBOutlet weak var tf_enterPromoCode: UITextField!
    @IBOutlet weak var tf_nameAndSurname: CustomTextField!
    @IBOutlet var viewFirstCustomPopUp: UIView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var btnDidEnterPromoCode: UIButton!
    var effect : UIVisualEffect!
    
    //MARK: Actions
    @IBAction func actionDidEnterPromoCode(_ sender: Any) {
        if tf_enterPromoCode.text?.isEmpty == false {
            DataUtility.sharedInstance.savePromoCode(promoCode: tf_enterPromoCode.text)
            animateOut()
        }
    }
    @IBAction func actionNoPromoCode(_ sender: Any) {
        animateOut()
    }
    @IBAction func actionFacebook(_ sender: Any) {
        let fblogin              =  FBSDKLoginManager()
        appdelegat.isFBLogin     = true
        fblogin.logIn(withReadPermissions: ["public_profile","email"], from: self, handler:{ [unowned self] (result,error) in
            appdelegat.isFBLogin = false
            let info     = result as? Dictionary<String,String>
            var fb_id : String!
            if error != nil {
                print("Facebook connection problems !")
                return
            }else if result!.isCancelled {
                print("Facebook login canceled")
                return
            }else {
                let fbToken = result!.token.tokenString
                DataUtility.sharedInstance.saveFBToken(token: fbToken!)
                fb_id = result?.token.appID
                DataUtility.sharedInstance.saveFBID(fb_id: fb_id!)
                self.getFacebookToken()
            }
        })

    }
    @IBAction func actionSignUp(_ sender: Any) {
        if formStatus == true {
            registerUser()
        }
        else {
            let alertController = UIAlertController(title: "Info", message: "Please fill all the fields.", preferredStyle:UIAlertControllerStyle.alert)
            
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            { action -> Void in
                print("invalid form")
            })
            self.present(alertController, animated: true, completion: nil)

        }
    }
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        effect = visualEffectView.effect!
        visualEffectView.effect = nil
        visualEffectView.layer.cornerRadius = 5
        viewFirstCustomPopUp.layer.cornerRadius = 5
        setupDesign()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        appdelegat.titleForVC(vc: self, title: "Sign up",isTransparent: false)
        animateIn()
    }
    //MARK: Functions
    //MARK: Promo code popup
    func animateIn(){
        self.view.addSubview(viewFirstCustomPopUp)
        visualEffectView.isHidden = false
        viewFirstCustomPopUp.center = self.view.center
        viewFirstCustomPopUp.frame.origin.y -= viewFirstCustomPopUp.frame.height / 4
        viewFirstCustomPopUp.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewFirstCustomPopUp.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.visualEffectView.effect = self.effect
            self.viewFirstCustomPopUp.alpha = 1
            self.viewFirstCustomPopUp.transform = CGAffineTransform.identity
        }
    }
    func animateOut(){
        UIView.animate(withDuration: 0.3, animations: {
            self.viewFirstCustomPopUp.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.viewFirstCustomPopUp.alpha = 0
            self.visualEffectView.effect = nil
            
        }) { (Bool) in
            self.viewFirstCustomPopUp.removeFromSuperview()
            self.visualEffectView.isHidden = true
        }
    }
    func setupDesign(){
        viewFirstCustomPopUp.frame.size.height = self.view.frame.height * 0.4
        btnDidEnterPromoCode.frame.size.height = viewFirstCustomPopUp.frame.size.height * 0.18
        btnDidEnterPromoCode.layer.cornerRadius = 5
        btn_googlePlus.layer.cornerRadius = 5
        btn_facebook.layer.cornerRadius = 5
        btn_signUp.layer.cornerRadius = 5
        btn_googlePlus.addShadowImage(shadowStyle: .ShadowRightBottomAndLeft, shadowColor: nil, shadowRadius: 5)
        btn_facebook.addShadowImage(shadowStyle: .ShadowRightBottomAndLeft, shadowColor: nil, shadowRadius: 5)
    }
    
    //MARK: Registster user
    func registerUser() {
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        var dataStore = DataStore()
        dataStore.delegat_register = self
        dataStore.registerUser(info: prepareJson())
    }
    func didRegisterUser(response: ResponseRegistration?) {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        if let _response = response {
            if _response.status.key == SUCCESS_RESPONSE_KEY {
                getToken()
            }
            if _response.status.key == "FORM_ERROR" {
                DataUtility.sharedInstance.hideActivityIndicator(self.view)
                if    let error = _response.payload.first?.form.children.error {
                    DataUtility.sharedInstance.showMessage(error.title, message:error.reason)
                }
                else if  let error = _response.payload.first?.form.children.plainPassword.children.first.errors {
                    DataUtility.sharedInstance.showMessage("Info", message:error.first!)
                }
                else {
                    DataUtility.sharedInstance.showMessage("Info", message:_response.status.key )
                }
            }
            else if _response.status.key == "FORM_ERROR"{
                
            }
        }
    }
    
    private func prepareJson()->RegistrationJSON {
        var json = Dictionary<String,Any>()
        var registration_form = Dictionary<String,Any>()
        var plainPassword = Dictionary<String,Any>()
        json["email"] = tf_email.text!
        json["name_and_surname"] = tf_nameAndSurname.text!
        if DataUtility.sharedInstance.getPromoCode() != nil {
            json["promo"] = DataUtility.sharedInstance.getPromoCode()
        }
        //json["username"] = tf_email.text!
        if (!(tf_password.text!.isEmpty) && !(tf_confirmPassword.text!.isEmpty)){
            plainPassword["first"] = tf_password.text!
            plainPassword["second"] = tf_confirmPassword.text!
            json["plainPassword"] = plainPassword
        }
        registration_form["registration_form"] = json
        
        return registration_form
    }
    //MARK: Get token via Facebook
    func getFacebookToken(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        let json      = prepareFacebookLogin()
        var dataStore = DataStore()
        dataStore.delegat_getToken = self
        dataStore.getToken(info: json)
    }
    //facebook login
    private func prepareFacebookLogin()->Dictionary<String,String>{
        var json         = UserToken()
        json.fb_token = DataUtility.sharedInstance.getFBToken()
        json.client_secret = " 4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
        json.client_id = "1_5zt2fjzasbs4sc0w8kgg80wssc8k008ogcskocoo4sog00wo88"
        json.grant_type  = "password"
        return json.toDictionary()
    }
    //MARK: Get token
    func getToken(){
        let json      = prepareJsonGetToken()
        var dataStore = DataStore()
        dataStore.delegat_getToken = self
        dataStore.getToken(info: json)
    }
    
    private func prepareJsonGetToken()->Dictionary<String,String>{
        var json         = UserToken()
        json.password    = tf_password.text!
        let email_encode = tf_email.text!
        json.username    = email_encode
        json.client_secret = "4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
        json.grant_type  = "password"
        return json.toDictionary()
    }
    
    func didGetToken(response: ResponseLogin?) {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        if let _response = response {
            if _response.status.key == SUCCESS_RESPONSE_KEY{
                //DataUtility.sharedInstance.ordinaryLoginPass(success: "OK")
                DataUtility.sharedInstance.saveUserCredentials(username: tf_email.text!, password: tf_password.text!)
                saveToken(info: _response.payload.first!)
                registerDevice()
                
            }
            else if _response.status.key == BAD_DATA_RESPONSE_KEY{
                DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.messages.last!)
            }
        }
    }
    
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
        DataUtility.sharedInstance.saveUser(email: tf_password.text!, password: tf_email.text!)
    }
    
    //MARK: Register device
    func registerDevice(){
        var dataStore = DataStore()
        dataStore.delegat_registerDevice = self
        dataStore.registerDevice(info: DataUtility.sharedInstance.getDeviceInfo() as! Dictionary<String, String>)
    }
    
    func didRegisterDevice() {
        self.performSegue(withIdentifier: "signUp", sender: self)
    }
    
    //MARK: Textfields
    func textFieldDidBeginEditing(_ textField: CustomTextField) {
        self.activeTextField = textField
        activeTextField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: CustomTextField) {
        if activeTextField == tf_email {
            if tf_email.text?.isValidEmail() == false {
                formStatus = false
                let alertController = UIAlertController(title: "Invalid email", message: "Please enter valid email.", preferredStyle:UIAlertControllerStyle.alert)
                
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                { action -> Void in
                    print("invalid mail")
                })
                self.present(alertController, animated: true, completion: nil)
                return

            }
            else {
                    formStatus = true
                    return
            }
        }
        
        if activeTextField.text?.isEmpty == true {
            formStatus = false
        }
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: CustomTextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
