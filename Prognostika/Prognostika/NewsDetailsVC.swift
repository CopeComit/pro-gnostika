//
//  NewsDetailsVC.swift
//  Prognostika
//
//  Created by Cope on 3/8/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class NewsDetailsVC: UITableViewController, HeaderViewDelegate, ProtocolUserPredictions, ProtocolRefreshToken{

    //MARK: Variables
    let button = UIButton()
    let backButton = UIButton()
    var activePrediction : Predictions!
    var isPending = appdelegat.isPending
    var isDeleting : Bool!
    //MARK: Outlets

    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.backgroundColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 41.0/255.0, alpha: 1.0)
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController?.prefersStatusBarHidden

        self.tableView.tableHeaderView = HeaderView.init(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: UIScreen.main.bounds.height * 0.307))
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 2000
        createDeleteButton()
        createBackButton()
        addHeaderViewObj()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let topOffest = CGPoint(x: 0, y: -(self.tableView.contentInset.top))
        self.tableView.contentOffset = topOffest
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        sendPrediction()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        isDeleting = false
        appdelegat.payload = 0
        appdelegat.offset = 0
    }
    //MARK: Functions
    
    
    //MARK: Send user predictions
    func deletePrediction(){
        if activePrediction.deletedAt == nil || activePrediction.deletedAt == ""{
            isDeleting = true
            if activePrediction.userPredictionID == nil {
                postDeleteUserPredictions()
            }
            else{
                patchDeleteUserPredictions()
            }
        }
    }
    func sendPrediction(){
        if activePrediction.seenAt == nil  || activePrediction.seenAt == ""{
            if activePrediction.userPredictionID == nil {
                postUserPredictions()
            }
            else{
                patchUserPredictions()
            }
        }
        
    }
    func postUserPredictions(){
        var dataStore = DataStore()
        dataStore.delegat_userPredictions = self
        dataStore.postUserPredictions(info: preparePostJSON())
    }
    func patchUserPredictions(){
        var dataStore = DataStore()
        dataStore.delegat_userPredictions = self
        dataStore.patchUserPredictions(info: preparePatchJSON(), id: activePrediction.userPredictionID)
    }
    //seen at
    func preparePostJSON()->Dictionary<String,Any>{
        var json = Dictionary<String,Any>()
        var userPrediction = Dictionary<String,Any>()
        userPrediction["user"] = activePrediction.userId
        userPrediction["prediction"] = activePrediction.id
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        userPrediction["seenAt"] = result
        json["user_prediction"] = userPrediction
        
        return json
    }
    func preparePatchJSON()->Dictionary<String,Any>{
        var json = Dictionary<String,Any>()
        var userPrediction = Dictionary<String,Any>()
        let date = NSDate()
        let dateFormatter = DateFormatter()
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone = timeZone as TimeZone!
        print(dateFormatter.string(from: date as Date))
        userPrediction["seenAt"] = dateFormatter.string(from: date as Date)
        json["user_prediction"] = userPrediction
        
        return json
    }
    //deleted at
    func postDeleteUserPredictions(){
        var dataStore = DataStore()
        dataStore.delegat_userPredictions = self
        dataStore.postUserPredictions(info: prepareDeletePostJSON())
    }
    func patchDeleteUserPredictions(){
        var dataStore = DataStore()
        dataStore.delegat_userPredictions = self
        dataStore.patchUserPredictions(info: prepareDeletePatchJSON(), id: activePrediction.userPredictionID)
    }
    func prepareDeletePostJSON()->Dictionary<String,Any>{
        var json = Dictionary<String,Any>()
        var userPrediction = Dictionary<String,Any>()
        userPrediction["user"] = activePrediction.userId
        userPrediction["prediction"] = String(activePrediction.id)
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        userPrediction["deletedAt"] = result
        json["user_prediction"] = userPrediction
        
        return json
    }
    func prepareDeletePatchJSON()->Dictionary<String,Any>{
        var json = Dictionary<String,Any>()
        var userPrediction = Dictionary<String,Any>()
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        userPrediction["deletedAt"] = result
        json["user_prediction"] = userPrediction
        
        return json
    }
    func didSendUserPredictions(response: ResponsePredictions?) {
        if response != nil {
            if response?.status.key == "OK" {
                if isDeleting != nil && isDeleting == true{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            else if response?.status.key == "TOKEN_EXPIRED" {
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                saveSuspendedDeviceInfo(info: (response?.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
    }
    //MARK: Refresh token
    func refreshToken(){
        //let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_refreshToken = self
        dataStore.refreshToken()
    }
    func didRefreshToken(response: ResponseLogin?) {
        if let _response = response {
            if _response.status.key == "OK"{
                saveToken(info: _response.payload.first!)
                //call again
                sendPrediction()
            }
        }
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
    }
    //MARK: Save suspended device
    private func saveSuspendedDeviceInfo(info : Predictions){
        DataUtility.sharedInstance.saveSuspendedDeviceInfo(username:DataUtility.sharedInstance.getUserCredentials().username, suspended_until: info.suspended_until)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    //TODO: setup design for pending bets
    func setupDesginForPendingBets(){
        
    }
    func createDeleteButton(){
        let yFrame = UIScreen.main.bounds.height - (UIScreen.main.bounds.height * 0.0625 )
        button.frame = CGRect(x: 0, y: yFrame, width: self.view.frame.size.width, height: UIScreen.main.bounds.height * 0.0625)
        button.backgroundColor = UIColor(colorLiteralRed: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
        button.setTitle("ΔΙΑΓΡΑΦΩ", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)
    }
    func createBackButton(){
        backButton.frame = CGRect(x: 10, y: 30, width: 50, height: 50)
        backButton.backgroundColor = UIColor.clear
        backButton.setImage(UIImage(named: "arrow_back"), for: .normal)
        backButton.addTarget(self, action: #selector(actionBackButton(sender:)), for: .touchUpInside)
        self.view.addSubview(backButton)
    }
    func didPressBackButton(button: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromRight
        view.window!.layer.add(transition, forKey: kCATransition)
        let VC1 = storyboard.instantiateViewController(withIdentifier: "NewsListVC") as! NewsListVC
        let navController = UINavigationController(rootViewController: VC1)
        self.present(navController, animated:false, completion: nil)
    }
    
    func actionBackButton(sender: UIButton!){
        let header = HeaderView()
        header.delegate = self
        header.delegate?.didPressBackButton(button: sender)
    }
    
    func buttonAction(sender: UIButton!) {
        deletePrediction()
        print("Button delete tapped from VC")
    }
    
    func addHeaderViewObj() {
        let header = HeaderView()
        header.delegate = self
        self.view.addSubview(header)
    }
    //MARK: Table
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = self.tableView.tableHeaderView as! HeaderView
        headerView.scrollViewDidScroll(scrollView: scrollView)
        //adjust button floating
        var frame: CGRect = self.button.frame
        frame.origin.y = scrollView.contentOffset.y + self.tableView.frame.size.height - self.button.frame.size.height
        self.button.frame = frame
        self.view.bringSubview(toFront: self.button)
        //adjust button floating back
        var backButtonframe: CGRect = self.backButton.frame
        backButtonframe.origin.y = scrollView.contentOffset.y + self.tableView.frame.size.height - ( UIScreen.main.bounds.height - 10)
        self.backButton.frame = backButtonframe
        self.view.bringSubview(toFront: self.backButton)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailsInfo")! as! NewsDetailsInfo
            cell.frame.size.height = 500.0
            cell.lbl_sportLeague.text = activePrediction.leagueTitle + " (" + activePrediction.countryTitle + "," + activePrediction.sportTitle + ")"
            cell.lbl_titleOfNews.text = activePrediction.bet
            cell.lbl_oddData.text = String(activePrediction.odd.cleanValue)
            cell.lbl_stakeData.text = String(activePrediction.stake)
            cell.lbl_profitData.text = activePrediction.profit
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsDetailsCell")! as! NewsDetailsCell
            //fill data
            if let contentHtml = activePrediction.content {
                //lbl_newsDetails.text =  activePrediction.content
                cell.lbl_news_text.from(html: contentHtml)
                cell.lbl_news_text.font = UIFont.systemFont(ofSize: 15)

            }
            cell.lbl_title.text = activePrediction.title

            return cell
        }
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "NextButtonTableViewCell")! as! ButtonDeleteNewsCell
        
        return header.contentView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 50
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UIScreen.main.bounds.height * 0.307
        }
        else {
            return UITableViewAutomaticDimension
        }
    }
}
