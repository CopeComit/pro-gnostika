//
//  ButtonBorderExtension.swift
//  Prognostika
//
//  Created by Cope on 3/6/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation
import UIKit

typealias roundedButon = UIButton
extension roundedButon {
    func buttonBorder(button : UIButton, borderColor: UIColor)->UIButton {
        let roundedButton = UIButton()
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        
        return button
    }
}

