//
//  LoginAndRegisterVC.swift
//  Prognostika
//
//  Created by Cope on 3/2/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit
import Foundation
class LoginAndRegisterVC: UIViewController, ProtocolSuccesLoginUser {

    //MARK: Variables
    
    //MARK: Outlets
    @IBOutlet weak var img_logo: UIImageView!
    @IBOutlet weak var img_bckg: UIImageView!
    @IBOutlet weak var view_bottom_container: UIView!
    
    //Constraints
    @IBOutlet weak var contraint_img_logo_top: NSLayoutConstraint!
    @IBOutlet weak var constraint_img_logo_left: NSLayoutConstraint!
    @IBOutlet weak var constraint_img_logo_bottom: NSLayoutConstraint!
    @IBOutlet weak var constraint_img_logo_right: NSLayoutConstraint!
    //Buttons
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var btn_register: UIButton!

    //MARK: Actions
    @IBAction func ActionLogin(_ sender: UIButton) {
        if  let _ = DataUtility.sharedInstance.getSuspendedDeviceInfo() {
            DataUtility.sharedInstance.checkIfSuspensionDateExpired()
            DataUtility.sharedInstance.showMessage("Access denied", message:"Suspended until:" + DataUtility.sharedInstance.getSuspendedDeviceInfo()!)
        }
        else {
            self.performSegue(withIdentifier: "siginIn", sender: self)
        }
}
    
    @IBAction func ActionSignUp(_ sender: UIButton) {
        if  let _ = DataUtility.sharedInstance.getSuspendedDeviceInfo() {
            DataUtility.sharedInstance.checkIfSuspensionDateExpired()
            DataUtility.sharedInstance.showMessage("Access denied", message:"Suspended until:" + DataUtility.sharedInstance.getSuspendedDeviceInfo()!)
        }
        else{
            self.performSegue(withIdentifier: "signUp", sender: self)
        }
    }
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        setupDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        view_bottom_container.isHidden = true
        if  let _ = DataUtility.sharedInstance.getSuspendedDeviceInfo() {
            DataUtility.sharedInstance.checkIfSuspensionDateExpired()
            DataUtility.sharedInstance.showMessage("Access denied", message:"Suspended until:" + DataUtility.sharedInstance.getSuspendedDeviceInfo()!)
            view_bottom_container.isHidden = false
        }
        else {
            tryAutomaticLogin()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    //MARK: Functions
    func setupDesign(){
        let constraints = ElementConstratinsProportion()
        let constraintInfo = constraints.resizeConstraint(originalTopConstraint: 149, originalRightConstraint: nil,originalBottomConstraint: nil, originalLeftConstraint: nil)
        contraint_img_logo_top.constant = constraintInfo.value(forKey: "constraint_top") as! CGFloat
        btn_login.backgroundColor = .clear
        btn_login.layer.cornerRadius = 5
        btn_login.layer.borderWidth = 0.5
        btn_login.layer.borderColor = UIColor.black.cgColor
        btn_register.layer.cornerRadius = 5
        
    }
    //MARK: Automatic login
    private func tryAutomaticLogin(){
        if let _fbtoken  = DataUtility.sharedInstance.getFBToken() {
            appdelegat.isFBLogin = false
            loginFbOrGoogle(valid_token: _fbtoken, isFbToken: true)
            return
        }
        if let _ = DataUtility.sharedInstance.getOrdinaryLogin() {
            loginUser()
            return
        }
        else {
            view_bottom_container.isHidden = false
        }
    }
    
    private func loginUser(){
        let credentials  = DataUtility.sharedInstance.getUserCredentials()
        let login = LoginUtility(username: credentials.username!, password: credentials.password!)
        
        login.delegat_success_login = self
        login.delegat_view = self
        login.loginUser()
    }
    //MARK: Login fb or google
    private func loginFbOrGoogle(valid_token : String, isFbToken : Bool){
        var login : LoginUtility!
        if isFbToken == true {
            login = LoginUtility(fb_token: valid_token)
        }else {
            login = LoginUtility(google_token: valid_token)
        }
        login.delegat_success_login = self
        login.delegat_view = self
        login.loginUser()
    }
    func didSuccessLoginUser() {
        openNewsListVC()
    }
    
    func openNewsListVC(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC1 = storyboard.instantiateViewController(withIdentifier: "NewsListVC") as! NewsListVC
        let navController = UINavigationController(rootViewController: VC1)
        self.present(navController, animated:false, completion: nil)
    }
}
