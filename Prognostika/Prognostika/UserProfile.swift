//
//	Payload.swift
//
//	Create by grom on 17/12/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
//MARK: TODO Check profile fields with puza and miha
struct UserProfile{
    
    var id : Int!
    var username : String!
    var email : String!
    var enabled : Bool!
    var last_login : String!
    var nameAndSurname : String!
    var remaining_units : Double!
    //Extra arguments
    var userPredictionsID : Int!
    var boughtAt  : String!
    var seenAt    : String!
    var deletedAt : String!
    //SUSPENDED
    var suspended_until : String!
    //old Fields
	var credentialsExpired : Bool!
	var expired : Bool!
	var locked : Bool!
    var validTo : String!
    //arguments for facebook
    var fb_token : String!
    var google_token : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
        suspended_until = dictionary["suspended_until"] as? String
		credentialsExpired = dictionary["credentials_expired"] as? Bool
		email = dictionary["email"] as? String
		enabled = dictionary["enabled"] as? Bool
		expired = dictionary["expired"] as? Bool
		id = dictionary["id"] as? Int
		locked = dictionary["locked"] as? Bool
		nameAndSurname = dictionary["name_and_surname"] as? String
		username = dictionary["username"] as? String
        validTo = dictionary["validTo"] as? String
        userPredictionsID = dictionary["userPredictionsID"] as? Int
        remaining_units = dictionary["remainingUnits"] as? Double
        last_login = dictionary["last_login"] as? String
        //extra arguments
        boughtAt = dictionary["boughtAt"] as? String
        seenAt = dictionary["seenAt"] as? String
        deletedAt = dictionary["deletedAt"] as? String
        suspended_until = dictionary["suspended_until"] as? String
        //facebook
        fb_token = dictionary["fb_token"] as? String
        google_token = dictionary["google_token"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		var dictionary = NSMutableDictionary()
		if credentialsExpired != nil{
			dictionary["credentials_expired"] = credentialsExpired
		}
		if email != nil{
			dictionary["email"] = email
		}
		if enabled != nil{
			dictionary["enabled"] = enabled
		}
		if expired != nil{
			dictionary["expired"] = expired
		}
		if id != nil{
			dictionary["id"] = id
		}
		if locked != nil{
			dictionary["locked"] = locked
		}
		if nameAndSurname != nil{
			dictionary["name_and_surname"] = nameAndSurname
		}
		if username != nil{
			dictionary["username"] = username
		}
        if suspended_until != nil{
            dictionary["suspended_until"] = suspended_until
        }
        if validTo != nil{
            dictionary["validTo"] = validTo
        }
        if userPredictionsID != nil{
            dictionary["userPredictionsID"] = userPredictionsID
        }
        if remaining_units != nil{
            dictionary["remainingUnits"] = remaining_units
        }
        if last_login != nil{
            dictionary["last_login"] = last_login
        }
        //extra arguments
        if boughtAt != nil{
            dictionary["boughtAt"] = boughtAt
        }
        if seenAt != nil{
            dictionary["seenAt"] = seenAt
        }
        if deletedAt != nil{
            dictionary["deletedAt"] = deletedAt
        }
        if suspended_until != nil{
            dictionary["suspended_until"] = suspended_until
        }
        if fb_token != nil {
            dictionary["fb_token"] = fb_token
        }
        if google_token != nil {
            dictionary["google_token"] = google_token
        }
		return dictionary
	}

}
