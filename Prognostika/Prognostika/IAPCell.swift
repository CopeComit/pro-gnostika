//
//  IAPCell.swift
//  Prognostika
//
//  Created by Cope on 8/7/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit
import StoreKit
class IAPCell: UITableViewCell {

    @IBOutlet weak var imgUnderline: UIImageView!
    @IBOutlet weak var lblPacketName: UILabel!
    @IBOutlet weak var lblUnits: UILabel!
    @IBOutlet weak var btnBuy: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        
        return formatter
    }()
    
    var buyButtonHandler: ((_ product: SKProduct) -> ())?
    
    var product: SKProduct? {
        didSet {
            guard let product = product else { return }
            
            //textLabel?.text = product.localizedTitle
            
            if PrognostikaProducts.store.isProductPurchased(product.productIdentifier) {
                accessoryType = .checkmark
                accessoryView = nil
                //detailTextLabel?.text = ""
            } else if IAPHelper.canMakePayments() {
                IAPCell.priceFormatter.locale = product.priceLocale
                //detailTextLabel?.text = IAPCell.priceFormatter.string(from: product.price)
                
                accessoryType = .none
                accessoryView = self.newBuyButton()
            } else {
                //detailTextLabel?.text = "Not available"
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textLabel?.text = ""
        detailTextLabel?.text = ""
        accessoryView = nil
    }
    
    func newBuyButton() -> UIButton {
        let price : String = String(describing: product!.price) + "€"
        let button = UIButton(type: .system)
        button.setTitleColor(tintColor, for: UIControlState())
        button.setTitle(price , for: UIControlState())
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.addTarget(self, action: #selector(IAPCell.buyButtonTapped(_:)), for: .touchUpInside)
        button.sizeToFit()
        button.frame.size.width += 20
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        if price == "99.99€"{
            button.layer.borderColor = UIColor(colorLiteralRed: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
            button.setTitleColor(UIColor(colorLiteralRed: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0), for: .normal)
        }
        else{
            button.layer.borderColor = UIColor(colorLiteralRed: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
            button.setTitleColor(UIColor(colorLiteralRed: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0), for: .normal)
        }
        
        return button
    }
    
    func buyButtonTapped(_ sender: AnyObject) {
        buyButtonHandler?(product!)
    }

}
