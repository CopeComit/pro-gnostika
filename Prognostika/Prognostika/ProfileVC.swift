//
//  ProfileVC.swift
//  Prognostika
//
//  Created by Cope on 4/5/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController, ProtocolUserProfile, ProtocolRefreshToken {
    //MARK: Lets and Vars
    var userProfile =  [UserProfile]()
     var open : Bool!
    //MARK: Outlets
    @IBOutlet weak var lblEmailData: UILabel!
    @IBOutlet weak var lblUserNameData: UILabel!
    @IBOutlet weak var lblRemainingUnitsData: UILabel!
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var imgFirstContainer: UIImageView!
    @IBOutlet weak var imgSecondContainer: UIImageView!
    @IBOutlet weak var btn_buy: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var imgEmailUnderline: UIImageView!
    @IBOutlet weak var imgAccountType: UIImageView!
    @IBOutlet weak var constraintTopBtnSignout: NSLayoutConstraint!
    @IBOutlet weak var constraintFirstContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var imgSocialUnderline: UIImageView!
    @IBOutlet weak var imgSocialContainer: UIImageView!
    @IBOutlet weak var imgProfileUnderline: UIImageView!
    @IBOutlet weak var lblSignedInBy: UILabel!
    //share promo code outlets
    @IBOutlet weak var viewSharePromoCode: UIView!
    @IBOutlet weak var btnSharePromoCode: UIButton!
    @IBOutlet weak var lblPromoCode: UILabel!
    @IBOutlet weak var lblPromoCodeData: UILabel!
    
    //MARK: Actions
    func displayAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
        return
    }
    @IBAction func actionSharePromoCode(_ sender: Any) {
        let textforTEST = "In app testing..."
        // Check and see if the text field is empty
        if (textforTEST == "") {
            // The text field is empty so display an Alert
            displayAlert(title: "Warning", message: "Enter something in the text field!")
        } else {
            
            let shareText = "Hello my Friend \n, I'm happy to present you PROgnostika app. It is really the best service in the world. If you would like to beat the market, you are on right place. \n Please download app, install it, register with my promo code and enjoy all PROgnostika features and benefits. \n My promo code is:  \(lblPromoCodeData.text!) now! \n For more information you can take a look at http://pro-gnostika.gr"
            let iosString = "\niOS:"
            let androidString = "\nAndroid:"
            if let myWebsite = NSURL(string: "https://play.google.com/store/apps/details?id=com.vquarter.production&hl=en") { if let iosLINK = NSURL(string: "https://itunes.apple.com/se/app/vquarter/id951012702?mt=8"){
                let vc = UIActivityViewController(activityItems: [shareText,androidString,myWebsite,iosString,iosLINK], applicationActivities: [])
                
                vc.excludedActivityTypes = [.postToFacebook]
                present(vc, animated: true, completion: nil)
                }}
        }
    }
    @IBAction func openMenu(_ sender: Any) {
        self.view.backgroundColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 41.0/255.0, alpha: 1.0)
        if !open {
            self.captureScreen()
            self.showMenu(sender as! UIButton)
            open = true
        }
        else{
            self.closeMenu(sender as! UIButton)
            open = false
        }
    }
    
    @IBAction func actionEdit(_ sender: Any) {
        
    }
    @IBAction func logout(_ sender: Any) {
        DataUtility.sharedInstance.clearUser()
        DataUtility.sharedInstance.clearRememberUser()
        DataUtility.sharedInstance.clearAllLoginTokens()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC1 = storyboard.instantiateViewController(withIdentifier: "LoginAndRegisterVC") as! LoginAndRegisterVC
                    let navController = UINavigationController(rootViewController: VC1)
                    self.present(navController, animated:false, completion: nil)

        //OpenViewController(storyboard: UIStoryboard(name: "Main", bundle: nil), viewController: "LoginAndRegisterVC")
    }
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 20.0/255.0, green: 26.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        appdelegat.titleForVC(vc: self, title: "Profile",isTransparent: false)
        open = appdelegat.is_side_menu_open
        userProfile = []
        getUserProfile()
        setupDesign()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Functions
    func setupDesign(){
        if DataUtility.sharedInstance.getFBToken() != nil{
            imgFirstContainer.isHidden = true
            imgProfileUnderline.isHidden = true
            imgSocialContainer.isHidden = false
            imgSocialUnderline.isHidden = false
            lblEmailData.isHidden = true
            imgEmailUnderline.isHidden = true
            lblSignedInBy.isHidden = false
            lblSignedInBy.text = "Συνδεδεμένος από το facebook"
            imgAccountType.image = #imageLiteral(resourceName: "facebook avatar")
            constraintTopBtnSignout.constant = 8
            
        }
        btn_buy.layer.cornerRadius = 5
        imgSecondContainer.layer.cornerRadius = 5
        imgSecondContainer.addShadowImage(shadowStyle: .ShadowOnBottom, shadowColor: nil, shadowRadius: 5)
        //promo code box design
        viewSharePromoCode.layer.cornerRadius = 5
        viewSharePromoCode.addShadowImage(shadowStyle: .ShadowOnBottom, shadowColor: nil, shadowRadius: 5)
        btnSharePromoCode.frame.size.height = viewSharePromoCode.frame.size.height * 0.58
        btnSharePromoCode.layer.borderWidth = 1
        btnSharePromoCode.layer.borderColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        btnSharePromoCode.layer.cornerRadius = 5
    }
    func fillData(){
        lblEmailData.text = userProfile[0].email
        if userProfile[0].nameAndSurname != nil{
            lblUserNameData.text = userProfile[0].nameAndSurname
            DataUtility.sharedInstance.saveUserAdditionalCredentials(nameAndSurname: userProfile[0].nameAndSurname)
        }
        
        if userProfile[0].remaining_units != nil {
            let remainingUnits = String(userProfile[0].remaining_units)
            lblRemainingUnitsData.text = remainingUnits
        }
        
    }
    func getUserProfile(){
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        var dataStore = DataStore()
        dataStore.delegat_userProfile = self
        dataStore.getUserProfile()
    }
    func didGetUserProfile(response: ResponseUserProfile?) {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        if response != nil {
            if response?.status.key == "OK" {
                for profile in (response?.payload)! {
                    userProfile.append(profile)
                    if profile.remaining_units != nil{
                        lblRemainingUnitsData.text = String(profile.remaining_units!)
                    }
                    else {
                        lblRemainingUnitsData.text = "0"
                    }
                    
                }
                //userProfile = response?.payload.first
                fillData()
            }
            else if response?.status.key == "TOKEN_EXPIRED" {
                refreshToken()
            }
            else if response?.status.key == "DEVICE_SUSPENDED"{
                saveSuspendedDeviceInfo(info: (response?.payload.first)!)
                DataUtility.sharedInstance.clearUser()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"LoginAndRegisterVC") as! LoginAndRegisterVC
                self.present(viewController, animated: true)
            }
        }
    }
    //MARK: Refresh token
    func refreshToken(){
        //let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_refreshToken = self
        dataStore.refreshToken()
    }
    func didRefreshToken(response: ResponseLogin?) {
        if let _response = response {
            if _response.status.key == "OK"{
                //DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.key)
                saveToken(info: _response.payload.first!)
                //call again
                getUserProfile()
            }
        }
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
    }
    //MARK: Save suspended device
    private func saveSuspendedDeviceInfo(info : UserProfile){
        DataUtility.sharedInstance.saveSuspendedDeviceInfo(username:DataUtility.sharedInstance.getUserCredentials().username, suspended_until: info.suspended_until)
    }
    
}
