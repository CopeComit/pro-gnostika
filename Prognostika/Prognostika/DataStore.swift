import UIKit


protocol ProtocolRegister : class {
    func didRegisterUser(response : ResponseRegistration?)
}
protocol ProtocolGetToken : class {
    func didGetToken(response : ResponseLogin?)
}
protocol ProtocolRefreshToken : class {
    func didRefreshToken(response : ResponseLogin?)
}
protocol ProtocolRegisterDevice : class {
    func didRegisterDevice()
}
protocol ProtocolRequestPassword : class {
    func didRequestPassword(response: RequestPasswordResponse?)
}
protocol ProtocolResetPassword : class {
    func didResetPassword(response: RequestPasswordResponse?)
}
protocol ProtocolPredictions : class {
    func didGetPredictions(response : ResponsePredictions?)
}
protocol ProtocolFaq : class {
    func didGetFaq(response : ResponseFaq?)
}
protocol ProtocolRecentInfo : class {
    func didGetRecentInfo(response: ResponseRecentInfo?)
}
protocol ProtocolUserPredictions : class {
    func didSendUserPredictions(response: ResponsePredictions?)
}
protocol ProtocolGeneralStatistics : class {
    func didGetGeneralStatistics(response : ResponseGeneralStatistics?)
}
protocol ProtocolPersonalStatistics : class {
    func didGetPersonalStatistics(response : ResponsePersonalStatistics?)
}
protocol ProtocolUserProfile : class{
    func didGetUserProfile(response: ResponseUserProfile?)
}
protocol ProtocolPatchUserProfile : class{
    func didPatchUserProfile(response: ResponseUserProfile?)
}
protocol ProtocolEditProfile : class {
    func didEditProfile(response: ResponseEditProfile?)
}
protocol ProtocolSendEmail: class{
    func didSendEmail(response: ResponseContact?)
}
struct DataStore  {
    private let dataFetcher                 = DataFetcher()
    private let dataParser                  = DataParser()
    private let dataUrl                     = DataURL()
    
    weak var delegat_register               : ProtocolRegister?
    weak var delegat_registerDevice         : ProtocolRegisterDevice?
    var delegat_getToken                    : ProtocolGetToken?
    weak var delegat_refreshToken           : ProtocolRefreshToken?
    weak var delegat_requestPassword        : ProtocolRequestPassword?
    weak var delegat_resetPassword          : ProtocolResetPassword?
    weak var delegat_predictions            : ProtocolPredictions?
    weak var delegat_faq                    : ProtocolFaq?
    weak var delegat_recentInfo             : ProtocolRecentInfo?
    weak var delegat_userPredictions        : ProtocolUserPredictions?
    weak var delegat_personalStatistics     : ProtocolPersonalStatistics?
    weak var delegat_generalStatistics      : ProtocolGeneralStatistics?
    weak var delegat_userProfile            : ProtocolUserProfile?
    weak var delegat_patchUserProfile       : ProtocolPatchUserProfile?
    weak var delegat_editProfile            : ProtocolEditProfile?
    weak var delegat_contact                : ProtocolSendEmail?
    //MARK: Register user
    func registerUser(info : [String : Any]){
        let pathToRegister = DataURL.sharedInstance.register
        DataFetcher.sharedInstance.getData(pathToRegister, authorization: _USER_AUTHORIZATION, httpHeader: nil, json: info, method: .POST) { (object) in
            if let _object = object{
                let parse_result = self.dataParser.parseJson(_object as! JSON, .Register)
                if let _delegat = self.delegat_register{
                    mainTask {
                       _delegat.didRegisterUser(response: parse_result as? ResponseRegistration)
                    }
                }
                
            }
        }
    }
    //MARK: Register device
    func registerDevice(info : Dictionary<String,Any>){
        let pathToRegister = DataURL.sharedInstance.registerDevice
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        DataFetcher.sharedInstance.getData(pathToRegister, authorization: "Bearer " + accessToken!, httpHeader: nil, json: info, method: .POST) {(object) in
           // if object == nil {
                //let parse_result = self.dataParser.parseJson(object as! JSON, .RegisterDevice)
                if let _delegat = self.delegat_registerDevice {
                    mainTask {
                        _delegat.didRegisterDevice()
                    }
                }
            //}
        }
    }
    //MARK: Get token
    func getToken(info : Dictionary<String,String>){
        var pathToRegister = ""
        let username = info["username"]
        let password = info["password"]
        let fbToken = DataUtility.sharedInstance.getFBToken()
        var jsonString = ""
        if let _username = username , let _password = password {
            jsonString = "?client_id=1_5zt2fjzasbs4sc0w8kgg80wssc8k008ogcskocoo4sog00wo88&client_secret=4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8&grant_type=password&password=" + _password + "&username=" + _username
        }
        if fbToken != nil{
            jsonString = "?client_id=1_5zt2fjzasbs4sc0w8kgg80wssc8k008ogcskocoo4sog00wo88&client_secret=4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8&fb_token=" + "\(fbToken!)" + "&grant_type=password"
        }
        pathToRegister = DataURL.sharedInstance.requestTokenPassword + jsonString
        DataFetcher.sharedInstance.getData(pathToRegister, authorization: nil, httpHeader: nil, json: nil, method: .GET) {(object) in
                if let _object = object {
                    let result = DataParser.sharedInstance.parseJson(_object as! JSON, .GetToken)
                    if let _delegat = self.delegat_getToken {
                        mainTask {
                            _delegat.didGetToken(response: result as? ResponseLogin)
                        }
                    }
                }
            }
    }
    //MARK: Refresh token
    func refreshToken(){
        let refreshToken = DataUtility.sharedInstance.getRefreshToken()
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        var pathToRegister = ""
        
        var jsonString = ""
        if refreshToken != nil{
            jsonString = "?client_id=2_5zt2fjzasbs4sc0w8kgg80wssc8k008ogcskocoo4sog00wo88&client_secret=4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8&grant_type=refresh_token&refresh_token=" + refreshToken!
        }
        pathToRegister = DataURL.sharedInstance.requestTokenPassword + jsonString
        
        DataFetcher.sharedInstance.getData(pathToRegister, authorization: "Bearer "+accessToken!, httpHeader: nil, json: nil, method: .GET) {(object) in
            if let _object = object {
                let result = DataParser.sharedInstance.parseJson(_object as! JSON, .GetToken)
                if let _delegat = self.delegat_refreshToken {
                    mainTask {
                        _delegat.didRefreshToken(response: result as! ResponseLogin?)
                    }
                }
            }
        }
    }
    //MARK: Request new password
    func requestPassword(info : Dictionary<String,String>){
        let pathToRegister = DataURL.sharedInstance.requestPassword
        //let accessToken = DataUtility.sharedInstance.getAccessToken()
        DataFetcher.sharedInstance.getData(pathToRegister, authorization: nil, httpHeader: nil, json: info, method: .POST) {(object) in
            if let _object = object {
                let result = DataParser.sharedInstance.parseJson(_object as! JSON, .RequestPassword)
                if let _delegat = self.delegat_requestPassword {
                    mainTask {
                        _delegat.didRequestPassword(response: result as! RequestPasswordResponse? )
                    }
                }
            }
        }
    }
    //MARK: Reset password
    func resetPassword(info : Dictionary<String,Any>){
        let path = DataURL.sharedInstance.resetPassword
        DataFetcher.sharedInstance.getData(path, authorization: nil, httpHeader: nil, json: info, method: .POST) { (object) in
            if let  _object = object{
                let result = DataParser.sharedInstance.parseJson(_object as! JSON, .RequestPassword)
                if let _delegat = self.delegat_resetPassword {
                    mainTask {
                        _delegat.didResetPassword(response: result as? RequestPasswordResponse)
                    }
                }
            }
        }
    }
    //MARK: Get predictions
    func getPredictions(isPredictionRoom : Int) {
        var pathToGetPredictions = ""
        let predictionsExtension = "&limit=20&offset=" + "\(appdelegat.offset)"
        
        if isPredictionRoom == 1 {
            pathToGetPredictions = DataURL.sharedInstance.getActivePredictions
        }else if isPredictionRoom == 2{
            pathToGetPredictions = DataURL.sharedInstance.getPendingPredictions
        }else if isPredictionRoom == 3{
            pathToGetPredictions = DataURL.sharedInstance.getOutcomesPredictions
        }
        
        if appdelegat.offset != 0 && appdelegat.payload != 0 {
            pathToGetPredictions = pathToGetPredictions + predictionsExtension
        }
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        DataFetcher.sharedInstance.getData(pathToGetPredictions, authorization: "Bearer " + accessToken!, httpHeader: nil, json: nil, method: .GET) { (object) in
            if let _object = object{
                print(_object)
                let parse_result = self.dataParser.parseJson(_object as! JSON, .Predictions)
                if let _delegat = self.delegat_predictions{
                    mainTask {
                        _delegat.didGetPredictions(response: parse_result as! ResponsePredictions?)
                    }
                }
                
            }
        }
    }
    //MARK: Send user predictions
    func postUserPredictions(info: Dictionary<String, Any>){
        let path = DataURL.sharedInstance.userPredictions
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        
        DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: info, method: .POST) {(object) in
            if let _object = object {
                let result = DataParser.sharedInstance.parseJson(_object as! JSON, .Predictions)
                if let _delegat = self.delegat_userPredictions {
                    mainTask {
                        _delegat.didSendUserPredictions(response: result as! ResponsePredictions?)
                    }
                }
            }
        }
    }
    func patchUserPredictions(info: Dictionary<String, Any>, id: Int){
        let path = DataURL.sharedInstance.userPredictions + "/" + "\(id)"
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        
        DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: info, method: .PATCH) {(object) in
            if let _object = object {
                let result = DataParser.sharedInstance.parseJson(_object as! JSON, .Predictions)
                if let _delegat = self.delegat_userPredictions {
                    mainTask {
                        _delegat.didSendUserPredictions(response: result as! ResponsePredictions?)
                    }
                }
            }
        }
    }
    //MARK: Get faq
    func getFaq(){
        let path = DataURL.sharedInstance.getFaq
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        
        DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: nil, method: .GET) { (object) in
            if let _object = object{
                let parse_result = self.dataParser.parseJson(_object as! JSON, .Faq)
                if let _delegat = self.delegat_faq{
                    mainTask {
                        _delegat.didGetFaq(response: (parse_result as! ResponseFaq?)!)
                    }
                }
            }
        }
    }
    //MARK: Get recent info
    func getRecentInfo() {
        let path = DataURL.sharedInstance.getRecentInfo
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: nil, method: .GET) { (object) in
            if let _object = object{
                let parse_result = self.dataParser.parseJson(_object as! JSON, .RecentInfo)
                if let _delegat = self.delegat_recentInfo{
                    mainTask {
                        _delegat.didGetRecentInfo(response: parse_result as! ResponseRecentInfo?)
                    }
                }
                
            }
        }
    }
    //MARK: Patch user profile
    func patchUserProfile(info : Dictionary<String,Any>, id : String){
        var path = DataURL.sharedInstance.getUserProfile + "/" + id
//        let user = info["user"] as! NSDictionary
//        let remainingUnits = user["remainingUnits"] as! String
//        let urlSufix = "?_format=json&user%5BremainingUnits%5D=" + "\(String(describing: remainingUnits))"
//        path = path + urlSufix
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        
        
        //if appdelegat.pricePublicValueForUserPatch != nil && appdelegat.pricePublicValueForUserPatch != ""{
            DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: info, method: .POST) { (object) in
                if let _object = object{
                    let parse_result = self.dataParser.parseJson(_object as! JSON, .UserProfile)
                    if let _delegat = self.delegat_patchUserProfile{
                        mainTask {
                            _delegat.didPatchUserProfile(response: parse_result as! ResponseUserProfile?)
                        }
                    }
                }
            }
        //}
    }
    //MARK: Get user profile
    func getUserProfile(){
        let path = DataURL.sharedInstance.getUserProfile
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: nil, method: .GET) { (object) in
            if let _object = object{
                let parse_result = self.dataParser.parseJson(_object as! JSON, .UserProfile)
                if let _delegat = self.delegat_userProfile{
                    mainTask {
                        _delegat.didGetUserProfile(response: parse_result as! ResponseUserProfile?)
                    }
                }
            }
        }
    }
    //MARK: Edit profile
    func editProfile(info : Dictionary<String,Any>){
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        let userId = appdelegat.userProfile.first!.id!
        let path = DataURL.sharedInstance.editUser + String(describing: userId)
        
        DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: info, method: .POST) {(object) in
            if let _object = object {
                let parse_result =  self.dataParser.parseJson(_object as! JSON, .EditProfile)
                if let _delegat = self.delegat_editProfile {
                    mainTask {
                        _delegat.didEditProfile(response: parse_result as? ResponseEditProfile)
                    }
                }
            }
        }
    }
    //MARK: Get general statictis
    func getGeneralStatistics(){
        var path = DataURL.sharedInstance.getGeneralStatistics
        let pathExtension = appdelegat.statisticsFilter
        if pathExtension != ""{
            path = path + pathExtension
            print(path)
            path = (path.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSString) as URLPath
        }
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: nil, method: .GET) { (object) in
            if let _object = object{
                let parse_result = self.dataParser.parseJson(_object as! JSON, .GeneralStatistics)
                if let _delegat = self.delegat_generalStatistics{
                    mainTask {
                        print(parse_result ?? "no data1!!!!!")
                        _delegat.didGetGeneralStatistics(response: parse_result as! ResponseGeneralStatistics?)
                        
                    }
                }
                
            }
        }
    }
    //MARK: Get personal statictis
    func getPersonalStatistics(){
        var path = DataURL.sharedInstance.getPersonalStatistics
        let pathExtension = appdelegat.statisticsFilter
        if pathExtension != ""{
            path = path + pathExtension
            print(path)
            path = (path.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSString) as URLPath
        }
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        DataFetcher.sharedInstance.getData(path, authorization: "Bearer " + accessToken!, httpHeader: nil, json: nil, method: .GET) { (object) in
            if let _object = object{
                let parse_result = self.dataParser.parseJson(_object as! JSON, .GeneralStatistics)
                if let _delegat = self.delegat_generalStatistics{
                    mainTask {
                        print(parse_result ?? "no data1!!!!!")
                        _delegat.didGetGeneralStatistics(response: parse_result as! ResponseGeneralStatistics?)
                        
                    }
                }
                
            }
        }
    }
    //MARK: Send contact form
    func sendContactForm(info : [String : Any]){
        let pathForContact = DataURL.sharedInstance.sendContact
        let accessToken = DataUtility.sharedInstance.getAccessToken()
        DataFetcher.sharedInstance.getData(pathForContact, authorization: "Bearer " + accessToken!, httpHeader: nil, json: info, method: .POST) { (object) in
            print(object)
            if let _object = object{
                let parse_result = self.dataParser.parseJson(_object as! JSON, .Contact)
                mainTask {
                    self.delegat_contact?.didSendEmail(response: parse_result as? ResponseContact)
                }
            }
        }
    }
}

