//
//  NewsDetailsInfo.swift
//  Prognostika
//
//  Created by Cope on 3/10/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class NewsDetailsInfo: UITableViewCell {
    
    @IBOutlet weak var lbl_odd          : UILabel!
    @IBOutlet weak var lbl_oddData      : UILabel!
    @IBOutlet weak var lbl_stake        : UILabel!
    @IBOutlet weak var lbl_stakeData    : UILabel!
    @IBOutlet weak var lbl_profit       : UILabel!
    @IBOutlet weak var lbl_profitData   : UILabel!
    
    @IBOutlet weak var lbl_newsDetails      : UILabel!
    @IBOutlet weak var lbl_subtitleOfNews   : UILabel!
    @IBOutlet weak var lbl_titleOfNews      : UILabel!
    @IBOutlet weak var lbl_sportLeague      : UILabel!
    @IBOutlet weak var lbl_validTillDate    : UILabel!
    @IBOutlet weak var lbl_roomStartup      : UILabel!
    @IBOutlet weak var lbl_bought           : UILabel!
    @IBOutlet weak var img_sportOfNews      : UIImageView!
    @IBOutlet weak var btn_delete        : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.frame.size.height = UIScreen.main.bounds.height * 0.307

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
