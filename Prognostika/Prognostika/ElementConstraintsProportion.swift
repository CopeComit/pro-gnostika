//
//  ElementConstraintsProportion.swift
//  Prognostika
//
//  Created by Cope on 3/2/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation
import UIKit

typealias ElementConstratinsProportion = NSDictionary 

extension ElementConstratinsProportion{

    func resizeConstraint(originalTopConstraint: CGFloat?, originalRightConstraint : CGFloat?, originalBottomConstraint : CGFloat?, originalLeftConstraint : CGFloat?)-> NSDictionary{
        
        var constraintSizes = [String: CGFloat]()
        let screenSize: CGRect = UIScreen.main.bounds
        
        // size of applications screen
        let actualScreenWidth     = screenSize.width
        let actualScreenHeight    = screenSize.height
        
        //size of mockup design screen
        let originalScreenWidth    : CGFloat = 360
        let originalScreenHeight   : CGFloat = 640
        
        //apply mockup design proportions to application
        //top constraint
        if originalTopConstraint != nil {
            let actualConstraintTop = (originalTopConstraint! / originalScreenHeight) * actualScreenHeight
            constraintSizes["constraint_top"] = actualConstraintTop
        }
        //right constraint
        if originalRightConstraint != nil {
            let actualConstraintRight     = (originalRightConstraint! / originalScreenWidth) * actualScreenWidth
            constraintSizes["constraint_right"] = actualConstraintRight
        }
        //bottom constraint
        if originalBottomConstraint != nil {
            let actualConstraintBottom     = (originalBottomConstraint! / originalScreenHeight) * actualScreenHeight
            constraintSizes["constraint_bottom"] = actualConstraintBottom
        }
        //left constraint
        if originalLeftConstraint != nil {
            let actualConstraintLeft     = (originalLeftConstraint! / originalScreenWidth) * actualScreenWidth
            constraintSizes["constraint_left"] = actualConstraintLeft
        }
        return constraintSizes as NSDictionary
    }
}
