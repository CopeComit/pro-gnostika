//
//  AppDelegate.swift
//  Prognostika
//
//  Created by Cope on 2/8/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import FBSDKCoreKit
import GoogleSignIn
import Google

let appdelegat = UIApplication.shared.delegate as! AppDelegate
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, ProtocolGetToken {

    var window: UIWindow?
    var is_side_menu_open = false
    var isFBLogin = false
    typealias userToken = String
    let lastToken = "token"
    let DEVICE_INFO = "deviceInfo"
    var offset = 0
    var payload = 0
    var isActivePrediction = 1
    var isPending = Bool()
    var lblValidUntil = String()
    let SUCCESS_RESPONSE_KEY         =  "OK"
    let BAD_DATA_RESPONSE_KEY        = "BAD_DATA"
    var statisticsFilter = ""
    var faqActiveIndex : Int!
    var faqPreviousIndex : Int!
    var faqMinusCell : Int!
    var activeSportImageLarge : String!
    var faqToBeOpened : Int!
    var faqToBeClosed : Int!
    var dontShowIAPDialog = false
    var pricePublicValueForUserPatch : String!
    var userProfile =  [UserProfile]()
    enum APN : String
    {
        case  APN_APS   = "aps"
        case  APN_ALERT = "alert"
        case  APN_SOUND = "sound"
        case  APN_BADGE = "badge"
        case  APN_TITLE = "title"
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // APN
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {  
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        UIApplication.shared.statusBarView?.backgroundColor = .black
        let navBar = UINavigationBar.appearance()
        navBar.tintColor = UIColor.white
        IQKeyboardManager.sharedManager().enable = true
        saveInfoForPN()
        setupFBlogin(application: application, launchOptions: launchOptions)
        //FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    private func setupFBlogin(application : UIApplication ,  launchOptions : [UIApplicationLaunchOptionsKey: Any]?){
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if isFBLogin == false  {
            return GIDSignIn.sharedInstance().handle(url as URL!,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }else {
            let handle = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            return handle
        }
        return false
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func titleForVC(vc:UIViewController,title:String,isTransparent :Bool){
        
        vc.navigationController?.navigationBar.topItem?.title = ""
        let titleLabel = UILabel()
        let colour = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1)
        let attributes: [NSString : AnyObject] = [NSFontAttributeName as NSString: UIFont.systemFont(ofSize: 18, weight: UIFontWeightSemibold), NSForegroundColorAttributeName as NSString: colour, NSKernAttributeName as NSString : 1.0 as AnyObject]
        titleLabel.attributedText = NSAttributedString(string: title, attributes: attributes as [String : Any]?)
        titleLabel.sizeToFit()
        vc.navigationItem.titleView = titleLabel
        
        if isTransparent == true {
            vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            vc.navigationController?.navigationBar.shadowImage = UIImage()
            vc.navigationController?.navigationBar.isTranslucent = true
            vc.navigationController?.view.backgroundColor = UIColor.clear
        }
    }
    //MARK: APN
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        // Persist it in your backend in case it's new
        if let token = deviceToken.hexString as String?{
            print(token)
            saveLastToken(token: token)
            saveInfoForPN()
        }
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        if let info = data[APN.APN_APS.rawValue] as? NSDictionary
        {
            let alert = info.value(forKey: "alert") as? NSDictionary
            
            let alertTitle = alert?.value(forKey: "title") as? String
            let bodyTitle = alert?.value(forKey: "body") as? String
            DataUtility.sharedInstance.showMessage(alertTitle!, message: bodyTitle!)
            
            let _           =  info[APN.APN_SOUND.rawValue] as? String
            let badge       =  info[APN.APN_BADGE.rawValue] as? NSNumber
            
            if let _badge   = badge {
                
                UIApplication.shared.applicationIconBadgeNumber = _badge.intValue
            }
        }

    }
    
    //MARK: Get device and token information
    private func saveLastToken(token : userToken){
        UserDefaults.standard.setValue(token, forKey: self.lastToken)
        UserDefaults.standard.synchronize()
    }
    func getLastToken()->userToken?{
        return UserDefaults.standard.value(forKey: lastToken)as? userToken
    }
    //MARK: Get token when application enter foreground
    func getToken(){
//        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: "Loading")
        let json      = prepareJsonGetToken()
        var dataStore = DataStore()
        dataStore.delegat_getToken = self
        dataStore.getToken(info: json)
    }
    func prepareJsonGetToken()->Dictionary<String,String>{
        var json = UserToken()
        let username = DataUtility.sharedInstance.getUserCredentials().username
        let password = DataUtility.sharedInstance.getUserCredentials().password
        json.username = username
        json.password = password
        json.client_secret = "4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
        json.grant_type  = "password"

        return json.toDictionary()
    }
    func didGetToken(response: ResponseLogin?) {
        if let _response = response {
            if _response.status.key == SUCCESS_RESPONSE_KEY{
                DataUtility.sharedInstance.ordinaryLoginPass(success: "OK")
                DataUtility.sharedInstance.saveUserCredentials(username: DataUtility.sharedInstance.getUserCredentials().username!
, password: DataUtility.sharedInstance.getUserCredentials().password!)
                saveToken(info: _response.payload.first!)
            }
            else if _response.status.key == BAD_DATA_RESPONSE_KEY{
               // DataUtility.sharedInstance.hideActivityIndicator(self.view)
                DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.messages.last!)
            }
        }
    }
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
        DataUtility.sharedInstance.saveUser(email: DataUtility.sharedInstance.getUserCredentials().username!, password: DataUtility.sharedInstance.getUserCredentials().password!)
    }
    func saveInfoForPN(){
        var json = Dictionary<String,Any>()
        let deviceName = UIDevice.current.name
        json["device_name"] = deviceName
        json["device_token"] = getLastToken()
        json["device_identifier"] = getLastToken()
        let deviceModel = UIDevice.current.model
        json["device_model"] = deviceModel
        let deviceVersion = UIDevice.current.systemVersion
        json["device_version"] = deviceVersion
        let vendorID = UIDevice.current.identifierForVendor!.uuidString
        let iOSVendorID =  vendorID //"iOSVendorId" +
        json["device_identifier"] = iOSVendorID
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as! String
        json["app_version"] = appVersion
        json["app_name"] = appName
        json["app_id"] = "0000001"
        json["badge_allowed"] = "0"
        json["sound_allowed"] = "1"
        json["alert_allowed"] = "1"
        json["is_sandbox"] = "0"
        UserDefaults.standard.setValue(json, forKey: self.DEVICE_INFO)
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if DataUtility.sharedInstance.getUserCredentials().password != nil && DataUtility.sharedInstance.getUserCredentials().username != nil
        {
            getToken()
        }
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Prognostika")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }

}


//MARK: Extensions
extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

