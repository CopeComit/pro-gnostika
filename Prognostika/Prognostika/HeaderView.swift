//
//  HeaderView.swift
//  Parallex Auto Layout Demo
//
//  Created by Rune Madsen on 2015-08-30.
//  Copyright © 2015 The App Boutique. All rights reserved.
//

import UIKit
protocol HeaderViewDelegate {
    func didPressBackButton(button:UIButton)
}
class HeaderView: UIView {
    
    var delegate : HeaderViewDelegate?
    
    var heightLayoutConstraint = NSLayoutConstraint()
    var bottomLayoutConstraint = NSLayoutConstraint()
    //Label Valid until constraints
    var lblValidUntilheightLayoutConstraint = NSLayoutConstraint()
    var lblValidUntilbottomLayoutConstraint = NSLayoutConstraint()
    var lblValidUntilLeftLayoutConstraint = NSLayoutConstraint()

    //Golden image with bet label
    var imageGoldenBetHeightConstraint = NSLayoutConstraint()
    var imageGoldenBetBottomLayoutConstraint = NSLayoutConstraint()
    var imageGoldenBetLeftLayoutConstraint = NSLayoutConstraint()
    
    var containerView = UIView()
    var containerLayoutConstraint = NSLayoutConstraint()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        
        // The container view is needed to extend the visible area for the image view
        // to include that below the navigation bar. If this container view isn't present
        // the image view would be clipped at the navigation bar's bottom and the parallax
        // effect would not work correctly
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = UIColor.red
        self.addSubview(containerView)
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[containerView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["containerView" : containerView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[containerView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["containerView" : containerView]))
        containerLayoutConstraint = NSLayoutConstraint(item: containerView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1.0, constant: 0.0)
        self.addConstraint(containerLayoutConstraint)
        
        let imageView: UIImageView = UIImageView.init()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        
        let activeSport = appdelegat.activeSportImageLarge
        imageView.image = UIImage(named: activeSport!)
        containerView.addSubview(imageView)
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[imageView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["imageView" : imageView]))
        bottomLayoutConstraint = NSLayoutConstraint(item: imageView, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        containerView.addConstraint(bottomLayoutConstraint)
        heightLayoutConstraint = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: containerView, attribute: .height, multiplier: 1.0, constant: 0.0)
        containerView.addConstraint(heightLayoutConstraint)
        //Image golden bet
        let imageViewGoldenBet: UIImageView = UIImageView.init()
        imageViewGoldenBet.translatesAutoresizingMaskIntoConstraints = false
        imageViewGoldenBet.backgroundColor = UIColor.clear
        imageViewGoldenBet.clipsToBounds = true
        imageViewGoldenBet.contentMode = .scaleAspectFill
        imageViewGoldenBet.image = UIImage(named: "bet")
        containerView.addSubview(imageViewGoldenBet)
        ////bottom
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[imageViewGoldenBet]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["imageViewGoldenBet" : imageViewGoldenBet]))
        imageGoldenBetBottomLayoutConstraint = NSLayoutConstraint(item: imageViewGoldenBet, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        containerView.addConstraint(imageGoldenBetBottomLayoutConstraint)
        //height
        imageGoldenBetHeightConstraint = NSLayoutConstraint(item: imageViewGoldenBet, attribute: .height, relatedBy: .equal, toItem: containerView, attribute: .height, multiplier: 1/7, constant: 0.0)
        containerView.addConstraint(imageGoldenBetHeightConstraint)
        let imageGoldenWidthConstraint = NSLayoutConstraint(item: imageViewGoldenBet, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds
.width * 0.133)
        containerView.addConstraint(imageGoldenWidthConstraint)
        //Label valid until
        let lblValidUntil  : UITextView = UITextView.init()
        lblValidUntil.isUserInteractionEnabled = false
        lblValidUntil.textContainerInset.left = UIScreen.main.bounds
            .width * 0.152
        lblValidUntil.translatesAutoresizingMaskIntoConstraints = false
        lblValidUntil.clipsToBounds = true
        lblValidUntil.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.4)
        lblValidUntil.text = appdelegat.lblValidUntil
        lblValidUntil.textColor = UIColor.white
        containerView.addSubview(lblValidUntil)
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lblValidUntil]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["lblValidUntil" : lblValidUntil]))
        lblValidUntilbottomLayoutConstraint = NSLayoutConstraint(item: lblValidUntil, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        containerView.addConstraint(lblValidUntilbottomLayoutConstraint)
        lblValidUntilheightLayoutConstraint = NSLayoutConstraint(item: lblValidUntil, attribute: .height, relatedBy: .equal, toItem: containerView, attribute: .height, multiplier: 1/7, constant: 0.0)
        containerView.addConstraint(lblValidUntilheightLayoutConstraint)
        lblValidUntilLeftLayoutConstraint =  NSLayoutConstraint(item: lblValidUntil, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: imageViewGoldenBet, attribute: NSLayoutAttribute.leadingMargin, multiplier: 1, constant: 100)
        containerView.addConstraint(lblValidUntilLeftLayoutConstraint)

        containerView.bringSubview(toFront: imageViewGoldenBet)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func buttonAction(sender: UIButton!) {
        self.delegate?.didPressBackButton(button: sender)
        print("Button tapped")
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        containerLayoutConstraint.constant = scrollView.contentInset.top;
        let offsetY = -(scrollView.contentOffset.y + scrollView.contentInset.top);
        containerView.clipsToBounds = offsetY <= 0
        bottomLayoutConstraint.constant = offsetY >= 0 ? 0 : -offsetY / 2
        heightLayoutConstraint.constant = max(offsetY + scrollView.contentInset.top, scrollView.contentInset.top)
    }
}
