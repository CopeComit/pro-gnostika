//
//	ResponseEditProfileFirst.swift
//
//	Create by Apple on 19/12/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ResponseEditProfileFirst{

	var errors : [String]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		errors = dictionary["errors"] as? [String]
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if errors != nil{
			dictionary["errors"] = errors
		}
		return dictionary
	}

}
