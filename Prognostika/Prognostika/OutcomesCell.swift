//
//  OutcomesCell.swift
//  Prognostika
//
//  Created by Cope on 4/18/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class OutcomesCell: UITableViewCell {

    @IBOutlet weak var img_cellShadow: UIImageView!
    @IBOutlet weak var lblLeague: UILabel!
    @IBOutlet weak var lblBet: UILabel!
    @IBOutlet weak var lblOdd: UILabel!
    @IBOutlet weak var lblOddData: UILabel!
    @IBOutlet weak var lblStake: UILabel!
    @IBOutlet weak var lblStakeData: UILabel!
    @IBOutlet weak var lblProfit: UILabel!
    @IBOutlet weak var lblProfitData: UILabel!
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var lblOutcomeTitle: UILabel!
    @IBOutlet weak var imgOutcomeIndicator: UIImageView!
    
    @IBOutlet weak var imgLineOne: UIImageView!
    @IBOutlet weak var imgLineTwo: UIImageView!
    @IBOutlet weak var imgLineThree: UIImageView!
    @IBOutlet weak var imgLineFour: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
//        btn_buy.layer.cornerRadius = 5
//        btn_details.layer.cornerRadius = 5
        //img_cellShadow.layer.cornerRadius = 5
        img_cellShadow.addShadowImage(shadowStyle: .ShadowFourSides, shadowColor: UIColor.clear, shadowRadius: 5)
        //img_newsSport.roundedElement()
    }
}
