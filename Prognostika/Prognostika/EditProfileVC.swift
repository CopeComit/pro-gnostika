//
//  EditProfileVC.swift
//  Prognostika
//
//  Created by Cope on 5/5/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController, ProtocolEditProfile {

    //MARK: Variables
    var activeTextField                               = UITextField()
    //MARK: Outlets
    
    @IBOutlet weak var tfNameAndSurname: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfEnterPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    //MARK: Actions
    
    @IBAction func actionSave(_ sender: Any) {
        editUser()
    }
    @IBAction func actionCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        setupDesign()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func editUser() {
        var dataStore = DataStore()
        dataStore.delegat_editProfile = self
//        if !checkEmptyFields() {
//            return
//        }
        DataUtility.sharedInstance.showActivityIndicatory(self.view, message: MessageInfo.Loading.rawValue)
        dataStore.editProfile(info: prepareJson())
    }
    func setupDesign(){
        tfEmail.text = DataUtility.sharedInstance.getUserCredentials().username
        if DataUtility.sharedInstance.getUserAdditionalCredentials() != nil {
            tfNameAndSurname.text = DataUtility.sharedInstance.getUserAdditionalCredentials()
        }
    }
    
    private func prepareJson()->JSON {
        var registration_form = Dictionary<String,Any>()
        var json = Dictionary<String,Any>()
        var plainPassword = Dictionary<String,Any>()
        if (!(tfNameAndSurname.text!.isEmpty)) && (tfNameAndSurname.text != "Name and surename (optional)"){
            json["nameAndSurname"] = tfNameAndSurname.text!
        }
        if (!(tfEnterPassword.text!.isEmpty) && !(tfConfirmPassword.text!.isEmpty))  && ((tfEnterPassword.text != "New password") && (tfConfirmPassword.text != "Confirm password")){
            plainPassword["first"] = tfEnterPassword.text!
            plainPassword["second"] = tfConfirmPassword.text!
            json["plainPassword"] = plainPassword
        }
        
        registration_form["edit_profile_form"] = json
        
        return registration_form as JSON
    }
    func didEditProfile(response: ResponseEditProfile?) {
        DataUtility.sharedInstance.hideActivityIndicator(self.view)
        if let _response = response {
            if _response.status.key == "OK"{
                self.navigationController?.popViewController(animated: true)
            }
            if _response.status.key == "FORM_ERROR" && _response.payload.first?.errors.first != nil{
                DataUtility.sharedInstance.showMessage("Info", message: (_response.payload.first?.errors.first)!)
            }
            if _response.status.key == "FORM_ERROR" && _response.payload.first?.errors.first == nil{
                DataUtility.sharedInstance.showMessage("Info", message: "Και οι δύο κωδικοί πρόσβασης είναι απαραίτητοι")
            }
           
        }
        else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //FIXME: 3.Edit profile -> Kada kliknes na bilo koje polje i odes na sledece, ako nisi nista uneo, postavlja se engleski prevod. Da li to tako treba ili ne ?
    

    func checkEmptyFields() -> Bool{
        if tfNameAndSurname.text == "" || (tfNameAndSurname.text == "Name and surename (optional)"){
            DataUtility.sharedInstance.showMessage("Empty Field", message: "Please enter your name and surename")
            //return false
        }
        if (tfEnterPassword.text == "")  || (!(tfEnterPassword.text?.isEmpty)!) || (tfEnterPassword.text != "New password"){ //))
            DataUtility.sharedInstance.showMessage("Empty Field", message: "Και οι δύο κωδικοί πρόσβασης είναι απαραίτητοι")
            //return false
        }
        if (tfConfirmPassword.text?.isEmpty)! || (!(tfConfirmPassword.text?.isEmpty)! || (tfConfirmPassword.text != "Confirm password")){
            DataUtility.sharedInstance.showMessage("Empty Field", message: "Και οι δύο κωδικοί πρόσβασης είναι απαραίτητοι")
            //return false
        }
        
        else {
            return true
        }
        return true
    }
    //MARK: Textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField        = textField
        activeTextField.text        = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if activeTextField.text?.isEmpty == true {
            
            switch activeTextField.tag {
            case 11:
                activeTextField.placeholder = "Όνομα και Επώνυμο (προαιρετικό)"
//            case 12:
//                activeTextField.placeholder = "Username"
            case 13:
                activeTextField.placeholder = "Νέος Κωδικός"
            case 14:
                activeTextField.placeholder = "Επιβεβαίωση Κωδικού"
                
            default:
                print("Something else")
            }
        }
        self.view.endEditing(true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
