//
//  ForgotPasswordSecondVC.swift
//  Prognostika
//
//  Created by Cope on 3/22/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class ForgotPasswordSecondVC: UIViewController, ProtocolResetPassword, ProtocolGetToken{
    //MARK: Variables
    internal typealias RegistrationJSON = Dictionary<String,Any>
    let SUCCESS_RESPONSE_KEY         =  "OK"
    let BAD_DATA_RESPONSE_KEY        = "BAD_DATA"
    let FORM_ERROR = "FORM_ERROR"
    var activeTextField                               = UITextField()

    //MARK: Outlets
    @IBOutlet weak var tf_enterNewCode    : UITextField!
    @IBOutlet weak var tf_enterNewPassword: UITextField!
    @IBOutlet weak var tf_confirmPassword : UITextField!
    @IBOutlet weak var btn_saveNewPassword: UIButton!
    
    //MARK: Actions
    
    @IBAction func actionResetPassword(_ sender: Any) {
        resetPassword()
    }
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_saveNewPassword.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Reset password
    private func prepareJsonResetPassword()->RegistrationJSON {
        var json = Dictionary<String,Any>()
        var resetPassword = Dictionary<String,Any>()
        var firstSecond = Dictionary<String,Any>()
        firstSecond["first"] = tf_enterNewPassword.text!
        firstSecond["second"] = tf_confirmPassword.text!
        resetPassword["plainPassword"] = firstSecond
        resetPassword["token"] = tf_enterNewCode.text!
        json["reset_password"] = resetPassword
        return json
    }
    func resetPassword(){
        var dataStore = DataStore()
        dataStore.delegat_resetPassword = self
        dataStore.resetPassword(info: prepareJsonResetPassword())
    }
    func didResetPassword(response: RequestPasswordResponse?) {
        if let _response = response {
            if _response.status.key == SUCCESS_RESPONSE_KEY{
                getToken()
                self.performSegue(withIdentifier: "forgotPasswordToNewsList", sender: self)
            }
            if _response.status.key == FORM_ERROR{
                DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: "Error")
            }
            else{
                if _response.status.messages.last != nil {
                    DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.messages.last!)
                }
                else {
                    //DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: "")
                }
            }
        }
    }
    //Login user
    func getToken(){
        let json      = prepareJsonLogin()
        var dataStore = DataStore()
        dataStore.delegat_getToken = self
        dataStore.getToken(info: json)
    }
    //MARK: Protocol login
    private func prepareJsonLogin()->Dictionary<String,String>{
        var json         = UserToken()
        json.password    = tf_enterNewPassword.text!
        let email_encode = DataUtility.sharedInstance.getUserCredentials().username
        json.username    = email_encode
        json.client_secret = "4d0fee678aw40s0cs8ws884800w0gk4w0848ckk8c0gwgsgsg8"
        json.grant_type  = "password"
        return json.toDictionary()
    }
    func didGetToken(response: ResponseLogin?) {
        if let _response = response {
            if _response.status.key == SUCCESS_RESPONSE_KEY{
                //DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.key)
                
                //DataUtility.sharedInstance.ordinaryLoginPass(success: "OK")
                DataUtility.sharedInstance.saveUserCredentials(username: DataUtility.sharedInstance.getUserCredentials().username!, password: tf_enterNewPassword.text!)
                saveToken(info: _response.payload.first!)
                self.performSegue(withIdentifier: "forgotPasswordToNewsList", sender: self)
            }
            else{
                DataUtility.sharedInstance.showMessage(MessageInfo.Info.rawValue, message: _response.status.messages.last!)
            }
        }
    }
   
    private func saveToken(info : LoginPayload){
        DataUtility.sharedInstance.saveAcccesToken(token: info.accessToken)
        DataUtility.sharedInstance.saveRefreshToken(token: info.refreshToken)
        DataUtility.sharedInstance.saveUser(email:tf_enterNewPassword.text!, password: DataUtility.sharedInstance.getUserCredentials().username!)
    }
    //MARK: Textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
        activeTextField.placeholder = ""
        activeTextField.text = ""
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if activeTextField.text?.isEmpty == true {
            if activeTextField == tf_enterNewCode {
                activeTextField.placeholder = "Enter new code"
            }
            if activeTextField == tf_enterNewPassword {
                activeTextField.placeholder = "Enter new password"
            }
            if activeTextField == tf_confirmPassword {
                activeTextField.placeholder = "Confirm password"
            }
        }
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
