//
//  StatusBarColor.swift
//  Prognostika
//
//  Created by Cope on 4/11/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import Foundation
import UIKit
extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}

