//
//  CustomTextField.swift
//  Prognostika
//
//  Created by Cope on 3/14/17.
//  Copyright © 2017 Comit. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    class TextField: UITextField {
        
        let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5);
        
        override func textRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
        
        override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
        
        override func editingRect(forBounds bounds: CGRect) -> CGRect {
            return UIEdgeInsetsInsetRect(bounds, padding)
        }
    }

}
